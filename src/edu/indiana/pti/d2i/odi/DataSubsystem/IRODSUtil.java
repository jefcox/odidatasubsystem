package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposure;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposureFile;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemFile;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.sdsc.grid.io.FileMetaData;
import edu.sdsc.grid.io.MetaDataCondition;
import edu.sdsc.grid.io.MetaDataRecordList;
import edu.sdsc.grid.io.MetaDataSelect;
import edu.sdsc.grid.io.MetaDataSet;
import edu.sdsc.grid.io.StandardMetaData;
import edu.sdsc.grid.io.irods.IRODSAccount;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.irods.IRODSMetaDataSet;
import edu.sdsc.grid.io.srb.SRBMetaDataSet;

public class IRODSUtil
{
	private static IRODSAccount		irodsAccount	= null;
	private static IRODSFileSystem	irodsFS			= null;

	public static IRODSAccount getIRODSAccount()
	{
		ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();
		if (irodsAccount == null)
			IRODSUtil.irodsAccount = new IRODSAccount(props.getIrodsHost(), props.getIrodsPort(), props.getIrodsUser(), props.getIrodsPassword(),
					props.getIrodsPath(), props.getIrodsZone(), props.getIrodsArchiveResc());

		return irodsAccount;
	}

	public static IRODSFileSystem getIRODSFileSystem()
	{
		try
		{
			if (irodsFS == null)
				irodsFS = new IRODSFileSystem(IRODSUtil.getIRODSAccount());
			else if (irodsFS.isClosed())
				irodsFS = new IRODSFileSystem(IRODSUtil.getIRODSAccount());
		}
		catch (NullPointerException e)
		{
			//			this.updateStatus(ODIDataSubsystemTaskStatus.NPE_ERROR);
			String errorMessage = "NPException caught while trying to get and IRODSFileSystem with ";
			errorMessage += "irods account [ { user name={ " + IRODSUtil.getIRODSAccount().getUserName() + " } ";
			errorMessage += "host={ " + IRODSUtil.getIRODSAccount().getHost() + " } ";
			errorMessage += "]";
			//			log.error(this.dataProduct, errorMessage, e);

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			String errorMessage = "IOException caught while trying to get and IRODSFileSystem with ";
			errorMessage += "irods account [ { user name={ " + IRODSUtil.getIRODSAccount().getUserName() + " } ";
			errorMessage += "host={ " + IRODSUtil.getIRODSAccount().getHost() + " } ";
			errorMessage += "]";
			//			log.error(errorMessage, e);

			e.printStackTrace();
		}

		return irodsFS;
	}

	public static void printQueryResults(MetaDataRecordList[] rl)
	{
		System.out.println("**** IRODS Metdata Query Results ****");
		System.out.println("\n");
		if (rl != null)
		{
			System.out.print("\n");
			for (int i = 0; i < rl.length; i++)
			{
				System.out.print("\n" + rl[i]);
			}
		}
		System.out.println("\n");
		System.out.println("~~~~ IRODS Metdata Query Results ~~~~");
	}

	public static String QueryToString(MetaDataSelect[] selects, MetaDataCondition[] conditions)
	{
		String queryString = "SELECTS ";
		for (int i = 0; i < selects.length; i++)
			queryString += (i < (selects.length - 1)) ? "'" + selects[i] + "' , " : "'" + selects[i] + "'";

		queryString += " CONDITONS ";
		for (int i = 0; i < conditions.length; i++)
			queryString += (i < (conditions.length - 1)) ? conditions[i] + " , " : conditions[i];

		return queryString;
	}

	public static String stripZone(String path)
	{
		path = path.substring(path.indexOf("/") + 1);
		path = path.substring(path.indexOf("/"));

		return path;
	}

	public static void removeDataProduct(String logicalId, ODIDataSubsystemLogger log) throws NullPointerException, IOException
	{
		ODIDataSubsystemDataProduct metaData = IRODSUtil.getDataProduct(logicalId,log);

		IRODSFileSystem irodsFS = IRODSUtil.getIRODSFileSystem();
		IRODSFile archiveFile = null;
		if (metaData.getArchiveFileName() != "")
			archiveFile = new IRODSFile(irodsFS, metaData.getArchiveFileName());
		else if (metaData.getArchiveDir() != "")
			archiveFile = new IRODSFile(irodsFS, metaData.getArchiveDir());

		if (archiveFile != null)
		{
			if (archiveFile.isFile())
			{
				archiveFile.delete();
			}
			else if (archiveFile.isDirectory())
			{
				String[] fileList = archiveFile.list();
				for (String file : fileList)
				{
					IRODSFile replFile = new IRODSFile(irodsFS, archiveFile.getCanonicalPath() + "/" + file);
					replFile.delete();
				}
			}
		}
	}

	public static synchronized void createArchiveDir(IRODSFileSystem irodsFS, String newArchiveDir)
	{
		IRODSFile archiveDir = new IRODSFile(irodsFS, newArchiveDir);
		if (!archiveDir.exists())
		{
			archiveDir.mkdir();
		}
	}

	public static String[] getResourceInfo(IRODSFileSystem irodsFS, String fileName, String resource) throws IOException
	{
		String result[] = {
				"NONE", "NONE"
		};
		MetaDataCondition conditions[] = {
				MetaDataSet.newCondition(FileMetaData.FILE_NAME, MetaDataCondition.EQUAL, fileName),
				MetaDataSet.newCondition(SRBMetaDataSet.RESOURCE_NAME, MetaDataCondition.EQUAL, resource)
		};

		MetaDataSelect[] selects = new MetaDataSelect[] {
				MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_VAULT_PATH), MetaDataSet.newSelection(StandardMetaData.DIRECTORY_NAME)
		};

		MetaDataRecordList[] rl = irodsFS.query(conditions, selects);

		IRODSUtil.printQueryResults(rl);

		if (rl != null)
		{
			result[0] = rl[0].getValue(0).toString();
			result[1] = rl[0].getValue(1).toString();
		}

		return result;
	}

	public static void syncDataProductCache(ODIDataSubsystemDataProduct dataProduct) throws IOException
	{
		//		String fileInfo[] = IRODSUtil.getResourceInfo(irodsFS, archiveFile.getName(), resource);
		//		String resourcePath = fileInfo[0];
		//		String directory = FilenameUtils.getName(fileInfo[1]);
		//		IRODSResource cacheResource = dataProduct.getCacheResource();
		//		String resourcePath = cacheResource.getResourceVaultPath();
		//		if(dataProduct instanceof ODIDataSubsystem)
		//			LocalFile localDirectory = new LocalFile(resourcePath+"/"+dataProduct.getArchiveFileName());

		//		System.out.println(localDirectory.toString());
		//		if(!localDirectory.exists())
		//			localDirectory.mkdir();

		//		LocalFile localFile = new LocalFile(resourcePath+"/"+directory+"/"+archiveFile.getName());

		//		System.out.println(localFile.toString());
		//		if(!localFile.exists())
		{
			//			localFile.createNewFile();
			//			localFile.copyFrom(archiveFile, true);
		}
	}

	public static HashMap<String, String> getAllReplicas(String id)
	{
		HashMap<String, String> replicas = new HashMap<String, String>();
		irodsFS = IRODSUtil.getIRODSFileSystem();
		MetaDataCondition conditions[] = {
			MetaDataSet.newCondition("logicalID", MetaDataCondition.EQUAL, id)
		};
		MetaDataSelect[] fieldNames = new MetaDataSelect[] {
				MetaDataSet.newSelection(IRODSMetaDataSet.RESOURCE_NAME), MetaDataSet.newSelection(IRODSMetaDataSet.FILE_REPLICA_NUM),
		};
		MetaDataRecordList[] rl;
		try
		{
			rl = irodsFS.query(conditions, fieldNames);
			if (rl != null)
			{
				for (MetaDataRecordList record : rl)
				{
					replicas.put(record.getValue(0).toString(), record.getValue(1).toString());
				}
			}
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return replicas;
	}

	public static ODIDataSubsystemExposureFile getODIDataSubsystemExposureFile(String id, ODIDataSubsystemLogger log, ODIDataSubsystemExposure exposure)
	{
		ODIDataSubsystemFile file = IRODSUtil.getODIDataSubsystemFile(id, log);
		ODIDataSubsystemExposureFile exposureFile = new ODIDataSubsystemExposureFile(exposure, id, file.getLocalPath(), file.getChecksum(),
				file.getCacheResourceName(), file.getArchiveResource());
		exposureFile.setReplicas(file.getReplicas());
		exposureFile.setArchiveDir(file.getArchiveDir());
		exposureFile.setArchiveFileName(file.getArchiveFileName());
		exposureFile.setArchivePath(file.getArchivePath());

		return exposureFile;
	}

	public static ODIDataSubsystemFile getODIDataSubsystemFile(String id, ODIDataSubsystemLogger log)
	{
		IRODSFileSystem irodsFS = null;
		ODIDataSubsystemFile file = null;

		try
		{
			irodsFS = IRODSUtil.getIRODSFileSystem();

			MetaDataCondition conditions[] = {
				MetaDataSet.newCondition("logicalID", MetaDataCondition.EQUAL, id)
			};

			MetaDataSelect[] selects = new MetaDataSelect[] {
				MetaDataSet.newSelection(StandardMetaData.FILE_NAME),
			};

			log.debug("Attempting to resolve logical ID [ " + id + " ] as file using query [ " + IRODSUtil.QueryToString(selects, conditions) + " ]");

			MetaDataRecordList[] rl = irodsFS.query(conditions, selects);

			if (rl != null)
			{
				file = new ODIDataSubsystemFile(id);
				String fileName = null;
				IRODSFile archiveFile = null;
				fileName = rl[0].getValue(0).toString();
				archiveFile = new IRODSFile(irodsFS, fileName);

				MetaDataSelect[] fieldNames = new MetaDataSelect[] {
						MetaDataSet.newSelection(IRODSMetaDataSet.META_DATA_ATTR_NAME), MetaDataSet.newSelection(IRODSMetaDataSet.META_DATA_ATTR_VALUE)
				};

				log.debug("Attempting to get metadata for [ " + id + " ] as using query [ " + IRODSUtil.QueryToString(fieldNames, conditions) + " ]");

				rl = irodsFS.query(conditions, fieldNames);
				if (rl != null)
				{
					for (MetaDataRecordList record : rl)
					{
						for (int i = 0; i < record.getFieldCount(); i++)
						{
							if (record.getStringValue(i).compareTo("logicalID") == 0)
								file.setId(record.getStringValue(i + 1));
							if (record.getStringValue(i).compareTo("cacheResource") == 0)
								file.setCacheResource(record.getStringValue(i + 1));
							if (record.getStringValue(i).compareTo("archiveResource") == 0)
								file.setArchiveResource(record.getStringValue(i + 1));
						}
					}
					file.setArchivePath(IRODSUtil.stripZone(archiveFile.getCanonicalPath()));
					file.setArchiveFileName(archiveFile.getName());

					fieldNames = new MetaDataSelect[] {
							MetaDataSet.newSelection(IRODSMetaDataSet.RESOURCE_NAME), MetaDataSet.newSelection(IRODSMetaDataSet.FILE_REPLICA_NUM),
					};
					rl = irodsFS.query(conditions, fieldNames);
					HashMap<String, String> replicas = new HashMap<String, String>();
					if (rl != null)
					{
						for (MetaDataRecordList record : rl)
						{
							replicas.put(record.getValue(0).toString(), record.getValue(1).toString());
						}
					}
					file.setReplicas(replicas);
				}
			}
		}
		catch (NullPointerException e)
		{
			log.error("NPE while resolving Logical ID [ " + id + " ] ]");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			log.error("IOException while resolving Logical ID [ " + id + " ] ]");
			e.printStackTrace();
		}

		return file;
	}

	public static ODIDataSubsystemExposure getODIDataSubsystemExposure(String id, ODIDataSubsystemLogger log)
	{
		IRODSFileSystem irodsFS = null;
		ODIDataSubsystemExposure exposure = null;

		try
		{
			irodsFS = new IRODSFileSystem(IRODSUtil.getIRODSAccount());

			MetaDataCondition dirConditions[] = {
				MetaDataSet.newCondition(StandardMetaData.DIRECTORY_NAME, MetaDataCondition.EQUAL, irodsFS.getHomeDirectory() + "/" + id)
			};

			MetaDataSelect[] dirSelects = new MetaDataSelect[] {
				MetaDataSet.newSelection(StandardMetaData.DIRECTORY_NAME)
			};

			log.debug("Attempting to resolve logical ID [ " + id + " ] as directory using query [ " + IRODSUtil.QueryToString(dirSelects, dirConditions) + " ]");

			MetaDataRecordList[] dirRL = irodsFS.query(dirConditions, dirSelects);
			if (dirRL != null)
			{

				String directoryName = dirRL[0].getValue(0).toString();

				IRODSFile archiveDirectory = new IRODSFile(irodsFS, directoryName);
				exposure = new ODIDataSubsystemExposure(id, archiveDirectory.getName());

				exposure.setArchivePath(IRODSUtil.stripZone(archiveDirectory.getCanonicalPath()));
				exposure.setArchiveDir(archiveDirectory.getName());

				String[] fileList = archiveDirectory.list();
				for (String file : fileList)
				{
					String logicalID = FilenameUtils.getBaseName(FilenameUtils.getBaseName(file));
					ODIDataSubsystemExposureFile odiDataSubsystemExposureFile = IRODSUtil.getODIDataSubsystemExposureFile(logicalID, log, exposure);
					if (odiDataSubsystemExposureFile != null)
					{
						HashMap<String, String> replicas = odiDataSubsystemExposureFile.getReplicas();
						if (!replicas.isEmpty())
							exposure.setReplicas(replicas);

						exposure.addFile(odiDataSubsystemExposureFile);
					}
				}

				ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();
				exposure.setCacheResource(props.getIrodsArchiveCache());
				exposure.setArchiveResource(props.getIrodsArchiveResc());
			}
			log.debug("Resolve Logical ID [ " + id + " ] completed with success]");
		}
		catch (NullPointerException e)
		{
			log.error("NPE while resolving Logical ID [ " + id + " ] ]");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			log.error("IOException while resolving Logical ID [ " + id + " ] ]");
			e.printStackTrace();
		}

		return exposure;

	}

	public static ODIDataSubsystemDataProduct getDataProduct(String id, ODIDataSubsystemLogger log)
	{
		ODIDataSubsystemDataProduct dataProduct = null;

		dataProduct = IRODSUtil.getODIDataSubsystemFile(id, log);
		if (dataProduct == null)
		{
			dataProduct = IRODSUtil.getODIDataSubsystemExposure(id, log);
		}

		return dataProduct;
	}
}
