package edu.indiana.pti.d2i.odi.DataSubsystem;

public interface ODIDataSubsystemTask
{
	public void onStart();
	public void onComplete();
	public void execute();
}
