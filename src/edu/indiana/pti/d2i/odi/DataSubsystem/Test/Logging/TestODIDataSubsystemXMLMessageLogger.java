package edu.indiana.pti.d2i.odi.DataSubsystem.Test.Logging;

import org.junit.Test;

import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogMessageFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemXMLLogMessage;


public class TestODIDataSubsystemXMLMessageLogger extends TestODIDataSubsystemMessageLogger
{

	public TestODIDataSubsystemXMLMessageLogger()
	{
		ODIDataSubsystemLogMessageFactory<ODIDataSubsystemXMLLogMessage> messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemXMLLogMessage>(ODIDataSubsystemXMLLogMessage.class);
		this.messageLogger.setMessageFactory(messageFactory);
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testTraceThrowable()
	 */
	@Override
	@Test
	public void testTraceThrowable()
	{
		// TODO Auto-generated method stub
		super.testTraceThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testTrace()
	 */
	@Override
	@Test
	public void testTrace()
	{
		// TODO Auto-generated method stub
		super.testTrace();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testDebugThrowable()
	 */
	@Override
	@Test
	public void testDebugThrowable()
	{
		// TODO Auto-generated method stub
		super.testDebugThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testDebug()
	 */
	@Override
	@Test
	public void testDebug()
	{
		// TODO Auto-generated method stub
		super.testDebug();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testErrorThrowable()
	 */
	@Override
	@Test
	public void testErrorThrowable()
	{
		// TODO Auto-generated method stub
		super.testErrorThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testError()
	 */
	@Override
	@Test
	public void testError()
	{
		// TODO Auto-generated method stub
		super.testError();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testErrorODIDataSubsystemDataProductThrowable()
	 */
	@Override
	@Test
	public void testErrorODIDataSubsystemDataProductThrowable()
	{
		// TODO Auto-generated method stub
		super.testErrorODIDataSubsystemDataProductThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testErrorODIDataSubsystemDataProduct()
	 */
	@Override
	@Test
	public void testErrorODIDataSubsystemDataProduct()
	{
		// TODO Auto-generated method stub
		super.testErrorODIDataSubsystemDataProduct();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testFatalThrowable()
	 */
	@Override
	@Test
	public void testFatalThrowable()
	{
		// TODO Auto-generated method stub
		super.testFatalThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testFatal()
	 */
	@Override
	@Test
	public void testFatal()
	{
		// TODO Auto-generated method stub
		super.testFatal();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testInfoThrowable()
	 */
	@Override
	@Test
	public void testInfoThrowable()
	{
		// TODO Auto-generated method stub
		super.testInfoThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testInfo()
	 */
	@Override
	@Test
	public void testInfo()
	{
		// TODO Auto-generated method stub
		super.testInfo();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testWarnThrowable()
	 */
	@Override
	@Test
	public void testWarnThrowable()
	{
		// TODO Auto-generated method stub
		super.testWarnThrowable();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testWarn()
	 */
	@Override
	@Test
	public void testWarn()
	{
		// TODO Auto-generated method stub
		super.testWarn();
	}

	/* (non-Javadoc)
	 * @see edu.indiana.pti.d2i.odi.DataSubsystem.test.TestODIDataSubsystemMessageLogger#testPerf()
	 */
	@Override
	@Test
	public void testPerf()
	{
		// TODO Auto-generated method stub
		super.testPerf();
	}
}