package edu.indiana.pti.d2i.odi.DataSubsystem.Test;

import org.apache.commons.configuration.CompositeConfiguration;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemFileLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemMessageLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public abstract class ODIDataSubsystemTest
{
	protected CompositeConfiguration											config				= null;
	protected ODIDataSubsystemProperties										props				= ODIDataSubsystemProperties.getProperties();
	protected ODIDataSubsystemFileLogger										fileLogger			= ODIDataSubsystemFileLogger.getLogger("testing");
	protected ODIDataSubsystemMessageLogger										messageLogger		= ODIDataSubsystemMessageLogger.getLogger("testing");
	protected MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory	= null;

	protected ODIDataSubsystemTest()
	{
		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(props.getMessageServerType());
	}
	
	protected String getExpectedResponse()
	{
		String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
		String configString = this.getClass().getSimpleName() + "." + methodName;
		return this.config.getString(configString);
	}
}
