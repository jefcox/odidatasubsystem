package edu.indiana.pti.d2i.odi.DataSubsystem.Test;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import junit.framework.Assert;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public class TestDataProductRemove
{
	private String															LOG4J_PROPERTIES_FILENAME	= "config/log4j.properties";
	private String															LOG4J_LOGGER_NAME			= "ODIDataSubsystemService";
	private String															MESSAGE_SERVER_HOST			= "localhost";
	private String															MESSAGE_SERVER_TYPE			= "RABBIT_MQ";
	private String															MESSAGE_SERVER_VIRTUAL_HOST	= "/odi";
	private String															MESSAGE_SERVER_USERNAME		= "datasubsystem";
	private String															MESSAGE_SERVER_PASSWORD		= "";
	private String															DATASUBSYSTEM_EXCHANGE		= "datasubsystem.exchange";
	private String															ADMIN_STATUS_QUEUE			= "datasubsystem.exchange";
	private String															REMOVE_REQUEST_QUEUE		= "datasubsystem.exchange";
	private String															serviceName					= "ODIDataSubsystemService";
	private String															configFileName				= "config/ODIDataSubsystem.properties";
	private Logger															log							= null;
	private CompositeConfiguration											config						= new CompositeConfiguration();
	private ODIDataStorageSystem											dss							= null;
	private MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory			= null;
	private MessageQueueConnection											mqConnection				= null;

	public TestDataProductRemove()
	{
		try
		{
			ODIDataSubsystem.Start();
			this.dss = ODIDataStorageSystem.getInstance();
		}
		catch (ConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Before
	public void setUp() throws ConfigurationException
	{
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(new PropertiesConfiguration(this.configFileName));
		this.LOG4J_PROPERTIES_FILENAME = config.getString("log.log4j.properties.fileName");
		this.MESSAGE_SERVER_TYPE = config.getString("message.server.type");
		this.MESSAGE_SERVER_HOST = config.getString("message.server.host");
		this.MESSAGE_SERVER_VIRTUAL_HOST = config.getString("message.server.virtualHost");
		this.MESSAGE_SERVER_USERNAME = config.getString("message.server.username");
		this.MESSAGE_SERVER_PASSWORD = config.getString("message.server.password");
		this.LOG4J_LOGGER_NAME = config.getString("log.log4j.testing");
		this.DATASUBSYSTEM_EXCHANGE = config.getString("ODIDataStorageSystem.message.exchange");
		this.REMOVE_REQUEST_QUEUE = config.getString("ODIDataStorageSystem.message.queue.admin.remove.request");
		this.log = Logger.getLogger(LOG4J_LOGGER_NAME);
		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(this.MESSAGE_SERVER_TYPE);
		this.mqConnection = this.mqConnectionFactory.getConnection(MESSAGE_SERVER_HOST, MESSAGE_SERVER_USERNAME, MESSAGE_SERVER_PASSWORD,
				MESSAGE_SERVER_VIRTUAL_HOST);
	}

	@Test
	public void removeSingleExposure() throws IOException, ConfigurationException
	{
		TestDataProductIngest tdpIngest = new TestDataProductIngest();
		tdpIngest.setUp();
		tdpIngest.ingestPODIExposure();

		String responseQueue = "admin-remove-request-" + UUID.randomUUID();
		String exposureID = "o20111101T012313.00";
		String expectedResponse = FileUtils.readFileToString(new File("test-files/removeSingleExposureResponse.txt"));

		String message = "<admin>";
		message += "<request>";
		message += "<remove>";
		message += "<scope>all</scope>";
		message += "<LogicalIDs>";
		message += "<LogicalID>" + exposureID + "</LogicalID>";
		message += "</LogicalIDs>";
		message += "</remove>";
		message += "</request>";
		message += "</admin>";

		dss.publishTopic(message, this.REMOVE_REQUEST_QUEUE);
		this.log.debug("Published test message: [ " + message + " ] to: " + this.DATASUBSYSTEM_EXCHANGE);

		this.mqConnection.bindQueueToExchange(responseQueue, this.DATASUBSYSTEM_EXCHANGE, "admin.remove.request.complete", true);

		// also many more test cases, multiple ID's, ID for a single file rather than exposure
		// need to remove from only the cache resource
		String response = this.mqConnection.getMessage(responseQueue);
		this.mqConnection.removeQueue(responseQueue);

		this.log.debug("Received response: [ " + response + " ] from: " + responseQueue);
		this.log.debug("Expected response: [ " + expectedResponse + " ] from: " + responseQueue);

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}

	@Test
	public void removeMultipleExposures() throws IOException, ConfigurationException
	{
		TestDataProductIngest tdpIngest = new TestDataProductIngest();
		tdpIngest.setUp();
		tdpIngest.ingestPODIMultipleExposures();

		String responseQueue = "admin-remove-request-" + UUID.randomUUID();
		String[] exposureIDs = {
				"o20111101T012314.00", "o20111101T012315.00"
		};
		String expectedResponse = FileUtils.readFileToString(new File("test-files/removeSingleExposureResponse.txt"));

		String message = "<admin>";
		message += "<request>";
		message += "<remove>";
		message += "<scope>all</scope>";
		message += "<LogicalIDs>";
		
		for (String exposureID : exposureIDs)
			message += "<LogicalID>" + exposureID + "</LogicalID>";
		
		message += "</LogicalIDs>";
		message += "</remove>";
		message += "</request>";
		message += "</admin>";

		dss.publishTopic(message, this.REMOVE_REQUEST_QUEUE);
		this.log.debug("Published test message: [ " + message + " ] to: " + this.DATASUBSYSTEM_EXCHANGE);

		this.mqConnection.bindQueueToExchange(responseQueue, this.DATASUBSYSTEM_EXCHANGE, "admin.remove.request.complete", true);

		// also many more test cases, multiple ID's, ID for a single file rather than exposure
		// need to remove from only the cache resource
		String response = this.mqConnection.getMessage(responseQueue);
		System.out.println(response);
		this.mqConnection.removeQueue(responseQueue);

		this.log.debug("Received response: [ " + response + " ] from: " + responseQueue);
		this.log.debug("Expected response: [ " + expectedResponse + " ] from: " + responseQueue);

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}

	@Test
	public void removeSingleExposureCache() throws IOException, ConfigurationException
	{
		TestDataProductIngest tdpIngest = new TestDataProductIngest();
		tdpIngest.setUp();
		tdpIngest.ingestPODIExposure();

		String responseQueue = "admin-remove-request-" + UUID.randomUUID();
		String exposureID = "o20111101T012313.00";
		String expectedResponse = FileUtils.readFileToString(new File("test-files/removeSingleExposureResponse.txt"));

		String message = "<admin>";
		message += "<request>";
		message += "<remove>";
		message += "<scope>cache</scope>";
		message += "<LogicalIDs>";
		message += "<LogicalID>" + exposureID + "</LogicalID>";
		message += "</LogicalIDs>";
		message += "</remove>";
		message += "</request>";
		message += "</admin>";

		dss.publishTopic(message, this.REMOVE_REQUEST_QUEUE);
		this.log.debug("Published test message: [ " + message + " ] to: " + this.DATASUBSYSTEM_EXCHANGE);

		this.mqConnection.bindQueueToExchange(responseQueue, this.DATASUBSYSTEM_EXCHANGE, "admin.remove.request.complete", true);

		// also many more test cases, multiple ID's, ID for a single file rather than exposure
		// need to remove from only the cache resource
		String response = this.mqConnection.getMessage(responseQueue);
		this.mqConnection.removeQueue(responseQueue);

		this.log.debug("Received response: [ " + response + " ] from: " + responseQueue);
		this.log.debug("Expected response: [ " + expectedResponse + " ] from: " + responseQueue);

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}

	@Test
	public void removeMultipleExposuresCache() throws IOException, ConfigurationException
	{
		TestDataProductIngest tdpIngest = new TestDataProductIngest();
		tdpIngest.setUp();
		tdpIngest.ingestPODIMultipleExposures();

		String responseQueue = "admin-remove-request-" + UUID.randomUUID();
		String[] exposureIDs = {
				"o20111101T012314.00", "o20111101T012315.00"
		};
		String expectedResponse = FileUtils.readFileToString(new File("test-files/removeSingleExposureResponse.txt"));

		String message = "<admin>";
		message += "<request>";
		message += "<remove>";
		message += "<scope>cache</scope>";
		message += "<LogicalIDs>";
		
		for (String exposureID : exposureIDs)
			message += "<LogicalID>" + exposureID + "</LogicalID>";
		
		message += "</LogicalIDs>";
		message += "</remove>";
		message += "</request>";
		message += "</admin>";

		dss.publishTopic(message, this.REMOVE_REQUEST_QUEUE);
		this.log.debug("Published test message: [ " + message + " ] to: " + this.DATASUBSYSTEM_EXCHANGE);

		this.mqConnection.bindQueueToExchange(responseQueue, this.DATASUBSYSTEM_EXCHANGE, "admin.remove.request.complete", true);

		// also many more test cases, multiple ID's, ID for a single file rather than exposure
		// need to remove from only the cache resource
		String response = this.mqConnection.getMessage(responseQueue);
		System.out.println(response);
		this.mqConnection.removeQueue(responseQueue);

		this.log.debug("Received response: [ " + response + " ] from: " + responseQueue);
		this.log.debug("Expected response: [ " + expectedResponse + " ] from: " + responseQueue);

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}
}
