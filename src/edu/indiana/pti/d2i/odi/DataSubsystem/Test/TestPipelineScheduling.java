package edu.indiana.pti.d2i.odi.DataSubsystem.Test;

import java.io.IOException;

import junit.framework.Assert;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;

public class TestPipelineScheduling
{
	private String					LOG4J_PROPERTIES_FILENAME	= "config/log4j.properties";
	private String					LOG4J_LOGGER_NAME			= "ODIDataSubsystemService";
	private String					MESSAGE_SERVER_HOST			= "localhost";
	private String					MESSAGE_SERVER_TYPE			= "RABBIT_MQ";
	private String					PSA_REQUEST_QUEUE			= "datasubsystem.thaw";
	private String					WES_REQUEST_QUEUE			= "datasubsystem.thaw";
	private String					serviceName					= "ODIDataSubsystemService";
	private String					configFileName				= "config/ODIDataSubsystem.properties";
	private Logger					log							= null;
	private CompositeConfiguration	config						= new CompositeConfiguration();
//	private MessagingAdapter		messageAdapter				= null;

	@Before
	public void setUp() throws ConfigurationException
	{
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(new PropertiesConfiguration(this.configFileName));

		ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
		ODIOrchestrationSystem wes = ODIOrchestrationSystem.getInstance();

		this.LOG4J_PROPERTIES_FILENAME = config.getString("log.log4j.properties.fileName");
		this.MESSAGE_SERVER_TYPE = config.getString("message.server.type");
		this.MESSAGE_SERVER_HOST = config.getString("message.server.host");
		this.LOG4J_LOGGER_NAME = config.getString("log.log4j.testing");
		this.log = Logger.getLogger(LOG4J_LOGGER_NAME);
		this.WES_REQUEST_QUEUE = wes.getREQUEST_QUEUE();

//		this.messageAdapter = new MessagingAdapter(MESSAGE_SERVER_HOST, MessagingAdapter.getServerType(this.MESSAGE_SERVER_TYPE), this.config);

	}

	@Test
	public void testViaPSAMessage() throws IOException
	{
		String message = "";
		message = "<request>";
		message += "<time>2011-11-04:09:40:10</time>";
		message += "<dataset>20111031_7decd3a</dataset>";
		message += "<procid>7dec3a</procid>";
		message += "<nights>";
		message += "<night>2011-10-31</night>";
		message += "<night>2011-11-01</night>";
		message += "<night>2011-11-02</night>";
		message += "</nights>";
		message += "<subsets>";
		message += "<subset>*</subset>";
		message += "</subsets>";
		message += "<replace>false</replace>";
		message += "<archive>true</archive>";
		message += "</request>";
//		this.messageAdapter.publishToQueue(PSA_REQUEST_QUEUE, message);

		Assert.assertEquals(true, true);
	}
}
