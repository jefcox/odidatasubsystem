package edu.indiana.pti.d2i.odi.DataSubsystem.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import junit.framework.Assert;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.Before;
import org.junit.Test;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public class TestDataProductIngest extends ODIDataSubsystemTest
{
	private String															MESSAGE_SERVER_HOST				= "localhost";
	private String															MESSAGE_SERVER_TYPE				= "RABBIT_MQ";
	private String															MESSAGE_SERVER_VIRTUAL_HOST		= "/odi";
	private String															MESSAGE_SERVER_USERNAME			= "datasubsystem";
	private String															MESSAGE_SERVER_PASSWORD			= "";
	private String															ARCHIVE_REQUEST_QUEUE			= "datasubsystem.thaw";
	private String															EXPOSURE_ARCHIVE_STATUS_QUEUE	= "datasubsystem.message.queue.exposure.complete";
	private String															serviceName						= "ODIDataSubsystemService";
	private String															configFileName					= "config/ODIDataSubsystem.properties";
	private ODIDataStorageSystem											dss								= null;
	private EventStream														eventStream						= null;
	private MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory				= null;
	private MessageQueueConnection											mqConnection					= null;

	public TestDataProductIngest()
	{
		try
		{
			ODIDataSubsystem.Start();
			this.eventStream = EventStream.getInstance();
			this.dss = ODIDataStorageSystem.getInstance();
		}
		catch (ConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Before
	public void setUp() throws ConfigurationException
	{
		this.MESSAGE_SERVER_TYPE = props.getMessageServerType();
		this.MESSAGE_SERVER_HOST = props.getMessageServerHost();
		this.MESSAGE_SERVER_VIRTUAL_HOST = props.getMessageServerVirtualHost();
		this.MESSAGE_SERVER_USERNAME = props.getMessageServerUserName();
		this.MESSAGE_SERVER_PASSWORD = props.getMessageServerPassword();
		this.ARCHIVE_REQUEST_QUEUE = props.getArchiveRequestQueue();
		this.EXPOSURE_ARCHIVE_STATUS_QUEUE = props.getExposureArchiveStatusQueue();
		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(this.MESSAGE_SERVER_TYPE);
		this.mqConnection = this.mqConnectionFactory.getConnection(MESSAGE_SERVER_HOST, MESSAGE_SERVER_USERNAME, MESSAGE_SERVER_PASSWORD,
				MESSAGE_SERVER_VIRTUAL_HOST);

	}

	@Test
	public void ingestPODIExposure() throws IOException
	{
		this.eventStream = EventStream.getInstance();
		this.dss = ODIDataStorageSystem.getInstance();
		String exposureID = "o20111101T012313-jc-test.00";
		String expectedResponse = FileUtils.readFileToString(new File("test-files/ingestPODIExposureRPCResponse.txt"));

		String testFilesPath = "test-files/" + exposureID + "/";
		String incomingPath = "scratch-files/incoming_raw_tmp/" + exposureID + "/";
		File testDir = new File(testFilesPath);
		File incomingDir = new File(incomingPath);
		FileUtils.copyDirectory(testDir, incomingDir);

		File[] otas = incomingDir.listFiles();
		String message = "<IncomingRawExposures>";
		message += "<IncomingRawExposure>";
		message += "<ID>" + exposureID + "</ID>";
		message += "<OTACount>" + otas.length + "</OTACount>";
		message += "<OTAs>";

		for (File ota : otas)
		{
			String otaID = ota.getName();
			otaID = FilenameUtils.removeExtension(FilenameUtils.removeExtension(otaID));
			FileInputStream fis = new FileInputStream(ota);
			String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);

			message += "<OTA>";
			message += "<ID>" + otaID + "</ID>";
			message += "<File>" + ota + "</File>";
			message += "<MD5>" + md5 + "</MD5>";
			message += "</OTA>";
		}

		message += "</OTAs>";
		message += "</IncomingRawExposure>";
		message += "</IncomingRawExposures>";
		this.mqConnection.publishToQueue(this.ARCHIVE_REQUEST_QUEUE, message);
		String responseQueue = "test-ingest-response-queue-" + UUID.randomUUID();
		
		//Add in here to check both message to the portal and message that the archive was completed.
		this.mqConnection.bindQueueToExchange(responseQueue, props.getDataSubsystemExchange(), props.getExposureArchivedTopic(), true);
		String response = this.mqConnection.getMessage(responseQueue);
		this.mqConnection.removeQueue(responseQueue);

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}

//	@Test
	public void ingestPODIMultipleExposures() throws IOException
	{
		this.eventStream = EventStream.getInstance();
		this.dss = ODIDataStorageSystem.getInstance();
		String[] exposureIDs = {
				"o20111101T012314-jc-test.00", "o20111101T012315-jc-test.00"
		};
		String expectedResponse = FileUtils.readFileToString(new File("test-files/ingestPODIMultipleExposureRPCResponse.txt"));

		List<File> incomingDirs = new ArrayList<File>();
		for (String exposureID : exposureIDs)
		{
			String testFilesPath = "test-files/" + exposureID + "/";
			String incomingPath = "scratch-files/incoming_raw_tmp/" + exposureID + "/";
			File testDir = new File(testFilesPath);
			File incomingDir = new File(incomingPath);
			incomingDirs.add(incomingDir);
			FileUtils.copyDirectory(testDir, incomingDir);
		}

		String message = "<IncomingRawExposures>";
		int exposureIndex = 0;
		for (File incomingDir : incomingDirs)
		{
			File[] otas = incomingDir.listFiles();
			message += "<IncomingRawExposure>";
			message += "<ID>" + exposureIDs[exposureIndex] + "</ID>";
			message += "<OTACount>" + otas.length + "</OTACount>";
			message += "<OTAs>";

			for (File ota : otas)
			{
				String otaID = ota.getName();
				otaID = FilenameUtils.removeExtension(FilenameUtils.removeExtension(otaID));
				FileInputStream fis = new FileInputStream(ota);
				String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);

				message += "<OTA>";
				message += "<ID>" + otaID + "</ID>";
				message += "<File>" + ota + "</File>";
				message += "<MD5>" + md5 + "</MD5>";
				message += "</OTA>";
			}
			message += "</OTAs>";
			message += "</IncomingRawExposure>";
			exposureIndex++;
		}
		message += "</IncomingRawExposures>";

		this.mqConnection.publishToQueue(this.ARCHIVE_REQUEST_QUEUE, message);
		
		String response  = "";
		for(String exposure: exposureIDs)
		{
			response = this.mqConnection.getMessage(this.EXPOSURE_ARCHIVE_STATUS_QUEUE);
		}

		Assert.assertEquals(expectedResponse.substring(0, 25), response.substring(0, 25));
	}
}
