package edu.indiana.pti.d2i.odi.DataSubsystem.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import uk.ac.starlink.votable.VOTableBuilder;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;

public class TestOrchestrationSystem
{
	private String					LOG4J_PROPERTIES_FILENAME	= "config/log4j.properties";
	private String					LOG4J_LOGGER_NAME			= "ODIDataSubsystemService";
	private String					MESSAGE_SERVER_HOST			= "localhost";
	private String					MESSAGE_SERVER_TYPE			= "RABBIT_MQ";
	private String					MESSAGE_SERVER_VIRTUAL_HOST	= "/odi";
	private String					MESSAGE_SERVER_USERNAME		= "datasubsystem";
	private String					MESSAGE_SERVER_PASSWORD		= "";
	private String					PSA_REQUEST_QUEUE			= "datasubsystem.thaw";
	private String					WES_REQUEST_QUEUE			= "datasubsystem.thaw";
	private String					serviceName					= "ODIDataSubsystemService";
	private String					configFileName				= "config/ODIDataSubsystem.properties";
	private Logger					log							= null;
	private CompositeConfiguration	config						= new CompositeConfiguration();
	private MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory				= null;
	private MessageQueueConnection											mqConnection					= null;

	@Before
	public void setUp() throws ConfigurationException
	{
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(new PropertiesConfiguration(this.configFileName));

		ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
		ODIOrchestrationSystem wes = ODIOrchestrationSystem.getInstance();

		this.LOG4J_PROPERTIES_FILENAME = config.getString("log.log4j.properties.fileName");
		this.MESSAGE_SERVER_TYPE = config.getString("message.server.type");
		this.MESSAGE_SERVER_HOST = config.getString("message.server.host");
		this.MESSAGE_SERVER_VIRTUAL_HOST = config.getString("message.server.virtualHost");
		this.MESSAGE_SERVER_USERNAME = config.getString("message.server.username");
		this.MESSAGE_SERVER_PASSWORD = config.getString("message.server.password");

		this.LOG4J_LOGGER_NAME = config.getString("log.log4j.testing");
		this.log = Logger.getLogger(LOG4J_LOGGER_NAME);
		this.WES_REQUEST_QUEUE = wes.getREQUEST_QUEUE();
		this.mqConnection = this.mqConnectionFactory.getConnection(MESSAGE_SERVER_HOST, MESSAGE_SERVER_VIRTUAL_HOST, MESSAGE_SERVER_USERNAME,
				MESSAGE_SERVER_PASSWORD);

	}

	@Test
	public void testViaExecuteWorkflowMessage() throws IOException, InterruptedException
	{
		String message = "";
		File[] votFile = {
				new File("test-files/20111031_test_cab.xml"), new File("test-files/20111031_test_cafR.xml"), new File("test-files/20111031_test_ftrR.xml")
		};

		for (int i = 0; i < votFile.length; i++)
		{
			FileInputStream fStream = new FileInputStream(votFile[i]);
			try
			{
				FileChannel fc = fStream.getChannel();
				MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
				message = Charset.defaultCharset().decode(bb).toString();
			}
			finally
			{
				fStream.close();
			}
			System.out.println("Sending message: " + i + " to: " + WES_REQUEST_QUEUE + " total characters: " + message.length());
			this.mqConnection.publishToQueue(WES_REQUEST_QUEUE, message);
		}
		//wait on response from each publish.
		Thread.sleep(300000);
	}

	public void TestViaMessageFromPSA()
	{
		// 1. Query both calLib and xmccat to retrieve relevant files, then build voTable.
		// 2. Build message to the callibration planning system encapsulating voTable
		// 2.1 Message should include if results should be archived.

		VOTableBuilder votBuilder = new VOTableBuilder();

		String voTable = "";
		voTable += "<?xml version=\"1.0\"?>";
		voTable += "<VOTABLE version=\"1.2\" ";
		voTable += "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		voTable += "xmlns=\"http://www.ivoa.net/xml/VOTable/v1.2\"";
		voTable += "xmlns:stc=\"http://www.ivoa.net/xml/STC/v1.30\" >";
		voTable += "<RESOURCE name=\"11B_20111031_7decd3a\" utype=\"A\">";
		voTable += "  <TABLE name=\"11B_20111031_7decd3a\" utype=\"RemainderTable\">";
		voTable += "    <PARAM name=\"dataset\" datatype=\"char\" value=\"11B_20111031_7decd3a\"/>";
		voTable += "    <PARAM name=\"nights\" datatype=\"char\" value=\"2011-10-31,2011-11-01,2011-11-02\"/>";
		voTable += "    <FIELD name=\"filename\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"fileLocation\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"fileID\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"obstype\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"mjdobs\" datatype=\"double\"/>";
		voTable += "    <FIELD name=\"otaid\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"ccdbin1\" datatype=\"int\"/>";
		voTable += "    <FIELD name=\"ccdbin2\" datatype=\"int\"/>";
		voTable += "    <FIELD name=\"filter\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"exptime\" datatype=\"float\"/>";
		voTable += "    <FIELD name=\"otmode\" datatype=\"char\"/>";
		voTable += "    <DATA><TABLEDATA>";
		voTable += "    <TR><TD>b20111031T230758.00.07.fits</TD>";
		voTable += "        <TD>TBD</TD>";
		voTable += "        <TD>TBD</TD>";
		voTable += "        <TD>zero</TD>";
		voTable += "        <TD>55865.96386574</TD>";
		voTable += "        <TD>1107</TD>";
		voTable += "        <TD>1</TD>";
		voTable += "        <TD>1</TD>";
		voTable += "        <TD>g SDSS k1017</TD>";
		voTable += "        <TD>0.</TD>";
		voTable += "        <TD>STATIC</TD></TR>";
		voTable += "    </TABLEDATA></DATA>";
		voTable += "  </TABLE>";
		voTable += "</RESOURCE>";
		voTable += "</VOTABLE>";

		//TODO build VOTable from data queries based on dataset value
		//		pss.planProcessing(voTable);
		//		ODIDataSubsystemLogger log = pss.getLogger();
		log.debug("Sent the following VOTable to CalPlanner:\n " + voTable);

	}
}