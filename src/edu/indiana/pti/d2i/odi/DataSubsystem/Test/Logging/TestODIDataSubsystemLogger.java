package edu.indiana.pti.d2i.odi.DataSubsystem.Test.Logging;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.indiana.pti.d2i.odi.DataSubsystem.Test.ODIDataSubsystemTest;

public abstract class TestODIDataSubsystemLogger extends ODIDataSubsystemTest
{
	
	@Test
	public void testIsDebugEnabled()
	{
		this.messageLogger.enableDebug();
		assertEquals(true,this.messageLogger.isDebugEnabled());
	}

	@Test
	public void testIsTraceEnabled()
	{
		this.messageLogger.enableTrace();
		assertEquals(true,this.messageLogger.isTraceEnabled());
	}

	@Test
	public void testIsInfoEnabled()
	{
		this.messageLogger.enableTrace();
		assertEquals(true,this.messageLogger.isInfoEnabled());
	}
	
	@Test
	public void testDebugDisabled()
	{
		this.messageLogger.disableDebug();
		assertEquals(false,this.messageLogger.isDebugEnabled());
	}

	@Test
	public void testTraceDisabled()
	{
		this.messageLogger.disableTrace();
		assertEquals(false,this.messageLogger.isTraceEnabled());
	}

	@Test
	public void testInfoDisabled()
	{
		this.messageLogger.disableInfo();
		assertEquals(false,this.messageLogger.isInfoEnabled());
	}
}
