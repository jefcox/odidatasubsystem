package edu.indiana.pti.d2i.odi.DataSubsystem.Test.Logging;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemFile;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogLevel;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;

public abstract class TestODIDataSubsystemMessageLogger extends TestODIDataSubsystemLogger
{
	protected MessageQueueConnection	mqConnection	= null;
	private IOException					testException	= new IOException();
	private ODIDataSubsystemDataProduct	dataProduct		= new ODIDataSubsystemFile("test_data_product");
	String								queueName		= "datasubsystem.log.test";

	protected TestODIDataSubsystemMessageLogger()
	{
		this.mqConnection = this.mqConnectionFactory.getConnection(props.getMessageServerHost(), props.getMessageServerUserName(),
				props.getMessageServerPassword(), props.getMessageServerVirtualHost());
	}

	public void testTraceThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.TRACE;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableTrace();
		String expectedResponse = publishTestMessage(level, null, testException);
		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testTrace()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.TRACE;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableTrace();
		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testDebugThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.DEBUG;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableDebug();
		String expectedResponse = publishTestMessage(level, null, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testDebug()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.DEBUG;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableDebug();
		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testErrorThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.ERROR;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testError()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.ERROR;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testErrorODIDataSubsystemDataProductThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.ERROR;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, dataProduct, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testErrorODIDataSubsystemDataProduct()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.ERROR;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, dataProduct, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testFatalThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.FATAL;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testFatal()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.FATAL;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testInfoThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.INFO;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableInfo();
		String expectedResponse = publishTestMessage(level, null, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testInfo()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.INFO;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		messageLogger.enableInfo();
		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testWarnThrowable()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.WARN;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, testException);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testWarn()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.WARN;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	public void testPerf()
	{
		ODIDataSubsystemLogLevel level = ODIDataSubsystemLogLevel.PERFORMANCE;
		mqConnection.bindQueueToExchange(queueName, props.getLogExchange(), props.getLogTopic(level), true);

		String expectedResponse = publishTestMessage(level, null, null);

		String response = mqConnection.getMessage(queueName);

		mqConnection.removeQueue(queueName);
		assertEquals(expectedResponse, response);
	}

	protected String publishTestMessage(ODIDataSubsystemLogLevel level, ODIDataSubsystemDataProduct dataProduct, Throwable t)
	{
		String message = "Test " + level + " message";
		String expectedResponse = this.messageLogger.getMessageFactory().createMessage(message, dataProduct, t, level).toString();

		switch (level)
		{
			case TRACE:
				messageLogger.trace(message, t);
				break;
			case DEBUG:
				messageLogger.debug(message, t);
				break;
			case ERROR:
				messageLogger.error(dataProduct, message, t);
				break;
			case FATAL:
				messageLogger.fatal(message, t);
				break;
			case INFO:
				messageLogger.info(message, t);
				break;
			case PERFORMANCE:
				messageLogger.perf(message, t);
				break;
			case WARN:
				messageLogger.warn(message, t);
				break;
			default:
				break;
		}
		
		return expectedResponse;
	}
}
