package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.lang.annotation.Annotation;
import java.util.Iterator;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPStatementState;
import com.espertech.esper.client.EPSubscriberException;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.EventType;
import com.espertech.esper.client.SafeIterator;
import com.espertech.esper.client.StatementAwareUpdateListener;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.client.context.ContextPartitionSelector;

public class NullEPStatement implements EPStatement
{

	@Override
	public void addListener(UpdateListener arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void addListener(StatementAwareUpdateListener arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator<StatementAwareUpdateListener> getStatementAwareListeners()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<UpdateListener> getUpdateListeners()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAllListeners()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void removeListener(UpdateListener arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void removeListener(StatementAwareUpdateListener arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public EventType getEventType()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<EventBean> iterator()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SafeIterator<EventBean> safeIterator()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addListenerWithReplay(UpdateListener arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Annotation[] getAnnotations()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName()
	{
		return "NullEPStatement";
	}

	@Override
	public String getServiceIsolated()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EPStatementState getState()
	{
		return EPStatementState.DESTROYED;
	}

	@Override
	public Object getSubscriber()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getText()
	{
		// TODO Auto-generated method stub
		return " ";
	}

	@Override
	public long getTimeLastStateChange()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getUserObject()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isDestroyed()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isPattern()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isStarted()
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isStopped()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Iterator<EventBean> iterator(ContextPartitionSelector arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SafeIterator<EventBean> safeIterator(ContextPartitionSelector arg0)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSubscriber(Object arg0) throws EPSubscriberException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void start()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void stop()
	{
		// TODO Auto-generated method stub

	}

}
