package edu.indiana.pti.d2i.odi.DataSubsystem;

public interface ODIDataSubsystemTaskHook
{
	public void onStart();
	public void onComplete();
}
