package edu.indiana.pti.d2i.odi.DataSubsystem;

public class ODIDataSubsystemPriorityQueueingModule extends ODIDataSubsystemQueueingModule
{

	protected ODIDataSubsystemTaskQueue	taskQueue	= new ODIDataSubsystemTaskQueue();

	protected ODIDataSubsystemPriorityQueueingModule(String moduleName)
	{
		super(moduleName);
		taskQueue = new ODIDataSubsystemPriorityTaskQueue();
	}
}
