package edu.indiana.pti.d2i.odi.DataSubsystem;

public enum ODIDataSubsystemTaskState
{
	Started,Restarted,Uninitialized,Initialized,Working,Completed;
}
