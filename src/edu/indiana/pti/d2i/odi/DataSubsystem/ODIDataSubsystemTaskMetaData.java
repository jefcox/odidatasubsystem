package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;

public class ODIDataSubsystemTaskMetaData
{
	protected ODIDataSubsystemTaskState	state	= ODIDataSubsystemTaskState.Uninitialized;
	protected String					task	= ODIDataSubsystemTaskMetaData.class.getSimpleName();
	protected String					status	= "";

	public ODIDataSubsystemTaskMetaData()
	{
		this.state = ODIDataSubsystemTaskState.Initialized;
	}

	public ODIDataSubsystemTaskMetaData(List<String> resources, String archiveResource, String status)
	{
		this.status = status;
		this.state = ODIDataSubsystemTaskState.Initialized;
	}

	public ODIDataSubsystemTaskState getState()
	{
		return state;
	}

	public void setState(ODIDataSubsystemTaskState state)
	{
		this.state = state;
	}

	public void updateState(ODIDataSubsystemTaskState state)
	{
		this.state = state;
		EventStream.getInstance().insertEvent(this);
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public void updateStatus(String status)
	{
		this.status = status;
		EventStream.getInstance().insertEvent(this);
	}

	public String getTask()
	{
		return task;
	}

	public void setTask(String task)
	{
		this.task = task;
	}

	public String toString()
	{
		String output = "";
		output += "Task: " + this.task + "\n";
		output += "State: " + this.state + "\n";
		output += "Status: " + this.status + "\n";
		return output;
	}
}
