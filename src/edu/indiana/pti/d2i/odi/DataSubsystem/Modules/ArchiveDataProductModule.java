package edu.indiana.pti.d2i.odi.DataSubsystem.Modules;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemPriorityQueueingModule;

public class ArchiveDataProductModule extends ODIDataSubsystemPriorityQueueingModule
{

	protected ArchiveDataProductModule(String moduleName)
	{
		super(moduleName);
	}

}
