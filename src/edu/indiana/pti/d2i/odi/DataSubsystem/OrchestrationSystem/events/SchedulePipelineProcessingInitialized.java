package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIPipelineSchedulingTaskMetaData;

public class SchedulePipelineProcessingInitialized extends ODIDataSubsystemComplexEvent
{
	public SchedulePipelineProcessingInitialized()
	{
		this.name = SchedulePipelineProcessingInitialized.class.getCanonicalName();
		this.expression = "select * from ODIPipelineSchedulingTaskMetaData ";
		this.expression += "where ODIPipelineSchedulingTaskMetaData.state.toString() = '"+ODIDataSubsystemTaskState.Initialized+"'";
	}

	public void update(ODIPipelineSchedulingTaskMetaData event)
	{
		ODIOrchestrationSystem os = ODIOrchestrationSystem.getInstance();
		
		String message = "<response>";
		message += "<time>"+event.getTime()+"</time>";
		message += "<dataset>"+event.getDataset()+"</dataset>";
		message += "<status>"+event.getStatus()+"</status>";
		message += "<state>"+event.getState()+"</state>";
		message += "</response>";
		
		os.respondToRequest(message);
	}
}
