package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem;

import java.util.List;

import uk.ac.starlink.votable.VOTableBuilder;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemAbstractTask;

public class ScheduleProcessingTask extends ODIDataSubsystemAbstractTask
{
	protected List<String>						nights		= null;
	protected List<String>						subsets		= null;
	protected ODIPipelineSchedulingTaskMetaData	metaData	= null;
	protected boolean							archive		= true;
	protected String							XMCCatQuery	= null;
	protected String							calLibQuery	= null;

	public ScheduleProcessingTask(String time, String dataset, String procId, List<String> nights, List<String> subsets, boolean archive)
	{
		this.nights = nights;
		this.subsets = subsets;
		this.archive = archive;
//		super.metaData = this.metaData;
		this.metaData = new ODIPipelineSchedulingTaskMetaData(time, dataset, procId);
	}

	@Override
	public void execute()
	{
		// 1. Send message to the delivery app, it will return the raw ingest votable
		// 2. Build message to the callibration planning system encapsulating voTable
		// 2.1 Message should include if results should be archived.
		

//		ODIPipelineSchedulingSystem pss = ODIPipelineSchedulingSystem.getInstance();
		VOTableBuilder votBuilder = new VOTableBuilder();

		String voTable = "";
		voTable += "<?xml version=\"1.0\"?>";
		voTable += "<VOTABLE version=\"1.2\" ";
		voTable += "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		voTable += "xmlns=\"http://www.ivoa.net/xml/VOTable/v1.2\"";
		voTable += "xmlns:stc=\"http://www.ivoa.net/xml/STC/v1.30\" >";
		voTable += "<RESOURCE name=\"11B_20111031_7decd3a\" utype=\"A\">";
		voTable += "  <TABLE name=\"11B_20111031_7decd3a\" utype=\"RemainderTable\">";
		voTable += "    <PARAM name=\"dataset\" datatype=\"char\" value=\"11B_20111031_7decd3a\"/>";
		voTable += "    <PARAM name=\"nights\" datatype=\"char\" value=\"2011-10-31,2011-11-01,2011-11-02\"/>";
		voTable += "    <FIELD name=\"filename\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"fileLocation\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"fileID\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"obstype\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"mjdobs\" datatype=\"double\"/>";
		voTable += "    <FIELD name=\"otaid\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"ccdbin1\" datatype=\"int\"/>";
		voTable += "    <FIELD name=\"ccdbin2\" datatype=\"int\"/>";
		voTable += "    <FIELD name=\"filter\" datatype=\"char\"/>";
		voTable += "    <FIELD name=\"exptime\" datatype=\"float\"/>";
		voTable += "    <FIELD name=\"otmode\" datatype=\"char\"/>";
		voTable += "    <DATA><TABLEDATA>";
		voTable += "    <TR><TD>b20111031T230758.00.07.fits</TD>";
		voTable += "        <TD>TBD</TD>";
		voTable += "        <TD>TBD</TD>";
		voTable += "        <TD>zero</TD>";
		voTable += "        <TD>55865.96386574</TD>";
		voTable += "        <TD>1107</TD>";
		voTable += "        <TD>1</TD>";
		voTable += "        <TD>1</TD>";
		voTable += "        <TD>g SDSS k1017</TD>";
		voTable += "        <TD>0.</TD>";
		voTable += "        <TD>STATIC</TD></TR>";
		voTable += "    </TABLEDATA></DATA>";
		voTable += "  </TABLE>";
		voTable += "</RESOURCE>";
		voTable += "</VOTABLE>";

		//TODO build VOTable from data queries based on dataset value
//		pss.planProcessing(voTable);
//		ODIDataSubsystemLogger log = pss.getLogger();
//		log.debug("Sent the following VOTable to CalPlanner:\n " + voTable);

	}
}
