package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class SchedulePipelineProcessingRequest extends ODIDataSubsystemComplexEvent
{
	public SchedulePipelineProcessingRequest()
	{
		ODIOrchestrationSystem os = ODIOrchestrationSystem.getInstance();
		this.name = ODIOrchestrationSystem.class.getCanonicalName();
		this.expression = "select * from RabbitMQMessage ";
//		this.expression += "where RabbitMQMessage.envelope.routingKey = '"+os.getPIPELINE_REQUEST_QUEUE()+"'";
	}

	public void update(RabbitMQMessage event)
	{
		ODIOrchestrationSystem os = ODIOrchestrationSystem.getInstance();
		String message = new String(event.getBody());
		
		ODIDataSubsystemLogger log = os.getLogger();
		
		if(log.isDebugEnabled())
			log.debug("Recieved Pipeline Scheduling Request:\n " + message + "\n");

		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			String timeString = XMLUtil.getTextValue(request, "time");
			String dataset = XMLUtil.getTextValue(request, "dataset");
			String procId = XMLUtil.getTextValue(request, "procid");
			List<String> nights = XMLUtil.getTextValues(request, "night");
			List<String> subsets = XMLUtil.getTextValues(request, "subset");
			String replace = XMLUtil.getTextValue(request, "replace");
			String archive = XMLUtil.getTextValue(request, "archive");

			os.schedulePipelineProcessing(timeString, dataset, procId, nights, subsets, Boolean.parseBoolean(archive), Boolean.parseBoolean(replace));
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
