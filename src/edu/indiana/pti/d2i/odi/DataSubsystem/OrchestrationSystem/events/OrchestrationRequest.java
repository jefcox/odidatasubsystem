package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events;

import java.io.IOException;
import java.util.UUID;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class OrchestrationRequest extends ODIDataSubsystemComplexEvent
{
	public OrchestrationRequest()
	{
		ODIOrchestrationSystem os = ODIOrchestrationSystem.getInstance();
		this.name = OrchestrationRequest.class.getCanonicalName();
		this.expression = "select * from RabbitMQMessage ";
		this.expression += "where RabbitMQMessage.envelope.routingKey = '" + os.getREQUEST_QUEUE() + "'";
	}

	public void update(RabbitMQMessage event)
	{
		ODIOrchestrationSystem wes = ODIOrchestrationSystem.getInstance();
		String message1 = new String(event.getBody());
		
		String message = "<message>";
		message += "<header>";
		message += "<experimentid>" + UUID.randomUUID() + "</experimentid>";
		message += "<workflowName>cab</workflowName>";
		message += "<header>";
		message += "<body>";
		message += "<inputs>";
		message += "  <input>";
		message += "      <name>votable</name>";
		message += "      <value>/home1/01437/ogce/ODIDATA/20111031_test_cab/20111031_test_cab.xml</value>";
		message += "  </input>";
		message += "  <input>";
		message += "      <name>tarFile</name>";
		message += "      <value>/home1/01437/ogce/ODIDATA/20111031_test_cab/20111031_test_cab.tar</value>";
		message += "  </input>";
		message += "</inputs>";
		message += "</body>";
		message += "</message>";

		//		ClientResource resource = new ClientResource("http://workflow-dev2.odi.iu.edu:8080/airavata-rest-services/workflow/submit");  

		ClientResource resource = new ClientResource("http://localhost:8181/airavata-rest-services/workflow/submit");

		try
		{
			Representation representation = new StringRepresentation(message, MediaType.APPLICATION_XML);
			resource.setFollowingRedirects(false);
			resource.post(representation, MediaType.APPLICATION_XML).write(System.out);
		}
		catch (ResourceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
