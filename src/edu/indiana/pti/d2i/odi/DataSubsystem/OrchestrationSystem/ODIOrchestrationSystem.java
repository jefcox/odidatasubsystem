package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem;

import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemQueueingModule;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events.OrchestrationRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events.SchedulePipelineProcessingInitialized;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.events.SchedulePipelineProcessingRequest;

public class ODIOrchestrationSystem extends ODIDataSubsystemQueueingModule
{
	private String							propertyPrefix			= ODIOrchestrationSystem.class.getSimpleName();
	private static ODIOrchestrationSystem	instance				= null;
	private String							REQUEST_QUEUE			= null;
	private String							RESPONSE_QUEUE			= null;
	private String							WORKFLOW_REQUEST_QUEUE	= null;
	private String							WORKFLOW_RESPONSE_QUEUE	= null;
	private String							PIPELINE_REQUEST_QUEUE	= null;
	private String							PIPELINE_RESPONSE_QUEUE	= null;

	private ODIOrchestrationSystem()
	{
		super(ODIOrchestrationSystem.class.getSimpleName());
		this.REQUEST_QUEUE = props.getOrchestrationRequestQueue();
		this.RESPONSE_QUEUE = props.getOrchestrationResponseQueue();
	}

	private void detectEvents()
	{
		OrchestrationRequest orchestrationRequest = new OrchestrationRequest();
		SchedulePipelineProcessingRequest SchedulePipelineProcessingRequest = new SchedulePipelineProcessingRequest();
		SchedulePipelineProcessingInitialized SchedulePipelineProcessingInitialized = new SchedulePipelineProcessingInitialized();

		EventStream.getInstance().detect(orchestrationRequest);
		EventStream.getInstance().detect(SchedulePipelineProcessingRequest);
		EventStream.getInstance().detect(SchedulePipelineProcessingInitialized);
	}

	public static synchronized void start()
	{
		if (instance == null)
		{
			instance = new ODIOrchestrationSystem();
			instance.mqConnection.consume(instance.REQUEST_QUEUE);
		}
	}

	public static ODIOrchestrationSystem getInstance()
	{
		if (instance == null)
			start();

		return instance;
	}
	
	public ODIDataSubsystemLogger getLogger()
	{
		return this.getLogger();
	}

	public String getREQUEST_QUEUE()
	{
		return this.REQUEST_QUEUE;
	}

	public String getRESPONSE_QUEUE()
	{
		return this.RESPONSE_QUEUE;
	}
	
	public void respondToRequest(String message)
	{
		this.mqConnection.publishToQueue(RESPONSE_QUEUE, message);
	}

	public void schedulePipelineProcessing(String time, String dataset, String procId, List<String> nights, List<String> subsets, boolean archive,
			boolean replace)
	{
/*
		ScheduleProcessingTask task = new NullScheduleProcessingTask();
		if (replace)
			task = new ScheduleReProcessingTask(time, dataset, procId, nights, subsets, archive);
		else
			task = new ScheduleProcessingTask(time, dataset, procId, nights, subsets, archive);

		taskQueue.addTask(task);
*/
	}

}
