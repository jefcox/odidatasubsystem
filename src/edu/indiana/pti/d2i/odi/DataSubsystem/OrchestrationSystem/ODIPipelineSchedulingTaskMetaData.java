package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskMetaData;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;

public class ODIPipelineSchedulingTaskMetaData extends ODIDataSubsystemTaskMetaData
{
	private String time = null;
	private String dataset = null;
	private String procId = null;
	
	public ODIPipelineSchedulingTaskMetaData( String time, String dataset, String procId)
	{
		this.time = time;
		this.dataset = dataset;
		this.procId = procId;
		this.updateState(ODIDataSubsystemTaskState.Initialized);
	}

	public ODIPipelineSchedulingTaskMetaData(String status)
	{
		this.status = status;
		this.updateState(ODIDataSubsystemTaskState.Initialized);
	}

	public String toString()
	{
		super.toString();
		String output = "";
		output += "time: " + this.time + "\n";
		output += "dataset: " + this.dataset + "\n";
		output += "procId: " + this.procId + "\n";

		return output;
	}
	
	public void updateState(ODIDataSubsystemTaskState state)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
		Date date = new Date();
		this.time = dateFormat.format(date);
		super.updateState(state);
		System.out.println("pipelineMetadata state updated");
	}
	
	public void updateStatus(String status)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
		Date date = new Date();
		this.time = dateFormat.format(date);
		super.updateStatus(status);
	}

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public String getDataset()
	{
		return dataset;
	}

	public void setDataset(String dataset)
	{
		this.dataset = dataset;
	}

	public String getProcId()
	{
		return procId;
	}

	public void setProcId(String procId)
	{
		this.procId = procId;
	}
	
}
