package edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem;

import java.util.List;

public class ScheduleReProcessingTask extends ScheduleProcessingTask
{

	public ScheduleReProcessingTask(String time, String dataset, String procId, List<String> nights, List<String> subsets, boolean archive)
	{
		super(time, dataset, procId, nights, subsets, archive);
		//1. build reprocessing query, should include all files. Not just those that have not yet been processed
		//2. set this.XMCCatQuery and this.CalLibQuery to the relevant queries.
	}

	@Override
	public void execute()
	{
		//Similar to ProcessDataSetTask
	}
}
