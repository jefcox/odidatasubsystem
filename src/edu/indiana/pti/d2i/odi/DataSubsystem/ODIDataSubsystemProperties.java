package edu.indiana.pti.d2i.odi.DataSubsystem;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;

import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogLevel;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogMessageType;

public class ODIDataSubsystemProperties
{
	private int									irodsPort;
	private String								configFileName	= "config/ODIDataSubsystem.properties";
	private String								log4jPropertiesFileName;
	private String								messageServerType;
	private String								messageServerHost;
	private String								messageServerVirtualHost;
	private String								messageServerUserName;
	private String								messageServerPassword;
	private String								fatalTopic;
	private String								errorTopic;
	private String								warnTopic;
	private String								infoTopic;
	private String								debugTopic;
	private String								traceTopic;
	private String								perfTopic;
	private String								logExchange;
	private String								messagingLoggerName;
	private String								irodsHost;
	private String								irodsUser;
	private String								irodsPassword;
	private String								irodsPath;
	private String								irodsZone;
	private String								irodsArchiveResc;
	private String								irodsArchiveCache;
	private String								thawRequestQueue;
	private String								archiveRequestQueue;
	private String								instrumentOutBound;
	private String								instrumentIngested;
	private String								archiveCachePath;
	private String								dataSubsystemExchange;
	private String								removeRequestQueue;
	private String								adminStatusQueue;
	private String								fileArchiveStatusQueue;
	private String								exposureArchiveStatusQueue;

	private ODIDataSubsystemLogMessageType		logMessageType;
	private ODIDataSubsystemLogMessageType		logFileMessageType;
	private CompositeConfiguration				config			= null;
	private String								exposureArchivedTopic;
	private String								exposureThawedTopic;
	private String								detrendedDetailQueue;
	private String								orchestrationRequestQueue;
	private String								orchestrationResponseQueue;
	private String								exposureThawStatusTopic;

	private static ODIDataSubsystemProperties	instance		= null;

	private ODIDataSubsystemProperties()
	{
		this.config = new CompositeConfiguration();
		try
		{
			this.config.addConfiguration(new SystemConfiguration());
			this.config.addConfiguration(new PropertiesConfiguration(this.configFileName));
		}
		catch (ConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.ParsePropertiesFile();
	}

	private void ParsePropertiesFile()
	{
		this.log4jPropertiesFileName = config.getString("log.log4j.properties.fileName");
		this.messageServerType = config.getString("log.message.server.type");
		this.messageServerHost = config.getString("log.message.server.host");
		this.messageServerVirtualHost = config.getString("log.message.server.virtualHost");
		this.messageServerUserName = config.getString("log.message.server.username");
		this.messageServerPassword = config.getString("log.message.server.password");
		this.fatalTopic = config.getString("log.message.topic.fatal");
		this.errorTopic = config.getString("log.message.topic.error");
		this.warnTopic = config.getString("log.message.topic.warn");
		this.infoTopic = config.getString("log.message.topic.info");
		this.debugTopic = config.getString("log.message.topic.debug");
		this.traceTopic = config.getString("log.message.topic.trace");
		this.perfTopic = config.getString("log.message.topic.performance");
		this.logExchange = config.getString("log.message.exchange.log");
		this.messagingLoggerName = config.getString("log.log4j.logger.messaging");

		this.fileArchiveStatusQueue = config.getString("message.queue.file.archiveStatus");
		this.exposureArchiveStatusQueue = config.getString("message.queue.exposure.archiveStatus");
		this.irodsHost = config.getString("irods.host");
		this.irodsPort = config.getInt("irods.port");
		this.irodsUser = config.getString("irods.user");
		this.irodsPassword = config.getString("irods.password");
		this.irodsPath = config.getString("irods.defaultPath");
		this.irodsZone = config.getString("irods.zone");
		this.irodsArchiveResc = config.getString("irods.archiveResource");
		this.irodsArchiveCache = config.getString("irods.cacheResource");
		this.thawRequestQueue = config.getString("message.queue.thaw.request");
		this.archiveRequestQueue = config.getString("message.queue.archive.request");
		this.instrumentOutBound = config.getString("message.queue.instrument.outbound");
		this.instrumentIngested = config.getString("message.queue.instrument.ingested");
		this.archiveCachePath = config.getString("path.archiveCache");
		this.dataSubsystemExchange = config.getString("message.exchange.datasubsystem");
		this.removeRequestQueue = config.getString("message.queue.admin.remove.request");
		this.adminStatusQueue = config.getString("message.queue.admin.status");
		this.exposureArchivedTopic = config.getString("message.topic.exposureArchived");
		this.exposureThawedTopic = config.getString("message.topic.exposureThawed");
		this.exposureThawStatusTopic = config.getString("message.topic.exposureThawStatus");
		this.detrendedDetailQueue = config.getString("message.queue.detrended_details");
		this.orchestrationRequestQueue = config.getString("message.queue.orchestration.request");
		this.orchestrationResponseQueue = config.getString("message.queue.orchestration.response");

		this.logMessageType = ODIDataSubsystemLogMessageType.valueOf(config.getString("log.message.format").toUpperCase());
		this.logFileMessageType = ODIDataSubsystemLogMessageType.valueOf(config.getString("log.message.format.file").toUpperCase());
	}

	public static ODIDataSubsystemProperties getProperties()
	{
		if (instance == null)
			instance = new ODIDataSubsystemProperties();

		return instance;
	}

	public void reload()
	{
		this.ParsePropertiesFile();
	}

	/**
	 * @return the log4jPropertiesFileName
	 */
	public String getLog4jPropertiesFileName()
	{
		return log4jPropertiesFileName;
	}

	/**
	 * @return the messageServerType
	 */
	public String getMessageServerType()
	{
		return messageServerType;
	}

	/**
	 * @return the messageServerHost
	 */
	public String getMessageServerHost()
	{
		return messageServerHost;
	}

	/**
	 * @return the messageServerVirtualHost
	 */
	public String getMessageServerVirtualHost()
	{
		return messageServerVirtualHost;
	}

	/**
	 * @return the messageServerUserName
	 */
	public String getMessageServerUserName()
	{
		return messageServerUserName;
	}

	/**
	 * @return the messageServerPassword
	 */
	public String getMessageServerPassword()
	{
		return messageServerPassword;
	}

	/**
	 * @return the fatalTopic
	 */
	public String getFatalTopic()
	{
		return fatalTopic;
	}

	/**
	 * @return the errorTopic
	 */
	public String getErrorTopic()
	{
		return errorTopic;
	}

	/**
	 * @return the warnTopic
	 */
	public String getWarnTopic()
	{
		return warnTopic;
	}

	/**
	 * @return the infoTopic
	 */
	public String getInfoTopic()
	{
		return infoTopic;
	}

	/**
	 * @return the debugTopic
	 */
	public String getDebugTopic()
	{
		return debugTopic;
	}

	/**
	 * @return the traceTopic
	 */
	public String getTraceTopic()
	{
		return traceTopic;
	}

	/**
	 * @return the perfTopic
	 */
	public String getPerfTopic()
	{
		return perfTopic;
	}

	/**
	 * @return the logExchange
	 */
	public String getLogExchange()
	{
		return logExchange;
	}

	/**
	 * @return the logMessageType
	 */
	public ODIDataSubsystemLogMessageType getLogMessageType()
	{
		return this.logMessageType;
	}

	public ODIDataSubsystemLogMessageType getLogFileMessageType()
	{
		return this.logFileMessageType;
	}

	/**
	 * @return the messageingLoggerName
	 */
	public String getMessageingLoggerName()
	{
		return this.messagingLoggerName;
	}

	/**
	 * @return the serviceLoggerName
	 */
	public String getLoggerName(String moduleName)
	{
		return config.getString("log.log4j.logger." + moduleName);
	}

	/**
	 * @return the numThreads
	 */
	public int getNumThreads(String serviceName)
	{
		int numThreads = config.getInt("module." + serviceName + ".numThreads", 5);
		return numThreads;
	}

	/**
	 * @return the irodsHost
	 */
	public String getIrodsHost()
	{
		return irodsHost;
	}

	/**
	 * @return the irodsPort
	 */
	public int getIrodsPort()
	{
		return irodsPort;
	}

	/**
	 * @return the irodsUser
	 */
	public String getIrodsUser()
	{
		return irodsUser;
	}

	/**
	 * @return the irodsPassword
	 */
	public String getIrodsPassword()
	{
		return irodsPassword;
	}

	/**
	 * @return the irodsPath
	 */
	public String getIrodsPath()
	{
		return irodsPath;
	}

	/**
	 * @return the irodsZone
	 */
	public String getIrodsZone()
	{
		return irodsZone;
	}

	/**
	 * @return the irodsArchiveResc
	 */
	public String getIrodsArchiveResc()
	{
		return irodsArchiveResc;
	}

	/**
	 * @return the irodsArchiveCache
	 */
	public String getIrodsArchiveCache()
	{
		return irodsArchiveCache;
	}

	/**
	 * @return the thawRequestQueue
	 */
	public String getThawRequestQueue()
	{
		return thawRequestQueue;
	}

	/**
	 * @return the archiveRequestQueue
	 */
	public String getArchiveRequestQueue()
	{
		return archiveRequestQueue;
	}

	/**
	 * @return the instrumentOutBound
	 */
	public String getInstrumentOutBound()
	{
		return instrumentOutBound;
	}

	/**
	 * @return the instrumentIngested
	 */
	public String getInstrumentIngested()
	{
		return instrumentIngested;
	}

	/**
	 * @return the archiveCachePath
	 */
	public String getArchiveCachePath()
	{
		return archiveCachePath;
	}

	/**
	 * @return the dataSubsystemExchange
	 */
	public String getDataSubsystemExchange()
	{
		return dataSubsystemExchange;
	}

	/**
	 * @return the removeRequestQueue
	 */
	public String getRemoveRequestQueue()
	{
		return removeRequestQueue;
	}

	/**
	 * @return the adminStatusQueue
	 */
	public String getAdminStatusQueue()
	{
		return adminStatusQueue;
	}

	/**
	 * @return the fileArchiveStatusQueue
	 */
	public String getFileArchiveStatusQueue()
	{
		return fileArchiveStatusQueue;
	}

	/**
	 * @return the exposureArchiveStatusQueue
	 */
	public String getExposureArchiveStatusQueue()
	{
		return exposureArchiveStatusQueue;
	}

	/**
	 * @return the exposureArchivedTopic
	 */
	public String getExposureArchivedTopic()
	{
		return exposureArchivedTopic;
	}

	/**
	 * @return the exposureThawedTopic
	 */
	public String getExposureThawedTopic()
	{
		return exposureThawedTopic;
	}

	/**
	 * @return the detrendedDetailQueue
	 */
	public String getDetrendedDetailQueue()
	{
		return detrendedDetailQueue;
	}

	public String getLogTopic(ODIDataSubsystemLogLevel level)
	{
		String logTopic = "";
		switch (level)
		{
			case TRACE:
				logTopic = this.getTraceTopic();
				break;
			case DEBUG:
				logTopic = this.getDebugTopic();
				break;
			case ERROR:
				logTopic = this.getErrorTopic();
				break;
			case FATAL:
				logTopic = this.getFatalTopic();
				break;
			case INFO:
				logTopic = this.getInfoTopic();
				break;
			case PERFORMANCE:
				logTopic = this.getPerfTopic();
				break;
			case WARN:
				logTopic = this.getWarnTopic();
				break;
			default:
				break;
		}

		return logTopic;
	}

	/**
	 * @return the orchestrationResponseQueue
	 */
	public String getOrchestrationResponseQueue()
	{
		return orchestrationResponseQueue;
	}

	/**
	 * @return the orchestrationRequestQueue
	 */
	public String getOrchestrationRequestQueue()
	{
		return orchestrationRequestQueue;
	}

	/**
	 * @return the exposureThawStatusTopic
	 */
	public String getExposureThawStatusTopic()
	{
		return exposureThawStatusTopic;
	}

}
