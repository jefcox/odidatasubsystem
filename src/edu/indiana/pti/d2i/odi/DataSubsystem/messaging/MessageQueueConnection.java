package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemFileLogger;

public abstract class MessageQueueConnection
{
	protected ODIDataSubsystemProperties	props	= null;
	protected ODIDataSubsystemFileLogger	log		= null;

	public MessageQueueConnection()
	{
		this.props = ODIDataSubsystemProperties.getProperties();
		this.log = ODIDataSubsystemFileLogger.getLogger(props.getMessageingLoggerName());
	}

	public abstract void Connect(String hostName, String userName, String password);

	public abstract void Connect(String hostName, String userName, String password, String virtualHost);

	public abstract void consume(String queueName);

	public abstract void consume(String queueName, MessageHandler messageHandler);

	public abstract void consumeRPC(String queueName, MessageHandler messageHandler);

	public abstract void consumeRPC(String queueName);

	public abstract void bindQueueToExchange(String queueName, String exchangeName, String routingKey);

	public abstract void bindQueueToExchange(String queueName, String exchangeName, String routingKey, boolean isTopic);

	public abstract void publishToQueue(String queueName, String message);

	public abstract void publishToQueue(String queueName, String correlationId, String message);

	public abstract void publishToExchange(String exchangeName, String message, String routingKey);

	public abstract void publishToExchange(String exchangeName, String message, String routingKey, boolean isTopic);

	public abstract void acknowledgeMessage(long deliveryTag, boolean allmessages);

	public abstract void rejectMessage(long deliveryTag, boolean allmessages, boolean requeue);

	public abstract String call(String queueName, String message);

	public abstract String getMessage(String queueName);

	public abstract void removeQueue(String queueName);
}
