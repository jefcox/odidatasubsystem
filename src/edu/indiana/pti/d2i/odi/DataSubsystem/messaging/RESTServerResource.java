package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class RESTServerResource extends ServerResource
{

	@Get
	public String toString()
	{
		return "hello, world";
	}

	@Post
	public void complete()
	{
		
	}
}
