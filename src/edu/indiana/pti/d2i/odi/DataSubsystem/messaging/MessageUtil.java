package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

public class MessageUtil
{

	public static MessageQueueConnectionTypes getServerType(String serverType)
	{
		MessageQueueConnectionTypes type = null;
		if (serverType.equals("RABBIT_MQ"))
			type = MessageQueueConnectionTypes.RabbitMQ;

		if (serverType.equals("WSMESSENGER"))
			type = MessageQueueConnectionTypes.WSMessenger;

		if (serverType.equals("REST"))
			type = MessageQueueConnectionTypes.REST;

		return type;
	}
	
	public static MessageQueueConnectionFactory<? extends MessageQueueConnection> getMQConnectionFactory(String connType)
	{
		//TODO: Consider null connection factory
		MessageQueueConnectionFactory<? extends MessageQueueConnection> mqConnectionFactory = null;
		
		switch (MessageUtil.getServerType(connType))
		{
			case RabbitMQ:
				mqConnectionFactory = new MessageQueueConnectionFactory<RabbitMQConnection>(RabbitMQConnection.class);
				break;
			case REST:
				mqConnectionFactory = new MessageQueueConnectionFactory<RESTConnection>(RESTConnection.class);
				break;
			case WSMessenger:
//				messagingServer = new RESTConnection(hostName);
				break;
		}
		
		return mqConnectionFactory;
		
	}
}
