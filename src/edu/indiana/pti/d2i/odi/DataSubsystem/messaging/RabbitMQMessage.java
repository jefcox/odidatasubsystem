package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Envelope;

public class RabbitMQMessage extends Message
{
	public String			consumerTag;
	public String			replyTo;
	public String			correlationId;
	public Envelope			envelope;
	public BasicProperties	properties;
	public byte[]			body;

	public RabbitMQMessage(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body)
	{
		super(new String(body));
		this.consumerTag = consumerTag;
		this.replyTo = properties.getReplyTo();
		this.correlationId = properties.getCorrelationId();
		this.envelope = envelope;
		this.properties = properties;
		this.body = body;
	}

	public String getConsumerTag()
	{
		return consumerTag;
	}

	public void setConsumerTag(String consumerTag)
	{
		this.consumerTag = consumerTag;
	}

	/**
	 * @return the replyTo
	 */
	public String getReplyTo()
	{
		return replyTo;
	}

	/**
	 * @param replyTo the replyTo to set
	 */
	public void setReplyTo(String replyTo)
	{
		this.replyTo = replyTo;
	}

	/**
	 * @return the correlationId
	 */
	public String getCorrelationId()
	{
		return correlationId;
	}

	/**
	 * @param correlationId the correlationId to set
	 */
	public void setCorrelationId(String correlationId)
	{
		this.correlationId = correlationId;
	}

	public Envelope getEnvelope()
	{
		return envelope;
	}

	public void setEnvelope(Envelope envelope)
	{
		this.envelope = envelope;
	}

	public BasicProperties getProperties()
	{
		return properties;
	}

	public void setProperties(BasicProperties properties)
	{
		this.properties = properties;
	}

	public byte[] getBody()
	{
		return body;
	}

	public void setBody(byte[] body)
	{
		this.body = body;
	}

	@Override
	public String toString()
	{
		String outputString = "RabbitMQMessage - { ";

		outputString += "[ Type=" + this.getClass().getSimpleName() + " ] ";
		outputString += "[ ConsumerTag=" + this.consumerTag + " ] ";
		outputString += "[ Envelope.Exchange=" + this.envelope.getExchange() + " ] ";
		outputString += "[ Envelope.RoutingKey=" + this.envelope.getRoutingKey() + " ] ";
		outputString += "[ BasicProperties.MessageId=" + this.properties.getMessageId() + " ] ";
		outputString += "[ BasicProperties.CorrelationId=" + this.properties.getCorrelationId() + " ] ";
		outputString += "[ BasicProperties.ReplyTo=" + this.properties.getReplyTo() + " ] ";
		outputString += "[ BasicProperties.ContentType=" + this.properties.getContentType() + " ] ";
		outputString += "[ Body=" + new String(this.body) + " ] ";
		outputString += " }";

		return outputString;
	}
}
