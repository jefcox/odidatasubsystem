package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;


public class MessageQueueConnectionFactory<T extends MessageQueueConnection>
{
	Class<T> messageQueueConnectionClass = null;

	public MessageQueueConnectionFactory(Class<T> messageQueueConnectionClass)
	{
		this.messageQueueConnectionClass = messageQueueConnectionClass;
	}

	public T getConnection(String hostName, String userName, String password)
	{
		return this.getConnection(hostName, userName, password, "");
	}
	
	public T getConnection(String hostName, String userName, String password, String virtualHost)
	{
		//TODO: maybe make a null connection object
		T messageQueueConnection = null;;
		try
		{
			messageQueueConnection = this.messageQueueConnectionClass.newInstance();
		}
		catch (InstantiationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		messageQueueConnection.Connect(hostName, userName, password, virtualHost);
		return messageQueueConnection;
	}
}
