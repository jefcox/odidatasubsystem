package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

public enum MessageQueueConnectionTypes
{
	RabbitMQ, WSMessenger, REST;
}
