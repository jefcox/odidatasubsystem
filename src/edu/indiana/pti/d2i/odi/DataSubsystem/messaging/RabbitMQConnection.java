package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import java.io.IOException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;

public class RabbitMQConnection extends MessageQueueConnection
{
	private ConnectionFactory	factory		= null;
	private Connection			connection	= null;
	private Channel				channel		= null;

	@Override
	public void Connect(String hostName, String userName, String password)
	{
		this.Connect(hostName, userName, password, "");
	}

	@Override
	public void Connect(String hostName, String userName, String password, String virtualHost)
	{
		factory = new ConnectionFactory();
		factory.setHost(hostName);
		factory.setVirtualHost(virtualHost);
		factory.setUsername(userName);
		factory.setPassword(password);

		try
		{
			connection = factory.newConnection();
			channel = connection.createChannel();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void consume(String queueName)
	{
		try
		{
			channel.queueDeclare(queueName, true, false, false, null);
			final RabbitMQConsumer consumer = new RabbitMQConsumer(channel, log);
			channel.basicConsume(queueName, consumer);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void consumeRPC(String queueName)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void consume(String queueName, final MessageHandler messageHandler)
	{
		try
		{
			channel.queueDeclare(queueName, true, false, false, null);
			final RabbitMQConsumer consumer = new RabbitMQConsumer(channel, log);
			channel.basicConsume(queueName, consumer);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void bindQueueToExchange(String queueName, String exchangeName, String routingKey)
	{
		this.bindQueueToExchange(queueName, exchangeName, routingKey, false);
	}

	public void bindQueueToExchange(String queueName, String exchangeName, String routingKey, boolean topic)
	{

		try
		{
			if (routingKey != "")
			{
				if (topic)
					channel.exchangeDeclare(exchangeName, "topic", true);
				else
					channel.exchangeDeclare(exchangeName, "direct", true);
			}
			else
				channel.exchangeDeclare(exchangeName, "fanout", true);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		try
		{

			channel.queueDeclare(queueName, true, false, false, null);
			channel.queueBind(queueName, exchangeName, routingKey);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getMessage(String queueName)
	{
		String response = "";
		Channel channel = null;

		try
		{
			channel = this.connection.createChannel();
			channel.queueDeclare(queueName, true, false, false, null);
			QueueingConsumer consumer = new QueueingConsumer(channel);

			channel.basicConsume(queueName, true, consumer);

			QueueingConsumer.Delivery delivery;
			log.debug("Waiting to consume message from queue: " + queueName);

			delivery = consumer.nextDelivery();

			log.debug("Get Message Response-> properties[ replyTo[" + delivery.getProperties().getReplyTo() + "] correlationId["
						+ delivery.getProperties().getCorrelationId() + "]]" + " message[ " + delivery.getBody() + "]");

			response = new String(delivery.getBody());
		}
		catch (ShutdownSignalException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ConsumerCancelledException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			try
			{
				channel.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return response;
	}

	public String call(String queueName, String message)
	{
		String response = null;
		String corrId = java.util.UUID.randomUUID().toString();
		String replyQueueName = null;
		QueueingConsumer consumer = new QueueingConsumer(channel);
		try
		{
			replyQueueName = channel.queueDeclare().getQueue();
			channel.basicConsume(replyQueueName, true, consumer);

			BasicProperties props = new BasicProperties.Builder().correlationId(corrId).replyTo(replyQueueName).build();

			log.debug("RPC Call-> queuName[" + queueName + "] properties[ replyTo[" + props.getReplyTo() + "] correlationId[" + props.getCorrelationId()
						+ "]]" + " message[ " + message + "]");

			channel.basicPublish("", queueName, props, message.getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		while (true)
		{
			QueueingConsumer.Delivery delivery;
			try
			{
				delivery = consumer.nextDelivery();
				log.debug("RPC Response-> properties[ replyTo[" + delivery.getProperties().getReplyTo() + "] correlationId["
							+ delivery.getProperties().getCorrelationId() + "]]" + " message[ " + delivery.getBody() + "]");

				if (delivery.getProperties().getCorrelationId().equals(corrId))
				{
					response = new String(delivery.getBody());
					break;
				}
			}
			catch (ShutdownSignalException e)
			{
				e.printStackTrace();
			}
			catch (ConsumerCancelledException e)
			{
				e.printStackTrace();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		return response;
	}

	@Override
	public void publishToQueue(String queueName, String message)
	{
		try
		{
			channel.queueDeclare(queueName, true, false, false, null);
			channel.basicPublish("", queueName, null, message.getBytes());
			log.debug("Published To Queue-> queueName[" + queueName + "] message[ " + message + "]");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void publishToExchange(String exchangeName, String message, String routingKey)
	{
		this.publishToExchange(exchangeName, message, routingKey, false);
	}

	@Override
	public void publishToExchange(String exchangeName, String message, String routingKey, boolean isTopic)
	{
		try
		{
			if (routingKey != "")
			{
				if (isTopic)
					channel.exchangeDeclare(exchangeName, "topic", true);
				else
					channel.exchangeDeclare(exchangeName, "direct", true);
			}
			else
				channel.exchangeDeclare(exchangeName, "fanout", true);

			channel.basicPublish(exchangeName, routingKey, null, message.getBytes());
			log.debug("Published To Exchange-> exchangeName[" + exchangeName + "] message[ " + message + "] routingKey[" + routingKey + "]");

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void acknowledgeMessage(long deliveryTag, boolean allmessages)
	{
		try
		{
			channel.basicAck(deliveryTag, allmessages);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void rejectMessage(long deliveryTag, boolean allmessages, boolean requeue)
	{
		try
		{
			System.out.println("In MQ Connection deliveryTag: " + deliveryTag);
			channel.basicNack(deliveryTag, allmessages, requeue);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void consumeRPC(String queueName, final MessageHandler messageHandler)
	{
		try
		{
			channel.queueDeclare(queueName, true, false, false, null);
			channel.basicQos(1);

			final QueueingConsumer consumer = new QueueingConsumer(channel);
			channel.basicConsume(queueName, true, consumer);
			new Thread(new Runnable()
			{
				public void run()
				{
					while (true)
					{
						try
						{
							QueueingConsumer.Delivery delivery = consumer.nextDelivery();

							BasicProperties props = delivery.getProperties();
							BasicProperties replyProps = new BasicProperties.Builder().correlationId(props.getCorrelationId()).build();

							String message = new String(delivery.getBody());
							String response = "" + messageHandler.execute(message);

							channel.basicPublish("", props.getReplyTo(), replyProps, response.getBytes());

							channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
						}
						catch (InterruptedException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						catch (IOException e)
						{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ShutdownSignalException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ConsumerCancelledException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void publishToQueue(String queueName, String correlationId, String message)
	{
		try
		{
			BasicProperties replyProps = new BasicProperties.Builder().correlationId(correlationId).build();
			channel.basicPublish("", queueName, replyProps, message.getBytes());
			log.debug("Publish To Queue-> queuName[" + queueName + "] properties[ replyTo[" + replyProps.getReplyTo() + "] correlationId["
						+ replyProps.getCorrelationId() + "]]" + " message[ " + message + "]");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void removeQueue(String queueName)
	{
		try
		{
			channel.queueDelete(queueName);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
