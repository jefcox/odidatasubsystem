package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import java.io.IOException;

import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;

public class RabbitMQConsumer extends DefaultConsumer
{
	ODIDataSubsystemLogger log = null;
	public RabbitMQConsumer(Channel channel, ODIDataSubsystemLogger log)
	{
		super(channel);
		this.log = log;
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, BasicProperties properties, byte[] body) throws IOException
	{
		RabbitMQMessage event = new RabbitMQMessage(consumerTag, envelope, properties, body);
		log.debug("Consumed message [ " + event.toString() +" ] and inserting it into the event stream");
		EventStream.getInstance().insertEvent(event);
	}
}
