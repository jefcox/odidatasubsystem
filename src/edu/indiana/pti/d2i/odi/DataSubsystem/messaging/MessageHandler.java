package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

public interface MessageHandler
{
	public String execute(String message);
}
