package edu.indiana.pti.d2i.odi.DataSubsystem.messaging;

import java.io.IOException;

import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;


public class RESTConnection extends MessageQueueConnection
{
	String hostName = "localhost";
	
	public RESTConnection(String hostName)
	{
		try
		{
			this.hostName = hostName;
			new Server(Protocol.HTTP, 8182, RESTServerResource.class).start();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}

	@Override
	public void consume(String queueName)
	{
		
	}

	@Override
	public void consumeRPC(String queueName)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void consume(String queueName, final MessageHandler messageHandler)
	{
		
	}

	@Override
	public void bindQueueToExchange(String queueName, String exchangeName, String routingKey)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void bindQueueToExchange(String queueName, String exchangeName, String routingKey, boolean topic)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getMessage(String queueName)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String call(String queueName, String message)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void publishToQueue(String messageReciever, String message)
	{
		ClientResource resource = new ClientResource("http://" + hostName + "/" + messageReciever);  
		
		try
		{
			resource.post(message).write(System.out);

//			if (response.getStatus().isSuccess()) {
//			    System.out.println("Success! " + response.getStatus());
//			    System.out.println(response.getEntity().getText());
//			} else {
//			    System.out.println("ERROR! " + response.getStatus());
//			    System.out.println(response.getEntity().getText());
//			}

//			resource.get().write(System.out);
		}
		catch (ResourceException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void publishToExchange(String exchangeName, String message, String routingKey)
	{
		
	}

	@Override
	public void consumeRPC(String queueName, final MessageHandler messageHandler)
	{
		
	}

	@Override
	public void publishToQueue(String queueName, String correlationId, String message)
	{
		
	}

	@Override
	public void acknowledgeMessage(long deliveryTag, boolean allmessages)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rejectMessage(long deliveryTag, boolean allmessages, boolean requeue)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void publishToExchange(String exchangeName, String message, String routingKey, boolean isTopic)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Connect(String hostName, String userName, String password)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void Connect(String hostName, String userName, String password, String virtualHost)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeQueue(String queueName)
	{
		// TODO Auto-generated method stub
		
	}
}
