package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.log4j.PropertyConfigurator;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.OrchestrationSystem.ODIOrchestrationSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.ODIQueryProcessingSystem;

public class ODIDataSubsystem
{
	private static ODIDataSubsystem	instance					= null;
	private String					configFileName				= "DEFAULT";
	private String					LOG4J_PROPERTIES_FILENAME	= "config/log4j.properties";
	private CompositeConfiguration	config						= new CompositeConfiguration();

	private ODIDataSubsystem() throws ConfigurationException
	{
		this("config/ODIDataSubsystem.properties");
	}

	private ODIDataSubsystem(String configFile) throws ConfigurationException
	{
		System.setProperty("CONFIG_FILE_PATH", configFile);
		this.configFileName = configFile;
		this.parsePropertiesFile();
		PropertyConfigurator.configure((this.LOG4J_PROPERTIES_FILENAME));
	}

	public static void Start() throws ConfigurationException
	{
		if (instance == null)
			instance = new ODIDataSubsystem();
		EventStream.Start();
		ODIDataStorageSystem.start();
		ODIQueryProcessingSystem.start();
		ODIOrchestrationSystem.start();
	}

	public static void Start(String configFileName) throws ConfigurationException
	{
		instance = new ODIDataSubsystem(configFileName);
		EventStream.Start();
		ODIDataStorageSystem.start();
		ODIQueryProcessingSystem.start();
		ODIOrchestrationSystem.start();
	}

	public static void startQueryProcessingSystem()
	{
		ODIQueryProcessingSystem.start();
	}

	public static void startDataStorageSystem()
	{
		ODIDataStorageSystem.start();
	}

	public static void startOrchestrationSystem()
	{
		ODIOrchestrationSystem.start();
	}

	public static ODIDataSubsystem getInstance() throws ConfigurationException
	{
		if (instance == null)
			ODIDataSubsystem.Start();
		return instance;
	}

	private void parsePropertiesFile() throws ConfigurationException
	{
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(new PropertiesConfiguration(this.configFileName));
		this.LOG4J_PROPERTIES_FILENAME = config.getString("log.log4j.properties.fileName");
	}

	public static void main(String[] args) throws InterruptedException, ConfigurationException, NullPointerException, IOException
	{
		String path = "/";
		String configFileName = null;

		DataSubsystemSignalHandler.handleSignal("TERM");

		Option help = new Option("help", false, "Print this message");
		Option configFile = new Option("configFile", false, "Path to configuration file");
		Option DataStorageSystem = new Option("DataStorageSystem", false, "Start only the Data Storage System");
		Option QueryProcessingSystem = new Option("QueryProcessingSystem", false, "Start only the Query Processing System");
		Option OrchestrationSystem = new Option("OrchestrationSystem", false, "Start only the Orchestratoin System");
		Option Start = new Option("Start", false, "Start all System");

		Options options = new Options();
		options.addOption(help);
		options.addOption(configFile);
		options.addOption(DataStorageSystem);
		options.addOption(QueryProcessingSystem);
		options.addOption(OrchestrationSystem);
		options.addOption(Start);

		CommandLineParser parser = new GnuParser();
		try
		{
			CommandLine line = parser.parse(options, args);

			if (line.hasOption("help"))
			{
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("ODIDataSubsystem", options);
			}

			if (line.hasOption("configFile"))
			{
				configFileName = line.getOptionValue("configFile");
			}

			if (line.hasOption("DataStorageSystem"))
			{
				ODIDataSubsystem.startDataStorageSystem();
			}

			if (line.hasOption("Start"))
			{
				ODIDataSubsystem.Start();
			}

		}
		catch (ParseException exp)
		{
			System.err.println(exp.getMessage());
		}
	}
}
