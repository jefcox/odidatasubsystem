package edu.indiana.pti.d2i.odi.DataSubsystem;

public enum ODIDataSubsystemTaskStatus
{
	OK, 
	FAILED, 
	IO_ERROR, 
	NPE_ERROR, 
	CHECKSUM_MISMATCH, 
	NOT_CACHED, 
	NOT_ARCHIVED,
	ARCHIVE_NOT_FOUND,
	LOGICAL_ID_NOT_FOUND;
}
