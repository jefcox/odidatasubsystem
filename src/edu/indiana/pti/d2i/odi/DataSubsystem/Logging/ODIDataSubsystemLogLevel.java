package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

public enum ODIDataSubsystemLogLevel
{
	FATAL,ERROR,WARN,INFO,DEBUG,TRACE,PERFORMANCE
}
