package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import org.apache.log4j.Logger;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;

public class ODIDataSubsystemFileLogger extends ODIDataSubsystemLogger
{
	private Logger log = null;
	
	private ODIDataSubsystemFileLogger(String name)
	{
		super(name);
		this.log = this.getFileLogger();
		
		switch (props.getLogFileMessageType())
		{
			case XML:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemXMLLogMessage>(ODIDataSubsystemXMLLogMessage.class);
				break;
			case JSON:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemJSONLogMessage>(ODIDataSubsystemJSONLogMessage.class);
				break;
			case STRING:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemStringLogMessage>(ODIDataSubsystemStringLogMessage.class);
				break;
		}
	}
	
	public static ODIDataSubsystemFileLogger getLogger(String name)
	{
		return new ODIDataSubsystemFileLogger(name);
	}
	
	public void trace(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.TRACE);
		if (isTraceEnabled())
			this.log.trace(logMessage, t);
	}

	public void trace(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.TRACE);
		if (isTraceEnabled())
			this.log.trace(logMessage);
	}

	public void debug(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.DEBUG);
		if (isDebugEnabled())
			this.log.debug(logMessage, t);
	}

	public void debug(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.DEBUG);
		if (isDebugEnabled())
			this.log.debug(logMessage);
	}

	public void error(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.ERROR);
		this.log.error(logMessage, t);
	}

	public void error(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.ERROR);
		this.log.error(logMessage);
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), dataProduct, ODIDataSubsystemLogLevel.ERROR);
		this.log.error(logMessage, t);
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), dataProduct, ODIDataSubsystemLogLevel.ERROR);
		this.log.error(logMessage);
	}

	public void fatal(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.FATAL);
		this.log.fatal(logMessage, t);
	}

	public void fatal(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.FATAL);
		this.log.fatal(logMessage);
	}

	public void info(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.INFO);
		if (isInfoEnabled())
			this.log.info(logMessage, t);
	}

	public void info(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.INFO);
		if (isInfoEnabled())
			this.log.info(logMessage);
	}

	public void warn(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.WARN);
		this.log.warn(logMessage, t);
	}

	public void warn(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.WARN);
		this.log.warn(logMessage);
	}
}
