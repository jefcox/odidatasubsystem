package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;


public class ODIDataSubsystemStringLogMessage extends ODIDataSubsystemLogMessage
{
	
	ODIDataSubsystemStringLogMessage(String message, ODIDataSubsystemLogLevel level)
	{
		this(message, null, null, level);
	}

	ODIDataSubsystemStringLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemLogLevel level)
	{
		this(message, dataProduct, null, level);
	}
	
	ODIDataSubsystemStringLogMessage(String message, Throwable t, ODIDataSubsystemLogLevel level)
	{
		this(message, null, t, level);
	}

	ODIDataSubsystemStringLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, Throwable t, ODIDataSubsystemLogLevel level)
	{
		super(message, dataProduct, t, level);
	}
	
	public String toString()
	{
		String message = "[ ";  
		
		if(this.logMessage != null)
			message += "LOG_LEVEL: { " + this.logLevel + " } ";
		
		if(this.logMessage != null)
			message += "LOG_MESSAGE: { " + this.logMessage + " } ";

		if(this.exceptionMessage != null)
			message += "EXCEPTION_MESSAGE: { " + this.exceptionMessage + " } ";

		if(this.stackTrace != null)
		{
			message += "STACK_TRACE: { "; 
			for(StackTraceElement element: this.stackTrace)
				message += "TRACE: { " + element.toString() + " } ";
			message += "} "; 
		}

		if(this.dataProduct != null)
			message += "DATAPRODUCT: { " + this.dataProduct.toString() + " } ";
		
		message += " ] ";
		
		return message;
	}
}
