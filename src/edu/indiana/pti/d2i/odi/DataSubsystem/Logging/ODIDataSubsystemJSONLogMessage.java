package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;

public class ODIDataSubsystemJSONLogMessage extends ODIDataSubsystemLogMessage
{
	
	ODIDataSubsystemJSONLogMessage(String message, ODIDataSubsystemLogLevel level)
	{
		this(message, null, null, level);
	}

	ODIDataSubsystemJSONLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemLogLevel level)
	{
		this(message, dataProduct, null, level);
	}
	
	ODIDataSubsystemJSONLogMessage(String message, Throwable t, ODIDataSubsystemLogLevel level)
	{
		this(message, null, t, level);
	}

	ODIDataSubsystemJSONLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, Throwable t, ODIDataSubsystemLogLevel level)
	{
		super(message, dataProduct, t, level);
	}
	
	public String toString()
	{
		Gson gson = new Gson();
		Type type = new TypeToken<ODIDataSubsystemLogMessage>(){}.getType();
		String message = gson.toJson(this, type);
		
		return message;
	}
}
