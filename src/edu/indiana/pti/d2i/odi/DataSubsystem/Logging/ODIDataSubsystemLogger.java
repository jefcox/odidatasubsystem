package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;

public abstract class ODIDataSubsystemLogger
{
	private Logger																		log				= null;
	protected ODIDataSubsystemProperties												props			= null;
	protected ODIDataSubsystemLogMessageFactory<? extends ODIDataSubsystemLogMessage>	messageFactory	= null;

	protected ODIDataSubsystemLogger()
	{
		this("default");
	}

	protected ODIDataSubsystemLogger(String name)
	{
		this.log = Logger.getLogger(name);
		this.props = ODIDataSubsystemProperties.getProperties();
		PropertyConfigurator.configure((props.getLog4jPropertiesFileName()));

		switch (props.getLogMessageType())
		{
			case XML:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemXMLLogMessage>(ODIDataSubsystemXMLLogMessage.class);
				break;
			case JSON:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemJSONLogMessage>(ODIDataSubsystemJSONLogMessage.class);
				break;
			case STRING:
				this.messageFactory = new ODIDataSubsystemLogMessageFactory<ODIDataSubsystemStringLogMessage>(ODIDataSubsystemStringLogMessage.class);
				break;
		}
	}

	protected Logger getFileLogger()
	{
		return this.log;
	}

	public abstract void trace(Object message, Throwable t);

	public abstract void trace(Object message);

	public abstract void debug(Object message, Throwable t);

	public abstract void debug(Object message);

	public abstract void error(Object message, Throwable t);

	public abstract void error(Object message);

	public abstract void error(ODIDataSubsystemDataProduct dataProduct, Object message, Throwable t);

	public abstract void error(ODIDataSubsystemDataProduct dataProduct, Object message);

	public abstract void fatal(Object message, Throwable t);

	public abstract void fatal(Object message);

	public abstract void info(Object message, Throwable t);

	public abstract void info(Object message);

	public abstract void warn(Object message, Throwable t);

	public abstract void warn(Object message);

	public void enableDebug()
	{
		log.setLevel(Level.DEBUG);
	}

	public void disableDebug()
	{
		if (log.isDebugEnabled())
			log.setLevel(Level.ERROR);
	}

	public void enableTrace()
	{
		log.setLevel(Level.TRACE);
	}

	public void disableTrace()
	{
		if (log.isTraceEnabled())
			log.setLevel(Level.ERROR);
	}

	public void enableInfo()
	{
		log.setLevel(Level.INFO);
	}

	public void disableInfo()
	{
		if (log.isInfoEnabled())
			log.setLevel(Level.ERROR);
	}

	public boolean isDebugEnabled()
	{
		return log.isDebugEnabled();
	}

	public boolean isTraceEnabled()
	{
		return log.isTraceEnabled();
	}

	public boolean isInfoEnabled()
	{
		return log.isInfoEnabled();
	}

	public void setMessageFactory(ODIDataSubsystemLogMessageFactory<? extends ODIDataSubsystemLogMessage> messageFactory)
	{
		this.messageFactory = messageFactory;
	}

	public ODIDataSubsystemLogMessageFactory<? extends ODIDataSubsystemLogMessage> getMessageFactory()
	{
		return this.messageFactory;
	}
}
