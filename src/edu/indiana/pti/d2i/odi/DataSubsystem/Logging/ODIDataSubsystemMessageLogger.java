package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public class ODIDataSubsystemMessageLogger extends ODIDataSubsystemLogger
{
	protected MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory			= null;
	protected MessageQueueConnection											mqConnection				= null;

	private ODIDataSubsystemMessageLogger(String name)
	{
		super(name);
		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(props.getMessageServerType());

		this.mqConnection = this.mqConnectionFactory.getConnection(props.getMessageServerHost(), props.getMessageServerUserName(), props.getMessageServerPassword(),
				props.getMessageServerVirtualHost());
	}
	
	public static ODIDataSubsystemMessageLogger getLogger(String name)
	{
		return new ODIDataSubsystemMessageLogger(name);
	}
	
	public void trace(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.TRACE);
		if (isTraceEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getTraceTopic(), true);
	}

	public void trace(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.TRACE);
		if (isTraceEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getTraceTopic(), true);
	}

	public void debug(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.DEBUG);
		if (isDebugEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getDebugTopic(), true);
	}

	public void debug(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.DEBUG);
		if (isDebugEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getDebugTopic(), true);
	}

	public void error(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.ERROR);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getErrorTopic(), true);
	}

	public void error(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.ERROR);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getErrorTopic(), true);
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), dataProduct, t, ODIDataSubsystemLogLevel.ERROR);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getErrorTopic(), true);
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), dataProduct, ODIDataSubsystemLogLevel.ERROR);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getErrorTopic(), true);
	}

	public void fatal(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.FATAL);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getFatalTopic(), true);
	}

	public void fatal(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.FATAL);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getFatalTopic(), true);
	}

	public void info(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.INFO);
		if (isInfoEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getInfoTopic(), true);
	}

	public void info(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.INFO);
		if (isInfoEnabled())
			this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getInfoTopic(), true);
	}

	public void warn(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.WARN);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getWarnTopic(), true);
	}

	public void warn(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.WARN);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getWarnTopic(), true);
	}

	public void perf(Object message, Throwable t)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), t, ODIDataSubsystemLogLevel.PERFORMANCE);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getPerfTopic(), true);
	}

	public void perf(Object message)
	{
		ODIDataSubsystemLogMessage logMessage = messageFactory.createMessage(message.toString(), ODIDataSubsystemLogLevel.PERFORMANCE);
		this.mqConnection.publishToExchange(props.getLogExchange(), logMessage.toString(), props.getPerfTopic(), true);
	}
}
