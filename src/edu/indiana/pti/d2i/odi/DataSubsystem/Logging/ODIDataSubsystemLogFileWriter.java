package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public class ODIDataSubsystemLogFileWriter
{
	private String																LOG4J_PROPERTIES_FILENAME	= "config/log4j.properties";
	private String																MESSAGE_SERVER_HOST			= "localhost";
	private String																MESSAGE_SERVER_TYPE			= "RABBIT_MQ";
	private String																MESSAGE_SERVER_VIRTUAL_HOST	= "/odi";
	private String																MESSAGE_SERVER_USERNAME		= "datasubsystem";
	private String																MESSAGE_SERVER_PASSWORD		= "";
	private String																LOG_EXCHANGE				= "datasubsystem";
	private String																FATAL_QUEUE					= "datasubsystem.fatal";
	private String																ERROR_QUEUE					= "datasubsystem.error";
	private String																WARN_QUEUE					= "datasubsystem.warn";
	private String																INFO_QUEUE					= "datasubsystem.info";
	private String																configFileName				= "config/ODIDataSubsystem.properties";
	protected MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory			= null;
	protected MessageQueueConnection											mqConnection				= null;
	private CompositeConfiguration												config						= new CompositeConfiguration();
	private Logger																log							= null;

	private ODIDataSubsystemLogFileWriter(String name, String configFile)
	{
		this.log = Logger.getLogger(name);
		this.configFileName = configFile;
		try
		{
			this.parsePropertiesFile();
			PropertyConfigurator.configure((LOG4J_PROPERTIES_FILENAME));
		}
		catch (ConfigurationException e)
		{
			e.printStackTrace();
		}

		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(this.MESSAGE_SERVER_TYPE);

		this.mqConnection = this.mqConnectionFactory.getConnection(MESSAGE_SERVER_HOST, MESSAGE_SERVER_USERNAME, MESSAGE_SERVER_PASSWORD, MESSAGE_SERVER_VIRTUAL_HOST);
		initQueues();
	}

	private ODIDataSubsystemLogFileWriter(String name, String configFile, MessageQueueConnection mqConnection)
	{
		this.log = Logger.getLogger(name);
		this.configFileName = configFile;
		try
		{
			this.parsePropertiesFile();
			PropertyConfigurator.configure((LOG4J_PROPERTIES_FILENAME));
		}
		catch (ConfigurationException e)
		{
			e.printStackTrace();
		}
		
		this.mqConnection = mqConnection;
		this.mqConnection.Connect(MESSAGE_SERVER_HOST, MESSAGE_SERVER_USERNAME, MESSAGE_SERVER_PASSWORD, MESSAGE_SERVER_VIRTUAL_HOST);

		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(this.MESSAGE_SERVER_TYPE);
		initQueues();
	}

	public static ODIDataSubsystemLogFileWriter getLogger(String name, String configFile)
	{
		return new ODIDataSubsystemLogFileWriter(name, configFile);
	}

	public static ODIDataSubsystemLogFileWriter getLogger(String name, String configFile, MessageQueueConnection mqConnection)
	{
		return new ODIDataSubsystemLogFileWriter(name, configFile, mqConnection);
	}

	private void initQueues()
	{
		this.mqConnection.bindQueueToExchange(FATAL_QUEUE, LOG_EXCHANGE, "fatal");
		this.mqConnection.bindQueueToExchange(ERROR_QUEUE, LOG_EXCHANGE, "error");
		this.mqConnection.bindQueueToExchange(WARN_QUEUE, LOG_EXCHANGE, "warn");
		this.mqConnection.bindQueueToExchange(INFO_QUEUE, LOG_EXCHANGE, "info");
	}

	private void parsePropertiesFile() throws ConfigurationException
	{
		config.addConfiguration(new SystemConfiguration());
		config.addConfiguration(new PropertiesConfiguration(this.configFileName));

		this.LOG4J_PROPERTIES_FILENAME = config.getString("log.log4j.properties.fileName");
		this.MESSAGE_SERVER_TYPE = config.getString("log.log4j.message.server.type");
		this.MESSAGE_SERVER_HOST = config.getString("log.log4j.message.server.host");
		this.MESSAGE_SERVER_VIRTUAL_HOST = config.getString("message.server.virtualHost");
		this.MESSAGE_SERVER_USERNAME = config.getString("message.server.username");
		this.MESSAGE_SERVER_PASSWORD = config.getString("message.server.password");
		this.LOG_EXCHANGE = config.getString("log.message.exchange.log");
		this.FATAL_QUEUE = config.getString("log.message.queue.fatal");
		this.ERROR_QUEUE = config.getString("log.message.queue.error");
		this.WARN_QUEUE = config.getString("log.message.queue.warn");
		this.INFO_QUEUE = config.getString("log.message.queue.info");
	}

	public void trace(Object message, Throwable t)
	{
		if (log.isTraceEnabled())
		{
			log.trace(message, t);
		}
	}

	public void trace(Object message)
	{
		if (log.isTraceEnabled())
		{
			log.trace(message);
		}
	}

	public void debug(Object message, Throwable t)
	{
		if (log.isDebugEnabled())
		{
			log.debug(message, t);
		}
	}

	public void debug(Object message)
	{
		if (log.isDebugEnabled())
		{
			log.debug(message);
		}
	}

	public void error(Object message, Throwable t)
	{
		log.error(message, t);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "error");
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message, Throwable t)
	{
		log.error(message, t);
		String errorMessage = "<error><logicalId>" + dataProduct.getId() + "</logicalId>";
		errorMessage += "<description>" + message.toString() + "</description></error>";
		this.mqConnection.publishToExchange(LOG_EXCHANGE, errorMessage, "error");
	}

	public void error(Object message)
	{
		log.error(message);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "error");
	}

	public void error(ODIDataSubsystemDataProduct dataProduct, Object message)
	{
		log.error(message);
		String errorMessage = "<error><logicalId>" + dataProduct.getId() + "</logicalId>";
		errorMessage += "<description>" + message.toString() + "</description></error>";
		this.mqConnection.publishToExchange(LOG_EXCHANGE, errorMessage, "error");
	}

	public void fatal(Object message, Throwable t)
	{
		log.fatal(message, t);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "fatal");
	}

	public void fatal(Object message)
	{
		log.fatal(message);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "fatal");
	}

	public void info(Object message, Throwable t)
	{
		if (log.isInfoEnabled())
		{
			log.info(message, t);
			this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "info");
		}
	}

	public void info(Object message)
	{
		if (log.isInfoEnabled())
		{
			log.info(message);
			this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "info");
		}
	}

	public void warn(Object message, Throwable t)
	{
		log.warn(message, t);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "warn");
	}

	public void warn(Object message)
	{
		log.warn(message);
		this.mqConnection.publishToExchange(LOG_EXCHANGE, message.toString(), "warn");
	}

	public boolean isDebugEnabled()
	{
		return log.isDebugEnabled();
	}

	public boolean isTraceEnabled()
	{
		return log.isTraceEnabled();
	}

	public boolean isInfoEnabled()
	{
		return log.isInfoEnabled();
	}
}
