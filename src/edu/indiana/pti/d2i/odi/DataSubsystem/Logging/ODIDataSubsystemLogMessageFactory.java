package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import java.lang.reflect.InvocationTargetException;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;


public class ODIDataSubsystemLogMessageFactory<T extends ODIDataSubsystemLogMessage>
{
	Class<T> odiDataSubsystemLogMessageClass = null;

	public ODIDataSubsystemLogMessageFactory(Class<T> odiDataSubsystemLogMessageClass)
	{
		this.odiDataSubsystemLogMessageClass = odiDataSubsystemLogMessageClass;
	}

	public T createMessage(String message, ODIDataSubsystemLogLevel level)
	{
		return this.createMessage(message, null, null, level);
	}

	public T createMessage(String message, Object relevantObject, ODIDataSubsystemLogLevel level)
	{
		return this.createMessage(message, relevantObject, null, level);
	}

	public T createMessage(String message, Throwable t, ODIDataSubsystemLogLevel level)
	{
		return this.createMessage(message, null, t, level);
	}
	
	public T createMessage(String message, Object relevantObject, Throwable t, ODIDataSubsystemLogLevel level)
	{
		//TODO: maybe make a null connection object
		T logMessage = null;
		Class argTypes[] = {String.class, ODIDataSubsystemDataProduct.class, Throwable.class, ODIDataSubsystemLogLevel.class};
		Object args[] = {message, relevantObject, t, level};
		
		try
		{
			logMessage = this.odiDataSubsystemLogMessageClass.getDeclaredConstructor(argTypes).newInstance(args);
		}
		catch (InstantiationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IllegalArgumentException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (InvocationTargetException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NoSuchMethodException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SecurityException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return logMessage;
	}
}
