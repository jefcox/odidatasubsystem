package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;


public abstract class ODIDataSubsystemLogMessage
{
	protected ODIDataSubsystemLogLevel logLevel = null;
	protected String logMessage = null;
	protected ODIDataSubsystemDataProduct dataProduct = null;
	protected String exceptionMessage = null;
	protected StackTraceElement[] stackTrace = null;
	
	ODIDataSubsystemLogMessage(String message, ODIDataSubsystemLogLevel level)
	{
		this.logMessage = message;
		this.logLevel = level;
	}

	ODIDataSubsystemLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemLogLevel level)
	{
		this.logMessage = message;
		this.logLevel = level;
		this.dataProduct = dataProduct;
	}
	
	ODIDataSubsystemLogMessage(String message, Throwable t, ODIDataSubsystemLogLevel level)
	{
		this.logMessage = message;
		this.logLevel = level;
		this.exceptionMessage = t.getMessage();
		this.stackTrace = t.getStackTrace();
	}

	ODIDataSubsystemLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, Throwable t, ODIDataSubsystemLogLevel level)
	{
		this.logMessage = message;
		this.logLevel = level;
		if(t != null)
		{
			this.exceptionMessage = t.getMessage();
			this.stackTrace = t.getStackTrace();
		}
		this.dataProduct = dataProduct;
	}
	
	public ODIDataSubsystemLogLevel getLogLevel()
	{
		return this.logLevel;
	}
	
	/**
	 * @return the logMessage
	 */
	public String getLogMessage()
	{
		return logMessage;
	}

	/**
	 * @param logLevel the logLevel to set
	 */
	public void setLogLevel(ODIDataSubsystemLogLevel logLevel)
	{
		this.logLevel = logLevel;
	}


	/**
	 * @param logMessage the logMessage to set
	 */
	public void setLogMessage(String logMessage)
	{
		this.logMessage = logMessage;
	}
	
	/**
	 * @return the exceptionMessage
	 */
	public String getExceptionMessage()
	{
		return exceptionMessage;
	}

	/**
	 * @param exceptionMessage the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage)
	{
		this.exceptionMessage = exceptionMessage;
	}

	/**
	 * @return the stackTrace
	 */
	public StackTraceElement[] getStackTrace()
	{
		return stackTrace;
	}

	/**
	 * @param stackTrace the stackTrace to set
	 */
	public void setStackTrace(StackTraceElement[] stackTrace)
	{
		this.stackTrace = stackTrace;
	}

	/**
	 * @return the dataProduct
	 */
	public ODIDataSubsystemDataProduct getDataProduct()
	{
		return dataProduct;
	}

	/**
	 * @param dataProduct the dataProduct to set
	 */
	public void setDataProduct(ODIDataSubsystemDataProduct dataProduct)
	{
		this.dataProduct = dataProduct;
	}

	public abstract String toString();
}
