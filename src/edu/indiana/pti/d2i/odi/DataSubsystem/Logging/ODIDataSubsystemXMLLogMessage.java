package edu.indiana.pti.d2i.odi.DataSubsystem.Logging;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;

public class ODIDataSubsystemXMLLogMessage extends ODIDataSubsystemLogMessage
{
	
	ODIDataSubsystemXMLLogMessage(String message, ODIDataSubsystemLogLevel level)
	{
		this(message, null, null, level);
	}

	ODIDataSubsystemXMLLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemLogLevel level)
	{
		this(message, dataProduct, null, level);
	}
	
	ODIDataSubsystemXMLLogMessage(String message, Throwable t, ODIDataSubsystemLogLevel level)
	{
		this(message, null, t, level);
	}

	ODIDataSubsystemXMLLogMessage(String message, ODIDataSubsystemDataProduct dataProduct, Throwable t, ODIDataSubsystemLogLevel level)
	{
		super(message, dataProduct, t, level);
	}
	
	@Override
	public String toString()
	{
		XStream xstream = new XStream(new StaxDriver());
		xstream.alias("ODIDataSubsystemXMLLogMessage", this.getClass());
		String message = xstream.toXML(this);

		return message;
	}
}
