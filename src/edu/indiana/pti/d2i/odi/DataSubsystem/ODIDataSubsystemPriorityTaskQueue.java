package edu.indiana.pti.d2i.odi.DataSubsystem;
import java.util.concurrent.PriorityBlockingQueue;

public class ODIDataSubsystemPriorityTaskQueue extends ODIDataSubsystemTaskQueue
{
	ODIDataSubsystemPriorityTaskQueue()
	{
		this.queue = new PriorityBlockingQueue<ODIDataSubsystemTask>();
	}

}
