package edu.indiana.pti.d2i.odi.DataSubsystem;

public class ODIDataSubsystemTaskWorker extends Thread
{
	private ODIDataSubsystemTaskQueue queue;

	public ODIDataSubsystemTaskWorker(ODIDataSubsystemTaskQueue q)
	{
		this.queue = q;
	}

	public void run()
	{
		while (true)
		{
			try
			{
				ODIDataSubsystemTask task = queue.getTask();
				task.execute();
			} catch (InterruptedException e)
			{
				Thread.currentThread().interrupt();
				break;
			}
		}
	}
}
