package edu.indiana.pti.d2i.odi.DataSubsystem;

import org.apache.log4j.Logger;

import sun.misc.Signal;
import sun.misc.SignalHandler;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class DataSubsystemSignalHandler implements SignalHandler
{
	private SignalHandler	oldHandler;

	public static DataSubsystemSignalHandler handleSignal(String signalStr)
	{

		DataSubsystemSignalHandler handler = new DataSubsystemSignalHandler();
		handler.oldHandler = Signal.handle(new Signal(signalStr), handler);
		return handler;
	}

	public void handle(Signal sig)
	{
		Logger.getRootLogger().info("Caught Signal " + sig);
	
		signalAction(sig);
		// Chain back to previous handler
		if (oldHandler != SIG_DFL && oldHandler != SIG_IGN)
		{
			oldHandler.handle(sig);
		}
	}
	
	private void signalAction(Signal signal)
	{
		if (signal.getName().equals("TERM"))
		{
			ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
			dss.shutdown();
			System.exit(0);
		}
	}
}
