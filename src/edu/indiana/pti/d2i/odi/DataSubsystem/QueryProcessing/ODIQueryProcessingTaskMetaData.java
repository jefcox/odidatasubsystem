package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing;

import java.util.HashMap;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskMetaData;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;

public class ODIQueryProcessingTaskMetaData extends ODIDataSubsystemTaskMetaData
{
	private String	logicalID		= "";
	private String	queryString		= "";
	private HashMap<String, String>	results	= new HashMap<String, String>();

	public ODIQueryProcessingTaskMetaData()
	{
		this.state = ODIDataSubsystemTaskState.Initialized;
	}

	public ODIQueryProcessingTaskMetaData(String status)
	{
		this.status = status;
		this.state = ODIDataSubsystemTaskState.Initialized;
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}

	public String getQueryString()
	{
		return queryString;
	}

	public void setQueryString(String queryString)
	{
		this.queryString = queryString;
	}

	public HashMap<String, String> getResults()
	{
		return results;
	}

	public void setResults(HashMap<String, String> results)
	{
		this.results = results;
	}

	public void updateStatus(String status)
	{
		this.status = status;
		EventStream.getInstance().insertEvent(this);
	}
	
	public String toString()
	{
		String output = "";
		output += "Task: " + this.task + "\n";
		output += "State: " + this.state + "\n";
		output += "Status: " + this.status + "\n";
		output += "Logical ID: " + this.logicalID + "\n";
		output += "Query: " + this.queryString + "\n";
		output += "Results: " + this.results + "\n";

		return output;
	}
}
