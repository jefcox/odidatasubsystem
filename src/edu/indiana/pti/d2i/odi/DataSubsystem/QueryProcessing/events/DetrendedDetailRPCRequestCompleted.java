package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.ODIQueryProcessingSystem;

public class DetrendedDetailRPCRequestCompleted extends ODIDataSubsystemComplexEvent
{
	private ODIQueryProcessingSystem qps = ODIQueryProcessingSystem.getInstance();
	private String queueName = "";
	private String correlationID;
	
	public DetrendedDetailRPCRequestCompleted(String queueName, String correlationID, String logicalID)
	{
		this.queueName = queueName;
		this.correlationID = correlationID;
		this.expression = "select * from " + FoundDetrendedDetailByID.class.getSimpleName();
		this.expression += " where FoundDetrendedDetailByID.logicalID = '" + logicalID + "'";
		this.name = DetrendedDetailRPCRequestCompleted.class.getCanonicalName() + "(" + logicalID +") (" + queueName +") ("+ correlationID +")";
	}
	
	public void update(FoundDetrendedDetailByID event)
	{
		qps.publishDetrendedDetails(this.queueName, this.correlationID, event.getXmlResultString());
		EventStream.getInstance().stopDetect(this);
	}
}
