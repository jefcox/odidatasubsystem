package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing;

import java.util.List;

import javax.xml.transform.dom.DOMSource;

import uk.ac.starlink.votable.VOTableBuilder;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemQueueingModule;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events.DetrendedDetailRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events.FoundDetrendedDetailByID;

public class ODIQueryProcessingSystem extends ODIDataSubsystemQueueingModule
{
	private static ODIQueryProcessingSystem	instance		= null;
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	private ODIQueryProcessingSystem()
	{
		super(ODIQueryProcessingSystem.class.getSimpleName());
	}

	private void detectEvents()
	{
		DetrendedDetailRequest detrendedDetailRequest = new DetrendedDetailRequest();
		FoundDetrendedDetailByID detrendedDetailResult = new FoundDetrendedDetailByID();
		EventStream.getInstance().detect(detrendedDetailRequest);
		EventStream.getInstance().detect(detrendedDetailResult);
	}

	public static void start()
	{
		if (instance == null)
		{
			instance = new ODIQueryProcessingSystem();
			instance.detectEvents();
			instance.mqConnection.consume(instance.props.getDetrendedDetailQueue());
		}
	}

	public static synchronized ODIQueryProcessingSystem getInstance()
	{
		if (instance == null)
			start();

		return instance;
	}

	public ODIDataSubsystemLogger getLogger()
	{
		return super.getLogger();
	}
	
	public void publishDetrendedDetails(String queueName, String correlationId, String message)
	{
		this.mqConnection.publishToQueue(queueName, correlationId, message);
	}

	public void findDetrendedDetailByID(String logicalID)
	{
		FindDetrendedDetailByID findDetrendedDetail = new FindDetrendedDetailByID(logicalID);
		taskQueue.addTask(findDetrendedDetail);
	}
	
	public void resolveExposureTable(List<String> nights, List<String> subsets)
	{
		VOTableBuilder votBuilder = new VOTableBuilder();
		//this votFile needs to be read in from the defaultFile. Maybe even put this votable stuff somewhere else, just retrieve the
		// data necessary to build the file from here. Then post an event
		
		DOMSource votFile;
//		VOElement top = new VOElementFactory().makeVOElement( votFile );
	
	}

	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}
}
