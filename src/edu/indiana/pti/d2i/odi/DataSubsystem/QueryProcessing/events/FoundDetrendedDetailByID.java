package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events;

import java.util.Map;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.ODIQueryProcessingTaskMetaData;

public class FoundDetrendedDetailByID extends ODIDataSubsystemComplexEvent
{
	private String xmlResultString = "";
	private String logicalID = "";
	
	public FoundDetrendedDetailByID()
	{
		this.name = FoundDetrendedDetailByID.class.getCanonicalName();
		this.expression = "select * from ODIQueryProcessingTaskMetaData ";
		this.expression += " where ODIQueryProcessingTaskMetaData.state.toString() = '"+ ODIDataSubsystemTaskState.Completed.toString() +"'";
	}
	
	public void update(ODIQueryProcessingTaskMetaData event)
	{
		this.logicalID = event.getLogicalID();
		xmlResultString = "<Details>";
		xmlResultString += "<RawLogicalID>" + event.getLogicalID() + "</RawLogicalID>";
		
        for (Map.Entry<String,String> result : event.getResults().entrySet()) 
        {
        	String tag = result.getKey();
       		String value = result.getValue();
       		xmlResultString += "<" + tag + ">" + value + "</" + tag + ">";
        }

		xmlResultString += "</Details>";
		EventStream.getInstance().insertEvent(this);
	}

	public String getXmlResultString()
	{
		return xmlResultString;
	}

	public void setXmlResultString(String xmlResultString)
	{
		this.xmlResultString = xmlResultString;
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}
}
