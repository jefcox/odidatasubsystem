package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.ODIQueryProcessingSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class DetrendedDetailRequest extends ODIDataSubsystemComplexEvent
{
	ODIQueryProcessingSystem qps = ODIQueryProcessingSystem.getInstance();
	ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();
	
	public DetrendedDetailRequest()
	{
		this.name = DetrendedDetailRequest.class.getCanonicalName();
		this.expression = "select * from RabbitMQMessage ";
		this.expression += "where RabbitMQMessage.envelope.routingKey = '"+ props.getDetrendedDetailQueue() + "'";
	}

	public void update(RabbitMQMessage event)
	{
		ODIQueryProcessingSystem qps = ODIQueryProcessingSystem.getInstance();
		String message = new String(event.getBody());
		String queueName = event.getProperties().getReplyTo();
		String correlationID = event.getProperties().getCorrelationId();

		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			//TODO: put this logicalID string as something configurable
			List<String> logicalIDs = XMLUtil.getTextValues(request, "LogicalID");
			
			for (String logicalID : logicalIDs)
			{
				DetrendedDetailRPCRequestCompleted ddRPC = new DetrendedDetailRPCRequestCompleted(queueName, correlationID, logicalID);
				EventStream.getInstance().detect(ddRPC);
				qps.findDetrendedDetailByID(logicalID);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
