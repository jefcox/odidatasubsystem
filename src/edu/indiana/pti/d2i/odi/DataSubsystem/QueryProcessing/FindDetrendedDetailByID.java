package edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing;

import java.util.HashMap;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemAbstractTask;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;

public class FindDetrendedDetailByID extends ODIDataSubsystemAbstractTask
{
	private String							logicalID	= "";
	private ODIQueryProcessingTaskMetaData	metaData	= new ODIQueryProcessingTaskMetaData();

	public FindDetrendedDetailByID(String logicalID)
	{
		this.logicalID = logicalID;
//		super.metaData = this.metaData;
		this.metaData.setLogicalID(logicalID);
		this.metaData.setTask(this.getClass().getCanonicalName());
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}

	@Override
	public void execute()
	{
		HashMap<String, String> results = new HashMap<String, String>();
		results.put("CalibrationXYZLogicalID","adfadfasdfadhsklj");
		results.put("Calibration123LogicalID","ewurnuerlsad");
		results.put("Calibration456LogicalID","asawesagas");
		
		this.metaData.setResults(results);
		this.metaData.setStatus("OK");
		this.metaData.updateState(ODIDataSubsystemTaskState.Completed);
	}
}
