package edu.indiana.pti.d2i.odi.DataSubsystem;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;

public abstract class ODIDataSubsystemAbstractTask implements ODIDataSubsystemTask
{

	protected ODIDataSubsystemTaskHook		hook		= new ODIDataSubsystemNullTaskHook();
	protected ODIDataSubsystemTaskState		state		= null;
	protected ODIDataSubsystemTaskStatus	status		= ODIDataSubsystemTaskStatus.OK;
	protected int							priority	= 0;

	public abstract void execute();

	public void onStart()
	{
		hook.onStart();
		if (this.getState() != ODIDataSubsystemTaskState.Restarted)
			this.updateState(ODIDataSubsystemTaskState.Started);
//		this.updateState(ODIDataSubsystemTaskState.Working);
	}

	public void onComplete()
	{
		hook.onComplete();
		this.updateState(ODIDataSubsystemTaskState.Completed);
	}

	public int getPriority()
	{
		return priority;
	}

	public void setPriority(int priority)
	{
		this.priority = priority;
	}

	public ODIDataSubsystemTaskState getState()
	{
		return state;
	}

	public void setState(ODIDataSubsystemTaskState state)
	{
		this.state = state;
	}

	public void updateState(ODIDataSubsystemTaskState state)
	{
		this.state = state;
		EventStream.getInstance().insertEvent(this);
	}

	public ODIDataSubsystemTaskStatus getStatus()
	{
		return status;
	}

	public void setStatus(ODIDataSubsystemTaskStatus status)
	{
		this.status = status;
	}

	public void updateStatus(ODIDataSubsystemTaskStatus status)
	{
		this.status = status;
		EventStream.getInstance().insertEvent(this);
	}

	public String toString()
	{
		String output = "";
		output += "Task: " + this.getClass().getSimpleName() + "\n";
		output += "State: " + this.state + "\n";
		output += "Status: " + this.status + "\n";
		return output;
	}

}
