package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.IRODSResource;
import edu.sdsc.grid.io.irods.IRODSFileSystem;

public abstract class ODIDataSubsystemDataProduct
{
	private String					id;
	private String					localPath		= "";
	private String					archivePath		= "";
	private String					archiveDir		= "";
	private String					archiveFileName	= "";
	private String					checksum		= "";
	private String					cacheResource	= "";
	private String					archiveResource	= "";
	private HashMap<String, String>	replicas		= new HashMap<String, String>();
	private boolean					empty			= true;

	public ODIDataSubsystemDataProduct(String id)
	{
		this(id, null, null, null, null);
	}

	public ODIDataSubsystemDataProduct(String id, String localPath)
	{
		this(id, localPath, null, null, null);
	}

	public ODIDataSubsystemDataProduct(String id, String localPath, String checksum)
	{
		this(id, localPath, checksum, null, null);
	}

	public ODIDataSubsystemDataProduct(String id, String localPath, String cacheResource, String archiveResource)
	{
		this(id, localPath, null, cacheResource, archiveResource);
	}

	public ODIDataSubsystemDataProduct(String id, String localPath, String checksum, String cacheResource, String archiveResource)
	{
		this.id = id;
		this.localPath = localPath;
		this.checksum = checksum;
		this.cacheResource = cacheResource;
		this.archiveResource = archiveResource;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLocalPath()
	{
		return localPath;
	}

	public void setLocalPath(String localPath)
	{
		this.localPath = localPath;
	}

	public String getArchivePath()
	{
		return archivePath;
	}

	public void setArchivePath(String localPath)
	{
		this.archivePath = localPath;
	}

	public String getArchiveDir()
	{
		return archiveDir;
	}

	public void setArchiveDir(String archivePath)
	{
		this.archiveDir = archivePath;
	}

	public String getArchiveFileName()
	{
		return this.archiveFileName;
	}

	public void setArchiveFileName(String fileName)
	{
		this.archiveFileName = fileName;
	}

	public String getChecksum()
	{
		return this.checksum;
	}

	public void setChecksum(String checksum)
	{
		this.checksum = checksum;
	}

	public String getCacheResourceName()
	{
		return cacheResource;
	}

	public IRODSResource getCacheResource() throws NullPointerException, IOException
	{
		IRODSResource cacheResource = null;
		if (this.cacheResource != null)
			cacheResource = new IRODSResource(new IRODSFileSystem(IRODSUtil.getIRODSAccount()), this.cacheResource);
		return cacheResource;
	}

	public void setCacheResource(String cacheResource)
	{
		this.cacheResource = cacheResource;
		this.empty = false;
	}

	public String getArchiveResource()
	{
		return archiveResource;
	}

	public void setArchiveResource(String archiveResource)
	{
		this.archiveResource = archiveResource;
		this.empty = false;
	}

	public List<String> getReplicaNames()
	{
		List<String> replicaNames = new ArrayList<String>();
		for (String replicaName : this.replicas.keySet())
			replicaNames.add(replicaName);

		Collections.sort(replicaNames);
		return replicaNames;
	}
	
	public HashMap<String, String> getReplicas()
	{
		return this.replicas;
	}
	
	public String getReplicaNum(String replicaName)
	{
		return this.replicas.get(replicaName);
	}

	public void setReplicas(HashMap<String, String> replicas)
	{
		this.replicas = replicas;
		this.empty = false;
	}

	public boolean isCached()
	{
		boolean isCached = false;
		if (Collections.binarySearch(this.getReplicaNames(), this.cacheResource) > -1)
			isCached = true;

		return isCached;
	}

	protected abstract boolean verifyCache();

	public boolean isArchived()
	{
		boolean retValue = false;
		if (Collections.binarySearch(this.getReplicaNames(), this.archiveResource) > -1)
		{
			retValue = true;
		}

		return retValue;
	}

	public boolean isEmpty()
	{
		return empty;
	}

	public String toString()
	{
		String dpString = " [ id: " + this.getId() + " ] ";
		dpString += " [ localPath: " + this.getLocalPath() + " ] ";
		dpString += " [ archiveDir: " + this.getArchiveDir() + " ] ";
		dpString += " [ archiveResource: " + this.getArchiveResource() + " ] ";
		dpString += " [ cacheResource: " + this.getCacheResourceName() + " ] ";
		dpString += " [ checksum: " + this.getChecksum() + " ] ";
		return dpString;
	}
}
