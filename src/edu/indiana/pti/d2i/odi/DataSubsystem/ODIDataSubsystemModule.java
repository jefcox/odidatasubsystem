package edu.indiana.pti.d2i.odi.DataSubsystem;

import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemFileLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogPublisher;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemMessageLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnection;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageQueueConnectionFactory;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.MessageUtil;

public class ODIDataSubsystemModule
{
	protected ODIDataSubsystemLogPublisher										log					= null;
	protected ODIDataSubsystemProperties										props				= null;
	protected MessageQueueConnectionFactory<? extends MessageQueueConnection>	mqConnectionFactory	= null;
	protected MessageQueueConnection											mqConnection		= null;
	protected String															moduleName			= "ODIDataSubsystemModule";

	protected ODIDataSubsystemModule(String moduleName)
	{
		this.moduleName = moduleName;
		this.props = ODIDataSubsystemProperties.getProperties();
		ODIDataSubsystemLogger messageLogger = ODIDataSubsystemMessageLogger.getLogger(props.getLoggerName(moduleName));
		ODIDataSubsystemLogger fileLogger = ODIDataSubsystemFileLogger.getLogger(props.getLoggerName(moduleName));
		this.log = ODIDataSubsystemLogPublisher.getLogger();
		this.log.addSubscriber("messageLogger", messageLogger);
		this.log.addSubscriber("fileLogger", fileLogger);
		this.mqConnectionFactory = MessageUtil.getMQConnectionFactory(props.getMessageServerType());
		this.mqConnection = this.mqConnectionFactory.getConnection(props.getMessageServerHost(), props.getMessageServerUserName(),
				props.getMessageServerPassword(), props.getMessageServerVirtualHost());
	}

	protected ODIDataSubsystemLogger getLogger()
	{
		return this.log;
	}

	public void acknowledgeMessage(long deliveryTag, boolean allMessages)
	{
		this.mqConnection.acknowledgeMessage(deliveryTag, allMessages);
	}

	public void rejectMessage(long deliveryTag, boolean allMessages, boolean requeue)
	{
		this.mqConnection.rejectMessage(deliveryTag, allMessages, requeue);
	}

	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}
}
