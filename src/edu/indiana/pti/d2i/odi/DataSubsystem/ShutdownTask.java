package edu.indiana.pti.d2i.odi.DataSubsystem;

public class ShutdownTask extends ODIDataSubsystemAbstractTask
{
	ODIDataSubsystemTaskWorker worker;
	
	public ShutdownTask(ODIDataSubsystemTaskWorker worker)
	{
		this.worker = worker;
	}
	
	@Override
	public void execute() 
	{
		worker.interrupt();
	}
}
