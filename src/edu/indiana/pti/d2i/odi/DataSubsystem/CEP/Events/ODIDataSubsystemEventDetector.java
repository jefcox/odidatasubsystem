package edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;

public abstract class ODIDataSubsystemEventDetector
{
	public abstract void update(Object event);
	public abstract String getName();
	public abstract void setName(String name);
	public abstract EPStatement getStatement();
	public abstract void setStatement(EPStatement statement);
	public abstract String getExpression();
	public abstract void setExpression(String expression);
	public abstract void addListener(UpdateListener listener);
}
