package edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Listeners;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;

public class LogEventListener implements UpdateListener
{
	private ODIDataSubsystemLogger	log	= null;

	public LogEventListener(ODIDataSubsystemLogger log)
	{
		this.log = log;
	}

	@Override
	public void update(EventBean[] newEvents, EventBean[] oldEvents)
	{
		if (newEvents != null)
			for (EventBean event : newEvents)
			{
				log.debug("Detected event: " + event.getUnderlying().getClass().getCanonicalName());
				log.debug(event.getUnderlying().toString());
			}
	}

}
