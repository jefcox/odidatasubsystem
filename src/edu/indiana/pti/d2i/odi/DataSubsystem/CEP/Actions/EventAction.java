package edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Actions;

public interface EventAction
{
	public void execute();
}
