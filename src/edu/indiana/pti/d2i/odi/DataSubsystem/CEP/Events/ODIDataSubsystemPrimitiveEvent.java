package edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;

import edu.indiana.pti.d2i.odi.DataSubsystem.NullEPStatement;

public abstract class ODIDataSubsystemPrimitiveEvent extends ODIDataSubsystemEventDetector
{
	String		name					= ODIDataSubsystemPrimitiveEvent.class.getCanonicalName();
	String		expressionDetectedBy	= null;
	EPStatement	statementDetectedBy		= new NullEPStatement();

	public void update(Object event)
	{
		
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public EPStatement getStatement()
	{
		return this.statementDetectedBy;
	}

	public void setStatement(EPStatement statement)
	{
		this.statementDetectedBy = statement;
	}

	public String getExpression()
	{
		return this.expressionDetectedBy;
	}

	public void setExpression(String expression)
	{
		this.expressionDetectedBy = expression;
	}

	public void addListener(UpdateListener listener)
	{
		statementDetectedBy.addListener(listener);
	}
}
