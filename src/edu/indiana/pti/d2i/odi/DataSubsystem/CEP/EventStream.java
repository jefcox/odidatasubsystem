package edu.indiana.pti.d2i.odi.DataSubsystem.CEP;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Node;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPRuntime;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemModule;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Listeners.LogEventListener;

public class EventStream extends ODIDataSubsystemModule
{
	private static EventStream										instance	= null;
	private EPServiceProvider										epService	= null;
	private EPRuntime												epRuntime	= null;
	private ConcurrentHashMap<String, ODIDataSubsystemComplexEvent>	events		= new ConcurrentHashMap<String, ODIDataSubsystemComplexEvent>();

	private EventStream()
	{
		super(EventStream.class.getSimpleName());

		Configuration config = new Configuration();
		config.addEventTypeAutoName("edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events");
		config.addEventTypeAutoName("edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage");
		config.addEventTypeAutoName("edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events");
		config.addEventTypeAutoName("edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.events");
		config.addEventType("RabbitMQMessage","edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage");
		config.addEventType("ODIQueryProcessingTaskMetaData", "edu.indiana.pti.d2i.odi.DataSubsystem.QueryProcessing.ODIQueryProcessingTaskMetaData");
		this.epService = EPServiceProviderManager.getProvider("ODIEventStream", config);
		
	}

	public static void Start()
	{
		if (instance == null)
			instance = new EventStream();
	}

	public static EventStream getInstance()
	{
		if (instance == null)
			Start();

		return instance;
	}

	public String[] getStatementNames()
	{
		return this.epService.getEPAdministrator().getStatementNames();
	}

	public EPStatement getStatement(String name)
	{
		return this.epService.getEPAdministrator().getStatement(name);
	}
	
	public Set<String> getRegisteredEventNames()
	{
		return this.events.keySet();
	}
	
	public void detect(ODIDataSubsystemComplexEvent event)
	{

		ODIDataSubsystemComplexEvent oldEvent = events.get(event.getName());
		if (oldEvent == null)
		{
			if (event.getExpression() != null)
			{
				EPStatement statement = this.epService.getEPAdministrator().createEPL(event.getExpression());
				statement.setSubscriber(event);

				if (this.log.isDebugEnabled())
					statement.addListener(new LogEventListener(log));

				event.setStatement(statement);
				events.put(event.getName(), event);
				log.info("Started Detecting Event: [ " + event.getName() + "] Expression: [" + event.getExpression() + "]");
			}
		}
		else
		{
			oldEvent.getStatement().start();
			log.info("Event: [" + oldEvent.getExpression() + "] detection started");

		}
	}

	public void stopDetect(ODIDataSubsystemComplexEvent event)
	{
		ODIDataSubsystemComplexEvent oldEvent = events.get(event.getName());
		if (oldEvent != null)
		{
			//TODO: figure out why stop creates additional events, currently just use destroy
			oldEvent.getStatement().destroy();
			//			oldEvent.getStatement().stop();
			events.remove(event.getName());
			log.debug("Stopped: " + event.getName());

			log.info("Event: [" + oldEvent.getExpression() + "] detection stopped and event destroyed");
		}
	}

	public void remove(ODIDataSubsystemComplexEvent event)
	{
		ODIDataSubsystemComplexEvent oldEvent = events.get(event.getName());
		if (oldEvent != null)
		{
			//TODO: figure out why stop creates additional events
			//			oldEvent.getStatement().stop();
			events.remove(oldEvent.getName());
			oldEvent.getStatement().destroy();
		}
	}

	public void insertEvent(Object event)
	{
		this.epService.getEPRuntime().sendEvent(event);
		log.debug("Event: "  + event.toString() + " inserted into event stream");
	}

	public void insertEvent(Node primativeEvent)
	{
		this.epService.getEPRuntime().sendEvent(primativeEvent);
		log.debug("Event: "  + primativeEvent.toString() + " inserted into event stream");
	}
}
