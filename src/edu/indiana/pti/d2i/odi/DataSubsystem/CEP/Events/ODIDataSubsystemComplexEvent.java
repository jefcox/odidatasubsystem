package edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events;

import java.util.UUID;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.UpdateListener;

import edu.indiana.pti.d2i.odi.DataSubsystem.NullEPStatement;

public abstract class ODIDataSubsystemComplexEvent extends ODIDataSubsystemEventDetector
{
	protected String		ID			= UUID.randomUUID().toString();
	protected String		name		= ODIDataSubsystemComplexEvent.class.getCanonicalName();
	protected String		expression	= null;
	protected EPStatement	statement	= new NullEPStatement();

	public void update(Object event)
	{
		
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public EPStatement getStatement()
	{
		return this.statement;
	}

	public void setStatement(EPStatement statement)
	{
		this.statement = statement;
	}

	public String getExpression()
	{
		return this.expression;
	}

	public void setExpression(String expression)
	{
		this.expression = expression;
	}

	public void addListener(UpdateListener listener)
	{
		statement.addListener(listener);
	}

	public String getID()
	{
		return ID;
	}

	public void setID(String iD)
	{
		ID = iD;
	}
}
