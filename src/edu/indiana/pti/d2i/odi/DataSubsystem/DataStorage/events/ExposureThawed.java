package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;

public class ExposureThawed extends ArchiveSetThawed
{
	String responseQueueName = "";
	String correlationID = "";

	public ExposureThawed(String responseQueueName, String correlationID, List<String> logicalIDs)
	{
		super(logicalIDs);
		this.xmlString = "<Response>";
		this.xmlString += "<Files>";
		this.responseQueueName = responseQueueName;
		this.correlationID = correlationID;
	}
	
	public void update(ArchiveThawed event)
	{
		this.xmlString += event.getXmlString();
		this.logicalIDs.remove(event.getLogicalID());
		if (logicalIDs.isEmpty())
		{
			this.xmlString += "</Files>";
			this.xmlString += "</Response>";
			EventStream.getInstance().insertEvent(this);
		}
	}


}
