package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;

public class DataProductSetArchived extends ODIDataSubsystemComplexEvent
{
	protected String xmlStart = "<Files>";
	protected String xmlEnd = "</Files>";
	protected String xmlString = "";
	protected int numFilesToArchive = 0;
	protected int numFilesArchivedSuccessfully = 0;
	private String setType = "";
	private List<String> logicalIDs = null;

	public DataProductSetArchived(List<String> logicalIDs)
	{
		this.name = DataProductSetArchived.class.getCanonicalName();
		this.logicalIDs = logicalIDs;
		this.numFilesToArchive = logicalIDs.size();
		int idCount = this.numFilesToArchive;
		this.expression = "select * from " + DataProductArchived.class.getSimpleName();
		this.expression += " where "+ DataProductArchived.class.getSimpleName() +".logicalID ";
		
		if (idCount == 1)
		{
			this.expression += "= '" + logicalIDs.get(0) + "'";
		}
		else
		{
			this.expression += "IN (";
			for (String logicalID : logicalIDs)
			{
				this.expression += "'" + logicalID + "'";
				idCount--;
				if (idCount > 0)
					this.expression += ",";
			}
			this.expression += ") ";
		}

		this.name =  this.expression;
	}
	
	public void update(DataProductArchived event)
	{
		this.xmlString += event.getXmlString();
		this.logicalIDs.remove(event.getLogicalID());
		
		if(event.getStatus() == ODIDataSubsystemTaskStatus.OK && event.getState() == ODIDataSubsystemTaskState.Completed)
			this.numFilesArchivedSuccessfully++;
		
		if(logicalIDs.isEmpty())
		{
			String xmlFinal = this.xmlStart + this.xmlString + this.xmlEnd;
			this.xmlString = xmlFinal;
			EventStream.getInstance().insertEvent(this);
			EventStream.getInstance().stopDetect(this);
		}
	}

	public String getXmlString()
	{
		return xmlString;
	}

	public void setXmlString(String xmlString)
	{
		this.xmlString = xmlString;
	}

	public String getSetType()
	{
		return setType;
	}

	public void setSetType(String setType)
	{
		this.setType = setType;
	}
	
	public boolean fullSetArchived()
	{
		return (this.numFilesToArchive == this.numFilesArchivedSuccessfully);
	}
}
