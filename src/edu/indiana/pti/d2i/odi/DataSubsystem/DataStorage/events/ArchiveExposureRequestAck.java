package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposure;

public class ArchiveExposureRequestAck extends ODIDataSubsystemComplexEvent
{
	long deliveryTag = 0;
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();

	public ArchiveExposureRequestAck(long deliveryTag, ODIDataSubsystemExposure exposure)
	{
		this.deliveryTag = deliveryTag;
		this.name = ArchiveExposureRequestAck.class.getCanonicalName() + "|" + deliveryTag;
		this.expression = "select * from " + ArchiveExposureComplete.class.getSimpleName();
		this.expression += " where " + ArchiveExposureComplete.class.getSimpleName() + ".exposureId  = '" + exposure.getId() + "'";
	}
	
	public void update(ArchiveExposureComplete exposureComplete)
	{
		dss.acknowledgeMessage(deliveryTag, false);
		EventStream.getInstance().stopDetect(this);
	}
}
