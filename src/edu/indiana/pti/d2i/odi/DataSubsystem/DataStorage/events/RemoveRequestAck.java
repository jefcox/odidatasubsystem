package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class RemoveRequestAck extends ODIDataSubsystemComplexEvent
{
	long deliveryTag = 0;
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();

	public RemoveRequestAck(long deliveryTag, String requestSetId)
	{
		this.deliveryTag = deliveryTag;
		this.name = RemoveRequestAck.class.getCanonicalName() + "|" + deliveryTag;
		this.expression = "select * from " + RemoveRequestComplete.class.getSimpleName();
		this.expression += " where " + RemoveRequestComplete.class.getSimpleName() + ".ID  = '" + requestSetId + "'";
	}
	
	public void update(RemoveRequestComplete removeRequestComplete)
	{
		dss.acknowledgeMessage(deliveryTag, false);
		EventStream.getInstance().stopDetect(this);
	}
}
