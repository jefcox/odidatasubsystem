package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposure;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposureFile;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class ArchiveExposureRequest extends ODIDataSubsystemComplexEvent
{
	ODIDataStorageSystem	dss	= ODIDataStorageSystem.getInstance();
	ODIDataSubsystemProperties props = null;

	public ArchiveExposureRequest() 
	{
		props =  ODIDataSubsystemProperties.getProperties();
		this.name = ArchiveExposureRequest.class.getCanonicalName();
		this.expression = "select * from " + RabbitMQMessage.class.getSimpleName();
		this.expression += " where RabbitMQMessage.envelope.routingKey = '" + props.getArchiveRequestQueue() + "'";
	}

	public void update(RabbitMQMessage event)
	{
		String message = new String(event.getBody());
		
		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			NodeList nodeList = request.getElementsByTagName("IncomingRawExposure");
			for (int i = 0; nodeList != null && i < nodeList.getLength(); i++)
			{
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;

					String exposureID = XMLUtil.getTextValue(element, "ID");
					String numOTAs = XMLUtil.getTextValue(element, "OTACount");
					
					ODIDataSubsystemExposure exposure = new ODIDataSubsystemExposure(exposureID, exposureID);
					
					NodeList otas = element.getElementsByTagName("OTA");
					for (int j = 0; otas != null && j < otas.getLength(); j++)
					{
						Node ota = otas.item(j);
						if (ota.getNodeType() == Node.ELEMENT_NODE)
						{
							Element otaElement = (Element) ota;
							String file = XMLUtil.getTextValue(otaElement, "File");
							String logicalID = XMLUtil.getTextValue(otaElement, "ID");
							String checksum = XMLUtil.getTextValue(otaElement, "MD5");
							ODIDataSubsystemExposureFile exposureFile = new ODIDataSubsystemExposureFile(exposure, logicalID, file, checksum);
							exposure.addFile(exposureFile);
						}
					}
					
					ArchiveExposureRequestAck requestAck = new ArchiveExposureRequestAck(event.getEnvelope().getDeliveryTag(), exposure);
					EventStream.getInstance().detect(requestAck);
					dss.archiveExposure(exposure);
				}
			}
		}
		catch (IOException e)
		{
			String errorMessage = e.getClass().getSimpleName() + " caught processing message : ";
			errorMessage += "[ " + message +" ] ";
			errorMessage += " in " + this.getClass().getSimpleName() + " ";
			errorMessage += "[ " + message +" ] ";
			dss.getLogger().error(errorMessage, e);
			dss.rejectMessage(event.getEnvelope().getDeliveryTag(), false, false);
		}
		catch (ParserConfigurationException e)
		{
			String errorMessage = e.getClass().getSimpleName() + " caught processing message : ";
			errorMessage += "[ " + message +" ] ";
			errorMessage += " in " + this.getClass().getSimpleName() + " ";
			errorMessage += "[ " + message +" ] ";
			dss.getLogger().error(errorMessage, e);
			dss.rejectMessage(event.getEnvelope().getDeliveryTag(), false, false);
		}
		catch (SAXException e)
		{
			String errorMessage = e.getClass().getSimpleName() + " caught processing message : ";
			errorMessage += "[ " + message +" ] ";
			errorMessage += " in " + this.getClass().getSimpleName() + " ";
			errorMessage += "[ " + message +" ] ";
			dss.getLogger().error(errorMessage, e);
			dss.rejectMessage(event.getEnvelope().getDeliveryTag(), false, false);
		}
	}
}
