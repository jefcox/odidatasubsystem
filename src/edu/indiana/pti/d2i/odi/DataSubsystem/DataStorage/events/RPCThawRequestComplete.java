package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;

public class RPCThawRequestComplete extends ODIDataSubsystemComplexEvent
{
	private ODIDataStorageSystem	dss				= ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemLogger	log				= dss.getLogger();
	private String					queueName		= "";
	private String					correlationId	= "";

	public RPCThawRequestComplete(String requestSetId, String queueName, String correlationId)
	{
		this.queueName = queueName;
		this.correlationId = correlationId;
		this.expression = "Select * from " + ArchiveSetThawed.class.getSimpleName();
		this.expression += " where ArchiveSetThawed.ID = '" + requestSetId + "'";
		this.name = this.expression;
	}

	public void update(ArchiveSetThawed archiveSetThawed)
	{
		String message = "";
		message += "<Response>";
		message += archiveSetThawed.getXmlString();
		message += "</Response>";
		log.debug("RPC Thaw Request Complete-> properties[ replyTo[" + queueName + "] correlationId[" + correlationId + "]]" + " message[ " + message + "]");
		dss.respondToRPC(this.queueName, this.correlationId, message);
		EventStream.getInstance().insertEvent(this);
		EventStream.getInstance().stopDetect(this);
	}
}
