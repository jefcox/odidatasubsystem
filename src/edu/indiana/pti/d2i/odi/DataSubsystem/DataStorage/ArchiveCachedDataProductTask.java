package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;

public class ArchiveCachedDataProductTask extends ODIDataStorageAbstractTask
{

	public ArchiveCachedDataProductTask(ODIDataSubsystemDataProduct dataProduct)
	{
		super(dataProduct);
	}

	public ArchiveCachedDataProductTask(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook)
	{
		super(dataProduct, hook);
	}

	@Override
	public void execute()
	{
		this.onStart();
		IRODSFileSystem irodsFS = null;

		dataProduct = IRODSUtil.getDataProduct(this.dataProduct.getId(), log);

		if (!dataProduct.isEmpty())
		{
			if (!dataProduct.isArchived())
			{
				try
				{
					irodsFS = IRODSUtil.getIRODSFileSystem();
					IRODSFile cacheFile = new IRODSFile(irodsFS, dataProduct.getArchiveFileName());

					long startTime = System.nanoTime();
					cacheFile.replicate(dataProduct.getArchiveResource());
					long endTime = System.nanoTime();

					IRODSFile archiveFile = new IRODSFile(irodsFS, dataProduct.getArchiveFileName());
					this.updateStatus(ODIDataSubsystemTaskStatus.OK);

					archiveFile.setResource(dataProduct.getArchiveResource());
					if (archiveFile.checksum().compareTo(cacheFile.checksum()) != 0)
						this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);

					log.info("[ " + cacheFile.getName() + " ] replicated to [ " + dataProduct.getArchiveResource() + " ] in [ " + (endTime - startTime)
							/ 1000000000.0 + " seconds] with status [ " + this.getStatus() + " ]");
				}
				catch (IOException e)
				{
					e.printStackTrace();
					this.onComplete();
				}
			}
		}
		else
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.ARCHIVE_NOT_FOUND);
		}

		this.onComplete();
	}

}
