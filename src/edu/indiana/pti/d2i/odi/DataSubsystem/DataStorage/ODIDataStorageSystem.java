package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemQueueingModule;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ArchiveExposureComplete;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ArchiveExposureRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ArchiveRemoved;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ArchiveThawed;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.DataProductArchived;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.DataStorageTaskStatusUpdated;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ExposureArchived;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ExposureTransferStarted;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.RPCThawRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.RemoveRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ThawRequest;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events.ThawStarted;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.local.LocalFile;

public class ODIDataStorageSystem extends ODIDataSubsystemQueueingModule
{

	private static ODIDataStorageSystem	instance	= null;

	private ODIDataStorageSystem()
	{
		super(ODIDataStorageSystem.class.getSimpleName());

	}

	private void detectEvents()
	{
		ArchiveExposureRequest archiveRequest = new ArchiveExposureRequest();
		ThawRequest thawRequest = new ThawRequest();
		RemoveRequest removeRequest = new RemoveRequest();
		RPCThawRequest rpcThawRequest = new RPCThawRequest();

		DataProductArchived fileArchived = new DataProductArchived();
		ExposureArchived exposureArchived = new ExposureArchived();
		ThawStarted fileThawStarted = new ThawStarted();
		ArchiveThawed fileThawed = new ArchiveThawed();
		ArchiveRemoved fileRemoved = new ArchiveRemoved();

		ExposureTransferStarted exposureTransferStarted = new ExposureTransferStarted();
		DataStorageTaskStatusUpdated taskStatusUpdate = new DataStorageTaskStatusUpdated();

		EventStream.getInstance().detect(archiveRequest);
		EventStream.getInstance().detect(thawRequest);
		EventStream.getInstance().detect(removeRequest);
		EventStream.getInstance().detect(rpcThawRequest);

		EventStream.getInstance().detect(fileArchived);
		EventStream.getInstance().detect(exposureArchived);
		EventStream.getInstance().detect(fileRemoved);
		EventStream.getInstance().detect(fileThawStarted);
		EventStream.getInstance().detect(fileThawed);
		EventStream.getInstance().detect(exposureTransferStarted);
		EventStream.getInstance().detect(taskStatusUpdate);
	}

	private void bindQueues()
	{
		this.mqConnection.bindQueueToExchange(props.getFileArchiveStatusQueue(), props.getDataSubsystemExchange(), props.getFileArchiveStatusQueue(), true);
		this.mqConnection.bindQueueToExchange(props.getThawRequestQueue(), props.getDataSubsystemExchange(), props.getThawRequestQueue(), true);
		this.mqConnection.bindQueueToExchange(props.getRemoveRequestQueue(), props.getDataSubsystemExchange(), props.getRemoveRequestQueue(), true);
		this.mqConnection.bindQueueToExchange(props.getArchiveRequestQueue(), props.getDataSubsystemExchange(), props.getArchiveRequestQueue(), true);
	}

	private void consumeMessages()
	{
		this.mqConnection.consume(props.getThawRequestQueue());
		this.mqConnection.consume(props.getRemoveRequestQueue());
		this.mqConnection.consume(props.getArchiveRequestQueue());
		this.mqConnection.consume(props.getInstrumentOutBound());
	}

	public static void start()
	{
		if (instance == null)
		{
			instance = new ODIDataStorageSystem();
			instance.bindQueues();
			instance.detectEvents();
			instance.consumeMessages();
		}
	}

	public static synchronized ODIDataStorageSystem getInstance()
	{
		if (instance == null)
			start();

		return instance;
	}

	public ODIDataSubsystemLogger getLogger()
	{
		return super.getLogger();
	}

	public void publishTopic(String message, String topic)
	{
		this.mqConnection.publishToExchange(props.getDataSubsystemExchange(), message, topic, true);
	}

	public void publishRemoveRequestComplete(String message)
	{
		this.publishTopic(message, "admin.remove.request.complete");
	}

	public void publishRemoveFileComplete(String message)
	{
		this.publishTopic(message, "admin.remove.file.complete");
	}

	public void notifyInstrumentExposureArchived(String message)
	{
		this.mqConnection.publishToQueue(props.getInstrumentIngested(), message);
	}

	public void notifyPortalExposureArchived(String message)
	{
		this.publishTopic(message, props.getExposureArchivedTopic());
	}

	public void respondToRPC(String queueName, String corelationId, String message)
	{
		this.mqConnection.publishToQueue(queueName, corelationId, message);
	}

	public void publishFileThawUpdate(String message)
	{
		this.mqConnection.publishToExchange(props.getDataSubsystemExchange(), message, props.getExposureThawStatusTopic(), true);
	}


	public void archiveDataProduct(String id, String localPath)
	{
		ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemFile(id, localPath, props.getIrodsArchiveCache(), props.getIrodsArchiveResc());
		ArchiveDataProductTask task = new ArchiveDataProductTask(dataProduct);
		taskQueue.addTask(task);
	}

	public void archiveDataProduct(String id, String localPath, ODIDataSubsystemTaskHook hook)
	{
		ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemFile(id, localPath, props.getIrodsArchiveCache(), props.getIrodsArchiveResc());
		ArchiveDataProductTask task = new ArchiveDataProductTask(dataProduct, hook);
		taskQueue.addTask(task);
	}

	public void archiveDataProduct(ODIDataSubsystemDataProduct dataProduct)
	{
		ArchiveDataProductTask task = new ArchiveDataProductTask(dataProduct);
		taskQueue.addTask(task);
	}

	public void archiveExposure(ODIDataSubsystemExposure exposure)
	{
		exposure.setArchiveResource(props.getIrodsArchiveResc());
		exposure.setCacheResource(props.getIrodsArchiveCache());
		exposure.setLocalPath(props.getArchiveCachePath() + "/" + exposure.getId());
		ArchiveExposureComplete exposureComplete = new ArchiveExposureComplete(exposure);
		EventStream.getInstance().detect(exposureComplete);

		IRODSFileSystem irodsFS = null;

		try
		{
			irodsFS = IRODSUtil.getIRODSFileSystem();
		}
		catch (NullPointerException e)
		{
			String errorMessage = "NullPointerException Caught trying to archive exposure:  {" + exposure.toString() + " }";
			log.error(errorMessage, e);
		}

		IRODSUtil.createArchiveDir(irodsFS, exposure.getExposureDir());

		for (ODIDataSubsystemDataProduct ota : exposure.getOTAS().values())
		{
			this.archiveDataProduct(ota);
		}
	}

	public void archiveData(ODIDataSubsystemDataProduct dataProduct)
	{
		dataProduct.setArchiveResource(props.getIrodsArchiveResc());
		dataProduct.setCacheResource(props.getIrodsArchiveCache());
		ArchiveCachedDataProductTask task = new ArchiveCachedDataProductTask(dataProduct);
		taskQueue.addTask(task);
	}

	// With thread hook
	public void archiveData(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook)
	{
		dataProduct.setArchiveResource(props.getIrodsArchiveResc());
		dataProduct.setCacheResource(props.getIrodsArchiveCache());
		ArchiveCachedDataProductTask task = new ArchiveCachedDataProductTask(dataProduct, hook);
		taskQueue.addTask(task);
	}
/*
	public ODIDataSubsystemDataProduct resolveLogicalID(String id)
	{
		return resolveLogicalID(id, new NullTask());
	}

	public ODIDataSubsystemDataProduct resolveLogicalID(String id, ODIDataSubsystemAbstractTask task)
	{
		ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemFile(id);

		return resolveLogicalID(id, dataProduct, task);
	}
	public ODIDataSubsystemDataProduct resolveLogicalID(String id, ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemAbstractTask task)
	{
		IRODSFileSystem irodsFS = null;

		try
		{
			irodsFS = IRODSUtil.getIRODSFileSystem();

			MetaDataCondition conditions[] = {
				MetaDataSet.newCondition("logicalID", MetaDataCondition.EQUAL, id)
			};

			MetaDataSelect[] selects = new MetaDataSelect[] {
				MetaDataSet.newSelection(StandardMetaData.FILE_NAME),
			};

			getLogger().debug("Attempting to resolve logical ID [ " + id + " ] as file using query [ " + IRODSUtil.QueryToString(selects, conditions) + " ]");

			MetaDataRecordList[] rl = irodsFS.query(conditions, selects);

			if (rl != null)
			{
				String fileName = null;
				IRODSFile archiveFile = null;
				IRODSFile cacheFile = null;
				fileName = rl[0].getValue(0).toString();
				archiveFile = new IRODSFile(irodsFS, fileName);
				cacheFile = new IRODSFile(irodsFS, fileName);

				MetaDataSelect[] fieldNames = new MetaDataSelect[] {
						MetaDataSet.newSelection(IRODSMetaDataSet.META_DATA_ATTR_NAME), MetaDataSet.newSelection(IRODSMetaDataSet.META_DATA_ATTR_VALUE)
				};

				rl = archiveFile.query(fieldNames);
				if (rl != null)
				{
					for (MetaDataRecordList record : rl)
					{
						for (int i = 0; i < record.getFieldCount(); i++)
						{
							if (record.getStringValue(i).compareTo("logicalID") == 0)
								dataProduct.setId(record.getStringValue(i + 1));
							if (record.getStringValue(i).compareTo("cacheResource") == 0)
								dataProduct.setCacheResource(record.getStringValue(i + 1));
							if (record.getStringValue(i).compareTo("archiveResource") == 0)
								dataProduct.setArchiveResource(record.getStringValue(i + 1));
						}
					}
					dataProduct.setArchivePath(IRODSUtil.stripZone(archiveFile.getCanonicalPath()));
					dataProduct.setArchiveFileName(archiveFile.getName());
					dataProduct.setReplicas(le());

					cacheFile.setResource(dataProduct.getCacheResourceName());
					if (archiveFile.exists() && dataProduct.isCached() && dataProduct.isArchived())
						task.updateStatus(ODIDataSubsystemTaskStatus.OK);
					else if (archiveFile.exists() && dataProduct.isArchived() && !dataProduct.isCached())
						task.updateStatus(ODIDataSubsystemTaskStatus.NOT_CACHED);
					else if (archiveFile.exists() && dataProduct.isCached() && !dataProduct.isArchived())
						task.updateStatus(ODIDataSubsystemTaskStatus.NOT_ARCHIVED);
				}
			}
			else if (rl == null)
			{
				MetaDataCondition dirConditions[] = {
					MetaDataSet.newCondition(StandardMetaData.DIRECTORY_NAME, MetaDataCondition.EQUAL, irodsFS.getHomeDirectory() + "/" + id)
				};

				MetaDataSelect[] dirSelects = new MetaDataSelect[] {
					MetaDataSet.newSelection(StandardMetaData.DIRECTORY_NAME)
				};

				getLogger().debug(
						"Attempting to resolve logical ID [ " + id + " ] as directory using query [ " + IRODSUtil.QueryToString(dirSelects, dirConditions)
								+ " ]");

				MetaDataRecordList[] dirRL = irodsFS.query(dirConditions, dirSelects);
				if (dirRL != null)
				{
					String directoryName = dirRL[0].getValue(0).toString();
					IRODSFile archiveDirectory = new IRODSFile(irodsFS, directoryName);

					dataProduct.setArchivePath(IRODSUtil.stripZone(archiveDirectory.getCanonicalPath()));
					dataProduct.setArchiveDir(archiveDirectory.getName());

					String[] fileList = archiveDirectory.list();
					for (String file : fileList)
					{
						String logicalID = FilenameUtils.getBaseName(FilenameUtils.getBaseName(file));
						ODIDataSubsystemFile odiDataSubsystemFile = IRODSUtil.getODIDataSubsystemFile(logicalID, log);

						if (odiDataSubsystemFile != null)
						{
							List<String> resources = odiDataSubsystemFile.getReplicaNames();
							if (!resources.isEmpty())
							{
								dataProduct.setReplicas(resources);
								break;
							}
						}
					}

					dataProduct.setCacheResource(props.getIrodsArchiveCache());
					dataProduct.setArchiveResource(props.getIrodsArchiveResc());
					task.updateStatus(ODIDataSubsystemTaskStatus.OK);
				}
			}
			getLogger().debug("Resolve Logical ID [ " + id + " ] completed with status [ " + task.getStatus() + " ]");
		}
		catch (NullPointerException e)
		{
			task.updateStatus(ODIDataSubsystemTaskStatus.LOGICAL_ID_NOT_FOUND);
			getLogger().error("NPE while resolving Logical ID [ " + id + " ] completed with status [ " + task.getStatus() + " ]");
			e.printStackTrace();
		}
		catch (IOException e)
		{
			task.updateStatus(ODIDataSubsystemTaskStatus.LOGICAL_ID_NOT_FOUND);
			getLogger().error("IOException while resolving Logical ID [ " + id + " ] completed with status [ " + task.getStatus() + " ]");
			e.printStackTrace();
		}

		return dataProduct;
	}
*/
	public void stageOutData(ODIDataSubsystemDataProduct dataProduct) throws IOException
	{
		dataProduct.setArchiveResource(props.getIrodsArchiveResc());
		dataProduct.setCacheResource(props.getIrodsArchiveCache());
		CacheArchiveDataTask task = new CacheArchiveDataTask(dataProduct);
		task.setPriority(1);
		taskQueue.addTask(task);
	}

	public void retrieveData(ODIDataSubsystemDataProduct dataProduct, String path) throws NullPointerException, IOException
	{
		dataProduct.setArchiveResource(props.getIrodsArchiveResc());
		dataProduct.setCacheResource(props.getIrodsArchiveCache());
		RetrieveArchiveDataTask task = new RetrieveArchiveDataTask(dataProduct, path);
		taskQueue.addTask(task);
	}

	public void removeDataProduct(ODIDataSubsystemDataProduct dataProduct, boolean removeAll) throws NullPointerException, IOException
	{
		dataProduct.setArchiveResource(props.getIrodsArchiveResc());
		dataProduct.setCacheResource(props.getIrodsArchiveCache());
		RemoveDataProductTask task = new RemoveDataProductTask(dataProduct, removeAll);
		task.setPriority(1);
		taskQueue.addTask(task);
	}

	public List<ODIDataSubsystemDataProduct> purgeCache(List<String> ids)
	{
		List<ODIDataSubsystemDataProduct> resolvedArchives = new ArrayList<ODIDataSubsystemDataProduct>();

		for (String id : ids)
			resolvedArchives.add(IRODSUtil.getDataProduct(id, log));

		return resolvedArchives;
	}

	public void deleteIRODSFile(LocalFile localFile)
	{
		IRODSFileSystem irodsFS = null;
		try
		{
			irodsFS = IRODSUtil.getIRODSFileSystem();
			IRODSFile archiveFile = new IRODSFile(irodsFS, localFile.getName());
			archiveFile.delete();
		}
		catch (NullPointerException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Object clone() throws CloneNotSupportedException
	{
		throw new CloneNotSupportedException();
	}
}
