package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;

public class CacheArchiveDataTask extends ODIDataStorageAbstractTask
{

	public CacheArchiveDataTask(ODIDataSubsystemDataProduct dataProduct)
	{
		super(dataProduct);
	}

	public CacheArchiveDataTask(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook)
	{
		super(dataProduct, hook);
	}

	@Override
	public void execute()
	{
		this.onStart();
		IRODSFileSystem irodsFS = null;

//		dataProduct = ODIDataStorageSystem.getInstance().resolveLogicalID(this.dataProduct.getId(), this.dataProduct, this);
		this.dataProduct = IRODSUtil.getDataProduct(this.dataProduct.getId(), log);

		if (!dataProduct.isEmpty())
		{
			if (!dataProduct.isCached())
			{
				try
				{
					irodsFS = IRODSUtil.getIRODSFileSystem();
					IRODSFile archiveFile = null;
					if (dataProduct.getArchiveFileName() != "")
						archiveFile = new IRODSFile(irodsFS, dataProduct.getArchiveFileName());
					else if (dataProduct.getArchiveDir() != "")
						archiveFile = new IRODSFile(irodsFS, dataProduct.getArchiveDir());

					long startTime = 0;
					long endTime = 0;
					if (archiveFile != null)
					{
//						System.out.println("Before isFile File: " + archiveFile.getName());
						if (archiveFile.isFile())
						{
//							System.out.println("File: " + archiveFile.getName());
							startTime = System.nanoTime();
							archiveFile.replicate(dataProduct.getCacheResourceName());
							endTime = System.nanoTime();
							IRODSFile cacheFile = new IRODSFile(irodsFS, dataProduct.getArchiveFileName());
							cacheFile.setResource(dataProduct.getCacheResourceName());
							if (archiveFile.checksumUsingMD5().compareTo(cacheFile.checksumUsingMD5()) != 0)
								this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);
						}
						else if (archiveFile.isDirectory())
						{
							System.out.println("Starting directory archive caching");
							startTime = System.nanoTime();
							archiveFile.replicate(dataProduct.getCacheResourceName());
							IRODSFile cacheFile = new IRODSFile(irodsFS, dataProduct.getArchiveDir());
							cacheFile.setResource(dataProduct.getCacheResourceName());
							
//							if (cacheFile.checksumUsingMD5().compareTo(cacheFile.checksumUsingMD5()) != 0)
//								this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);
							
							String[] fileList = archiveFile.list();
							for (String file : fileList)
							{
//								ODIDataSubsystemExposure exposure = new ODIDataSubsystemExposure(dataProduct.getId(), dataProduct.getArchiveDir());
//								ODIDataSubsystemExposureFile exposureFile = new ODIDataSubsystemExposureFile(exposure, file, file);
//								exposureFile.setArchiveFileName(file);

								IRODSFile replFile = new IRODSFile(irodsFS, archiveFile.getCanonicalPath() + "/" + file);
//								CacheArchiveDataTask archiveFileTask = new CacheArchiveDataTask(exposureFile, this.irodsAccount);
//								archiveFileTask.execute();
								//Need to add here to the dataproduct more information, maybe even in the util call
								
								cacheFile = new IRODSFile(irodsFS, dataProduct.getArchiveDir() + "/" + file);
								replFile.replicate(dataProduct.getCacheResourceName());
								cacheFile.setResource(dataProduct.getCacheResourceName());
								if (replFile.checksumUsingMD5().compareTo(cacheFile.checksumUsingMD5()) != 0)
									this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);
								log.debug("[ " + replFile.getName() + " ] replicated to [ " + dataProduct.getCacheResourceName() + " ]");
							}
							endTime = System.nanoTime();
						}
						log.info("[ " + archiveFile.getName() + " ] replicated to [ " + dataProduct.getCacheResourceName() + " ] in [ " + (endTime - startTime)
							/ 1000000000.0 + " seconds] with status [ " + this.getStatus() + " ]");
					}
					else
					{
						this.updateStatus(ODIDataSubsystemTaskStatus.ARCHIVE_NOT_FOUND);
						log.info("[ " + dataProduct.getId() + " ] NOT FOUND in archive when trying to thaw");
					}

				}
				catch (IOException e)
				{
					e.printStackTrace();
					this.onComplete();
				}
			}
			this.updateStatus(ODIDataSubsystemTaskStatus.OK);
		}
		else
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.ARCHIVE_NOT_FOUND);
		}

		this.onComplete();
	}
}