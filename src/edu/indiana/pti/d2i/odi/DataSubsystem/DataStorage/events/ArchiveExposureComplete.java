package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.ArrayList;

import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposure;

public class ArchiveExposureComplete extends DataProductSetArchived
{
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	ODIDataSubsystemExposure exposure = null;
	
	public ArchiveExposureComplete(ODIDataSubsystemExposure exposure)
	{
		super(new ArrayList<String>(exposure.getOTAS().keySet()));
		this.name = this.expression;
		this.xmlStart = "<ArchiveExposureComplete>";
		this.xmlEnd = "</ArchiveExposureComplete>";
		this.exposure = exposure;
	}
	
	public String getExposureId()
	{
		return exposure.getId();
	}
	
	public ODIDataSubsystemExposure getExposure()
	{
		return exposure;
	}
}
