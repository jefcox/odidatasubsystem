package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.local.LocalFile;

public class ArchiveDataProductTask extends ODIDataStorageAbstractTask
{
	private long	archiveStartTime	= 0;
	private long	archiveEndTime		= 0;
	private int		retries				= 3;
	private int		attempts			= 0;

	public ArchiveDataProductTask(ODIDataSubsystemDataProduct dataProduct)
	{
		super(dataProduct);
	}

	public ArchiveDataProductTask(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook)
	{
		super(dataProduct, hook);
	}

	@Override
	public void execute()
	{
		this.onStart();
		this.retries--;
		this.attempts++;

		if (retries >= 0)
		{
			IRODSFile archiveFile = null;
			LocalFile localFile = null;
			IRODSFileSystem irodsFS = null;
			this.updateStatus(ODIDataSubsystemTaskStatus.OK);

			try
			{
				irodsFS = IRODSUtil.getIRODSFileSystem();
				localFile = new LocalFile(dataProduct.getLocalPath());

				//TODO: Change this to look to maybe look for exposure type of dataproduct. May still want certain actions if 
				// there is an archiveDir set
				if (dataProduct.getArchiveDir() != "")
					archiveFile = new IRODSFile(irodsFS, dataProduct.getArchiveDir() + "/" + localFile.getName());
				else
					archiveFile = new IRODSFile(irodsFS, localFile.getName());

				String localFileMD5 = getMD5(localFile);
				String archiveFileMD5 = getMD5(archiveFile);

				if (localFileMD5 == null || this.dataProduct.getChecksum().compareTo(localFileMD5) != 0)
				{
					this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);
					String errorMessage = "[ CHECKSUM_MISMACH ] between local file: [ path={" + dataProduct.getLocalPath() + "} checksum={ " + localFileMD5
							+ "} ] ";
					errorMessage += "and file defined in request: [ path={" + dataProduct.getLocalPath() + "} checksum={ " + dataProduct.getChecksum() + "} ] ";
					errorMessage += "data product: [ " + this.dataProduct.getId() + " ] ";
					log.error(this.dataProduct, errorMessage);
				}
				else
				{
					this.archiveStartTime = System.nanoTime();
					boolean archiveSuccess = true;

					if (archiveFileMD5 == null || (archiveFileMD5.compareTo(localFileMD5) != 0))
						archiveSuccess = this.archiveFile(archiveFile, localFile, dataProduct);

					this.archiveEndTime = System.nanoTime();

					if (archiveSuccess)
					{
						try
						{
							this.getDataProduct().setArchivePath(archiveFile.getCanonicalPath());
							this.getDataProduct().setReplicas(IRODSUtil.getAllReplicas(this.dataProduct.getId()));
						}
						catch (IOException e)
						{
							this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
							String errorMessage = "IOException caught while trying to archive data product: { " + dataProduct.toString() + " }";
							log.error(this.dataProduct, errorMessage, e);
						}

						log.info("Archived [ " + this.getDataProduct().getArchiveFileName() + " ] with logicalID [ " + this.getDataProduct().getId()
								+ " ] in [ " + (this.archiveEndTime - this.archiveStartTime) / 1000000000.0 + " seconds] with status [ " + this.getStatus()
								+ " ]");
						//TODO:  Maybe this is not always the case, should probably make as an event.
						localFile.delete();
					}
					else
					{
						//TODO: Maybe more checking to clean up any zero byte files
						//						archiveFile.delete();
						this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
						String errorMessage = "Archive [ FAILED ] for data product [ id={" + this.dataProduct.getId() + "} path={" + dataProduct.getLocalPath()
								+ "} checksum={ " + dataProduct.getChecksum() + "} ] ";
						errorMessage += "after [ " + attempts + " ] attempts";
						log.error(this.dataProduct, errorMessage);
					}
				}
			}
			finally
			{

				//				public void close()
				//				           throws java.lang.NullPointerException,
				//				                  java.io.IOException
				//				This method is here for correctness, and will help avoid dropping connections when disconnect was never called. Note that this method closes the underlying IRODSFileSystem, and care must be taken so as not to close the IRODSFileSystem when it is intended for re-use. Specifically, this method is added for occasions where the IRODSFile is created with a URI parameter. In that case, the IRODSFileSystem is not reused, and without calling close, the termination of the IRODSFile causes a "readMsgHeader:header read- read 0 bytes, expect 4, status = -4000" error in IRODS.
				//				Throws:
				//				java.lang.NullPointerException
				//				java.io.IOException
				try
				{
					if (irodsFS != null)
						irodsFS.close();
					this.onComplete();
				}
				catch (IOException e)
				{
					this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
					String errorMessage = "IOException caught while trying to close irodsFS : { " + dataProduct.toString() + " }";
					log.error(this.dataProduct, errorMessage, e);
					this.onComplete();
				}
			}
		}
	}

	private boolean archiveFile(IRODSFile archiveFile, LocalFile localFile, ODIDataSubsystemDataProduct dataProduct)
	{
		boolean success = true;
		try
		{
			String infoMessage = "Check of archive file before copy: [ arhive path={ " + archiveFile.getPath() + " } ";
			infoMessage += "readable={ " + archiveFile.canRead() + " } ";
			infoMessage += "writeable={ " + archiveFile.canWrite() + " } ";
			infoMessage += "checksum={ " + archiveFile.checksumUsingMD5() + " } ]";
			infoMessage += "Check of local file before copy: [ local file path={ " + localFile.getPath() + " } ";
			infoMessage += "readable={ " + localFile.canRead() + " } ";
			infoMessage += "writeable={ " + localFile.canWrite() + " } ";
			infoMessage += "checksum={ " + localFile.checksumUsingMD5() + " } ]";
			log.info(infoMessage);

			archiveFile.copyFrom(localFile, true);

			infoMessage = "Check of archive file after copy: [ arhive path={ " + archiveFile.getPath() + " } ";
			infoMessage += "readable={ " + archiveFile.canRead() + " } ";
			infoMessage += "writeable={ " + archiveFile.canWrite() + " } ";
			infoMessage += "checksum={ " + archiveFile.checksumUsingMD5() + " } ]";
			infoMessage += "Check of local file after copy: [ local file path={ " + localFile.getPath() + " } ";
			infoMessage += "readable={ " + localFile.canRead() + " } ";
			infoMessage += "writeable={ " + localFile.canWrite() + " } ";
			infoMessage += "checksum={ " + localFile.checksumUsingMD5() + " } ]";
			log.info(infoMessage);

			String archiveMD5 = getMD5(archiveFile);
			if (archiveMD5.compareTo(dataProduct.getChecksum()) != 0)
			{
				this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);
				String errorMessage = "[ CHECKSUM_MISMACH ] between newly archived file: ";
				errorMessage += "[ path={" + archiveFile.getPath() + "} checksum={ " + archiveMD5 + "} ] ";
				errorMessage += "and file defined in request: [ path={" + dataProduct.getLocalPath() + "} checksum={ " + dataProduct.getChecksum() + "} ] ";
				errorMessage += "data product: [ " + this.dataProduct.getId() + " ] ";
				log.error(this.dataProduct, errorMessage);

				success = false;
				archiveFile.delete();
				this.setState(ODIDataSubsystemTaskState.Restarted);
				this.execute();
			}
			
			try
			{
				String[][] metaData = new String[][] {
						{
								"logicalID", dataProduct.getId()
						}, {
								"cacheResource", dataProduct.getCacheResourceName()
						}, {
								"archiveResource", dataProduct.getArchiveResource()
						}
				};

				archiveFile.modifyMetaData(metaData[0]);
				archiveFile.modifyMetaData(metaData[1]);
				archiveFile.modifyMetaData(metaData[2]);
			}
			catch (IOException e)
			{
				success = false;
				this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
				String errorMessage = "IOException caught while trying to add metadata to new archive file: ";
				errorMessage += "archive file [ { name={ " + archiveFile.getName() + " } ";
				errorMessage += "path={ " + archiveFile.getPath() + " } ";
				errorMessage += "]";
				log.error(this.dataProduct, errorMessage, e);
				this.setState(ODIDataSubsystemTaskState.Restarted);
				this.execute();
			}
		}
		catch (IOException e)
		{
			success = false;
			this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
			String errorMessage = "IOException caught while trying to copy localfile to archive ";
			errorMessage += "archive file [ name={ " + archiveFile.getName() + " } ";
			errorMessage += "path={ " + archiveFile.getPath() + " } ] ";
			errorMessage += "local file [ name={ " + localFile.getName() + " } ";
			errorMessage += "path={ " + localFile.getPath() + " } ] ";
			errorMessage += "data product [ checksum={ " + this.dataProduct.getChecksum() + " } ";
			errorMessage += "path={ " + this.dataProduct.getLocalPath() + " } ";
			errorMessage += "ID={ " + this.dataProduct.getId() + " } ] ";
			errorMessage += "Exception [ message={ " + e.getMessage() + " } ";
			errorMessage += "cause={ " + e.getCause() + " } ";
			errorMessage += "stack trace={ ";
			for (StackTraceElement stackElement : e.getStackTrace())
				errorMessage += stackElement.toString() + "\n";
			errorMessage += " } ] ";
			log.error(this.dataProduct, errorMessage, e);
			this.setState(ODIDataSubsystemTaskState.Restarted);
			archiveFile.delete();
			this.execute();
		}

		return success;
	}

	private String getMD5(IRODSFile file)
	{
		String checksum = " ";
		try
		{
			checksum = file.checksumUsingMD5();
		}
		catch (IOException e)
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
			String errorMessage = "IOException caught while trying to get MD5 checksum of ";
			errorMessage += "local file [ { name={ " + file.getName() + " } ";
			errorMessage += "path={ " + file.getPath() + " } ";
			errorMessage += "] ";
			log.error(this.dataProduct, errorMessage, e);
		}
		return checksum;
	}

	private String getMD5(LocalFile file)
	{
		String checksum = " ";
		try
		{
			checksum = file.checksumUsingMD5();
		}
		catch (IOException e)
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.IO_ERROR);
			String errorMessage = "IOException caught while trying to get MD5 checksum of ";
			errorMessage += "local file [ { name={ " + file.getName() + " } ";
			errorMessage += "path={ " + file.getPath() + " } ";
			errorMessage += "] before archiving";
			log.error(this.dataProduct, errorMessage, e);
		}
		return checksum;
	}
}
