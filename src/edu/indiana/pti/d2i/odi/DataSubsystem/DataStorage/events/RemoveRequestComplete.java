package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class RemoveRequestComplete extends ODIDataSubsystemComplexEvent
{
	private ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	
	public RemoveRequestComplete(String requestSetId)
	{
		this.expression = "Select * from " + ArchiveSetRemoved.class.getSimpleName();
		this.expression += " where " + ArchiveSetRemoved.class.getSimpleName() + ".ID = '" + requestSetId + "'";
		this.name = this.expression;
	}
	
	public void update(ArchiveSetRemoved event)
	{
		String message = "<admin><remove><response>";
		message += event.getXmlString();
		message += "</response></remove></admin>";
		dss.publishRemoveRequestComplete(message);
		EventStream.getInstance().stopDetect(this);
		EventStream.getInstance().insertEvent(this);
	}
}
