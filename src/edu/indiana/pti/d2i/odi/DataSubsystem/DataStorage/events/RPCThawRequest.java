package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class RPCThawRequest extends ODIDataSubsystemComplexEvent
{
	private ODIDataStorageSystem	dss	= ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();
	private ODIDataSubsystemLogger	log	= dss.getLogger();

	public RPCThawRequest()
	{
		this.name = RPCThawRequest.class.getCanonicalName();
		this.expression = "select * from " + RabbitMQMessage.class.getSimpleName();
		this.expression += " where RabbitMQMessage.envelope.routingKey = '" + props.getThawRequestQueue() + "'";
		this.expression += " and RabbitMQMessage.properties.replyTo != ''";
		this.expression += " and RabbitMQMessage.properties.correlationId != ''";

	}

	public void update(RabbitMQMessage event)
	{
		String message = new String(event.getBody());
		String queueName = event.getProperties().getReplyTo();
		String correlationID = event.getProperties().getCorrelationId();

		log.debug("RPC Thaw Request Recieved-> properties[ replyTo[" + queueName + "] correlationId[" + correlationID + "]]" + " message[ " + message + "]");

		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			List<String> logicalIDs = XMLUtil.getTextValues(request, "LogicalID");

			if (correlationID != null && queueName != null)
			{
				if (!logicalIDs.isEmpty())
				{
					ArchiveSetThawed fileSetThawed = new ArchiveSetThawed(logicalIDs);
					RPCThawRequestComplete rpcThawRequestComplete = new RPCThawRequestComplete(fileSetThawed.getID(), queueName, correlationID);
					RPCThawRequestAck requestAck = new RPCThawRequestAck(event.getEnvelope().getDeliveryTag(), rpcThawRequestComplete.getID());
					
					EventStream.getInstance().detect(requestAck);
					EventStream.getInstance().detect(rpcThawRequestComplete);
					EventStream.getInstance().detect(fileSetThawed);
					
				}
				else
				{
					//TODO add some sort of error notification that we got an empty set of logicalID's
				}
			}

			for (String logicalID : logicalIDs)
			{
				ODIDataSubsystemDataProduct dataProduct = IRODSUtil.getDataProduct(logicalID, dss.getLogger());
//				ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemDataProduct(logicalID);
				dss.stageOutData(dataProduct);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
