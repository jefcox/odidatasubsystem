package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;

public class RemoveDataProductTask extends ODIDataStorageAbstractTask
{
	boolean	removeAll	= false;

	public RemoveDataProductTask(ODIDataSubsystemDataProduct dataProduct, boolean removeAll)
	{
		super(dataProduct);
		this.removeAll = removeAll;
	}

	public RemoveDataProductTask(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook, boolean removeAll)
	{
		super(dataProduct, hook);
		this.removeAll = removeAll;
	}

	@Override
	public void execute()
	{
		this.onStart();
		IRODSFileSystem irodsFS = null;

		this.dataProduct = IRODSUtil.getDataProduct(this.dataProduct.getId(), log);

		if (!this.dataProduct.isEmpty())
		{
			irodsFS = IRODSUtil.getIRODSFileSystem();
			IRODSFile archiveFile = null;
			if (!this.dataProduct.getArchiveDir().isEmpty())
				archiveFile = new IRODSFile(irodsFS, this.dataProduct.getArchiveDir());
			else
				archiveFile = new IRODSFile(irodsFS, this.dataProduct.getArchiveFileName());

			long startTime = System.nanoTime();

			if (removeAll)
				archiveFile.delete();
			else
				archiveFile.deleteReplica(this.dataProduct.getCacheResourceName());

			long endTime = System.nanoTime();
			this.updateStatus(ODIDataSubsystemTaskStatus.OK);
			log.info("Deleted [ " + archiveFile.getName() + " ] with logicalID [ " + this.dataProduct.getId() + " ] in [ " + (endTime - startTime)
					/ 1000000000.0 + " seconds] with status [ " + this.getStatus() + " ]");
		}
		else
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.ARCHIVE_NOT_FOUND);
			this.onComplete();
		}
	}
}
