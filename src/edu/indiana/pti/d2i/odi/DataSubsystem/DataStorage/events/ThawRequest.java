package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class ThawRequest extends ODIDataSubsystemComplexEvent
{
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	public ThawRequest()
	{
		this.name = ThawRequest.class.getCanonicalName();
		this.expression = "select * from " + RabbitMQMessage.class.getSimpleName();
		this.expression += " where RabbitMQMessage.envelope.routingKey = '" + props.getThawRequestQueue() + "'";
		this.expression += " and RabbitMQMessage.replyTo is null";
		this.expression += " and RabbitMQMessage.correlationId is null";
	}

	public void update(RabbitMQMessage event)
	{
		String message = new String(event.getBody());
		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			
			List<String> logicalIDs = XMLUtil.getTextValues(request, "LogicalID");
			if(!logicalIDs.isEmpty())
			{
				ArchiveSetThawed fileSetThawed = new ArchiveSetThawed(logicalIDs);
				ThawRequestComplete thawRequestComplete = new ThawRequestComplete(fileSetThawed.getID());
				ThawRequestAck requestAck = new ThawRequestAck(event.getEnvelope().getDeliveryTag(), thawRequestComplete.getID());
				
				EventStream.getInstance().detect(requestAck);
				EventStream.getInstance().detect(thawRequestComplete);
				EventStream.getInstance().detect(fileSetThawed);
			}
			else
			{
				//TODO: add some sort of error handling here for empy set of logical id's
			}
			
			for (String logicalID : logicalIDs)
			{
//				ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemDataProduct(logicalID);
				ODIDataSubsystemDataProduct dataProduct = IRODSUtil.getDataProduct(logicalID, dss.getLogger());
				dss.stageOutData(dataProduct);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
