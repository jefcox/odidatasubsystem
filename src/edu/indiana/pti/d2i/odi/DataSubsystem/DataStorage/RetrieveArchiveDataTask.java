package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.local.LocalFile;

public class RetrieveArchiveDataTask extends ODIDataStorageAbstractTask
{
	String replicatePath = "";
	
	public RetrieveArchiveDataTask(ODIDataSubsystemDataProduct dataProduct, String replicatePath)
	{
		super(dataProduct);
		this.replicatePath = replicatePath;
	}

	public RetrieveArchiveDataTask(ODIDataSubsystemDataProduct dataProduct, String replicatePath, ODIDataSubsystemTaskHook hook)
	{
		super(dataProduct, hook);
		this.replicatePath = replicatePath;
	}

	@Override
	public void execute()
	{
		this.onStart();
		IRODSFileSystem irodsFS = null;

		this.dataProduct = IRODSUtil.getDataProduct(this.dataProduct.getId(), log);

		if (!this.dataProduct.isEmpty())
		{
			try
			{
				irodsFS = IRODSUtil.getIRODSFileSystem();
				IRODSFile archiveFile = new IRODSFile(irodsFS, this.dataProduct.getArchiveFileName());
				IRODSFile cacheFile = new IRODSFile(irodsFS, this.dataProduct.getArchiveFileName());

				if (!this.dataProduct.isCached())
					archiveFile.replicate(this.dataProduct.getCacheResourceName());

				LocalFile outFile = new LocalFile(this.replicatePath + "/" + this.dataProduct.getId() + ".part");
				LocalFile finalFile = new LocalFile(this.replicatePath + "/" + this.dataProduct.getId());

				long startTime = System.nanoTime();
				archiveFile.copyTo(outFile);
				outFile.renameTo(finalFile);
				long endTime = System.nanoTime();

				cacheFile.setResource(this.dataProduct.getCacheResourceName());
				if (archiveFile.checksum().compareTo(cacheFile.checksum()) != 0)
					this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);

				if (archiveFile.checksum().compareTo(finalFile.checksum()) != 0)
					this.updateStatus(ODIDataSubsystemTaskStatus.CHECKSUM_MISMATCH);

				log.info("Retrieved [ " + this.dataProduct.getArchiveFileName() + " ] with logicalID [ " + this.dataProduct.getId() + " ] in [ "
						+ (endTime - startTime) / 1000000000.0 + " seconds] with status [ " + this.getStatus() + " ]");
			}
			catch (IOException e)
			{
				e.printStackTrace();
				this.onComplete();
			}
		}
		else
		{
			this.updateStatus(ODIDataSubsystemTaskStatus.ARCHIVE_NOT_FOUND);
			this.onComplete();
		}
	}
}
