package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;

import edu.sdsc.grid.io.FileMetaData;
import edu.sdsc.grid.io.MetaDataCondition;
import edu.sdsc.grid.io.MetaDataRecordList;
import edu.sdsc.grid.io.MetaDataSelect;
import edu.sdsc.grid.io.MetaDataSet;
import edu.sdsc.grid.io.ResourceMetaData;
import edu.sdsc.grid.io.StandardMetaData;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.srb.SRBMetaDataSet;

public class IRODSResource
{
	private String	collResourceName		= "";
	private String	resourceName			= "";
	private String	resourceZone			= "";
	private String	resourceType			= "";
	private String	resourceClass			= "";
	private String	resourceLocation		= "";
	private String	resourceVaultPath		= "";
	private String	resourceFreeSpace		= "";
	private String	resourceFreeSpaceTime	= "";
	private String	resourceInfo			= "";
	private String	resourceComments		= "";
	private String	resourceCreateDate		= "";
	private String	resourceModifyDate		= "";
	private String	resourceStatus			= "";

	public IRODSResource()
	{

	}

	public IRODSResource(IRODSFileSystem irodsFS, String resourceName)
	{
		try
		{
			MetaDataSelect selects[] = {
					MetaDataSet.newSelection(SRBMetaDataSet.COLL_RESOURCE_NAME),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_NAME),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_ZONE),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_TYPE),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_CLASS),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_LOCATION),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_VAULT_PATH),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_FREE_SPACE),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_FREE_SPACE_TIME),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_INFO),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_COMMENTS),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_CREATE_DATE),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_MODIFY_DATE),
					MetaDataSet.newSelection(StandardMetaData.DIRECTORY_NAME),
					MetaDataSet.newSelection(SRBMetaDataSet.RESOURCE_STATUS)
			};

			MetaDataCondition conditions[] = {
				MetaDataSet.newCondition(SRBMetaDataSet.RESOURCE_NAME, MetaDataCondition.EQUAL, resourceName)
			};
			MetaDataRecordList[] rl = irodsFS.query(conditions, selects);

			if (rl != null)
			{
				this.collResourceName = (String) rl[0].getValue(SRBMetaDataSet.COLL_RESOURCE_NAME);
				this.resourceName = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_NAME);
				this.resourceZone = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_ZONE);
				this.resourceType = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_TYPE);
				this.resourceClass = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_CLASS);
				this.resourceLocation = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_LOCATION);
				this.resourceVaultPath = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_VAULT_PATH);
				this.resourceFreeSpace = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_FREE_SPACE);
				this.resourceFreeSpaceTime = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_FREE_SPACE_TIME);
				this.resourceInfo = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_INFO);
				this.resourceComments = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_COMMENTS);
				this.resourceCreateDate = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_CREATE_DATE);
				this.resourceModifyDate = (String) rl[0].getValue(SRBMetaDataSet.RESOURCE_MODIFY_DATE);
				this.resourceStatus = (String) rl[0].getValue(ResourceMetaData.RESOURCE_STATUS);
			}
			else
			{
				this.resourceName = resourceName;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public String getCollResourceName()
	{
		return collResourceName;
	}

	public void setCollResourceName(String collResourceName)
	{
		this.collResourceName = collResourceName;
	}

	public String getResourceName()
	{
		return resourceName;
	}

	public void setResourceName(String resourceName)
	{
		this.resourceName = resourceName;
	}

	public String getResourceZone()
	{
		return resourceZone;
	}

	public void setResourceZone(String resourceZone)
	{
		this.resourceZone = resourceZone;
	}

	public String getResourceType()
	{
		return resourceType;
	}

	public void setResourceType(String resourceType)
	{
		this.resourceType = resourceType;
	}

	public String getResourceClass()
	{
		return resourceClass;
	}

	public void setResourceClass(String resourceClass)
	{
		this.resourceClass = resourceClass;
	}

	public String getResourceLocation()
	{
		return resourceLocation;
	}

	public void setResourceLocation(String resourceLocation)
	{
		this.resourceLocation = resourceLocation;
	}

	public String getResourceVaultPath()
	{
		return resourceVaultPath;
	}

	public void setResourceVaultPath(String resourceVaultPath)
	{
		this.resourceVaultPath = resourceVaultPath;
	}

	public String getResourceFreeSpace()
	{
		return resourceFreeSpace;
	}

	public void setResourceFreeSpace(String resourceFreeSpace)
	{
		this.resourceFreeSpace = resourceFreeSpace;
	}

	public String getResourceFreeSpaceTime()
	{
		return resourceFreeSpaceTime;
	}

	public void setResourceFreeSpaceTime(String resourceFreeSpaceTime)
	{
		this.resourceFreeSpaceTime = resourceFreeSpaceTime;
	}

	public String getResourceInfo()
	{
		return resourceInfo;
	}

	public void setResourceInfo(String resourceInfo)
	{
		this.resourceInfo = resourceInfo;
	}

	public String getResourceComments()
	{
		return resourceComments;
	}

	public void setResourceComments(String resourceComments)
	{
		this.resourceComments = resourceComments;
	}

	public String getResourceCreateDate()
	{
		return resourceCreateDate;
	}

	public void setResourceCreateDate(String resourceCreateDate)
	{
		this.resourceCreateDate = resourceCreateDate;
	}

	public String getResourceModifyDate()
	{
		return resourceModifyDate;
	}

	public void setResourceModifyDate(String resourceModifyDate)
	{
		this.resourceModifyDate = resourceModifyDate;
	}

	public String getResourceStatus()
	{
		return resourceStatus;
	}

	public void setResourceStatus(String resourceStatus)
	{
		this.resourceStatus = resourceStatus;
	}
}
