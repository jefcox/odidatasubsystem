package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.CacheArchiveDataTask;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class ArchiveThawed extends ODIDataSubsystemComplexEvent
{
	private String logicalID = "";
	private String xmlString = "";
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();
	
	public ArchiveThawed()
	{
		this.name = ArchiveThawed.class.getCanonicalName();
		this.expression = "select * from " + CacheArchiveDataTask.class.getSimpleName();
		this.expression += " where " + CacheArchiveDataTask.class.getSimpleName() + ".state.toString() = '"+ ODIDataSubsystemTaskState.Completed.toString() +"'";
	}
	
	public void update(CacheArchiveDataTask task)
	{
		this.logicalID = task.getDataProduct().getId();
		
		this.xmlString = "<File>";
		this.xmlString += "<LogicalID>"+task.getDataProduct().getId()+"</LogicalID>";
		this.xmlString += "<Status>"+task.getStatus()+"</Status>";
		this.xmlString += "<Path>"+task.getDataProduct().getArchivePath()+"</Path>";
		this.xmlString += "<Detail> Task: [ "+task.getClass().getSimpleName()+" ] State: [ "+task.getState()+" ] Status: [ "+task.getStatus()+" ]</Detail>";
		this.xmlString += "</File>";
		
		String message ="";
		message += "<FileThawed>";
		message += this.xmlString;
		message += "</FileThawed>";

		ODIDataStorageSystem.getInstance().publishTopic(message,props.getFileArchiveStatusQueue());
		EventStream.getInstance().insertEvent(this);
	}
	
	public String getXmlString()
	{
		return this.xmlString;
	}
	
	public void setXmlString(String xml)
	{
		this.xmlString = xml;
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}
}
