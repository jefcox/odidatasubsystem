package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.RemoveDataProductTask;

public class ArchiveRemoved extends ODIDataSubsystemComplexEvent
{
	private String logicalID = "";
	private String xmlString = "";
	
	public ArchiveRemoved()
	{
		this.name = ArchiveRemoved.class.getCanonicalName();
		this.expression = "select * from " + RemoveDataProductTask.class.getSimpleName();
		this.expression += " where " + RemoveDataProductTask.class.getSimpleName() + ".state.toString() = '"+ ODIDataSubsystemTaskState.Completed.toString() +"'";
	}
	
	public void update(RemoveDataProductTask task)
	{
		this.logicalID = task.getDataProduct().getId();
		
		this.xmlString = "<File>";
		this.xmlString += "<LogicalID>"+task.getDataProduct().getId()+"</LogicalID>";
		this.xmlString += "<Status>"+task.getStatus()+"</Status>";
		this.xmlString += "<Detail> Task: [ "+task.getClass().getSimpleName()+" ] State: [ "+task.getState()+" ] Status: [ "+task.getStatus()+" ]</Detail>";
		this.xmlString += "</File>";
		
		String message ="";
		message += "<FileRemoved>";
		message += this.xmlString;
		message += "</FileRemoved>";
		
		ODIDataStorageSystem.getInstance().publishRemoveFileComplete(message);
		EventStream.getInstance().insertEvent(this);
	}
	
	public String getXmlString()
	{
		return this.xmlString;
	}
	
	public void setXmlString(String xml)
	{
		this.xmlString = xml;
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}
}
