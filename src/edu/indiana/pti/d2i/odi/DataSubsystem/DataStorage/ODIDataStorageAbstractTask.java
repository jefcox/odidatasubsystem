package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemAbstractTask;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskHook;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;

public abstract class ODIDataStorageAbstractTask extends ODIDataSubsystemAbstractTask
{

	protected ODIDataSubsystemDataProduct	dataProduct		= null;
	protected ODIDataSubsystemLogger		log				= ODIDataStorageSystem.getInstance().getLogger();

	public abstract void execute();

	public ODIDataStorageAbstractTask(ODIDataSubsystemDataProduct dataProduct)
	{
		this.dataProduct = dataProduct;
		this.state = ODIDataSubsystemTaskState.Initialized;
	}
	
	public ODIDataStorageAbstractTask(ODIDataSubsystemDataProduct dataProduct, ODIDataSubsystemTaskHook hook)
	{
		this.hook = hook;
	}

	public void setDataProduct(ODIDataSubsystemDataProduct dataProduct)
	{
		this.dataProduct = dataProduct;
	}

	public ODIDataSubsystemDataProduct getDataProduct()
	{
		return this.dataProduct;
	}
}
