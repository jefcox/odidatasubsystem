package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class RPCThawRequestAck extends ODIDataSubsystemComplexEvent
{
	long deliveryTag = 0;
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();

	public RPCThawRequestAck(long deliveryTag, String eventId)
	{
		this.deliveryTag = deliveryTag;
		this.name = RPCThawRequestAck.class.getCanonicalName() + "|" + deliveryTag;
		this.expression = "select * from " + RPCThawRequestComplete.class.getSimpleName();
		this.expression += " where " + RPCThawRequestComplete.class.getSimpleName() + ".ID  = '" + eventId + "'";
	}
	
	public void update(RPCThawRequestComplete exposureComplete)
	{
		dss.acknowledgeMessage(deliveryTag, false);
		EventStream.getInstance().stopDetect(this);
	}
}
