package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataSubsystemExposure;
import edu.indiana.pti.d2i.odi.DataSubsystem.Logging.ODIDataSubsystemLogger;

public class ExposureArchived extends ODIDataSubsystemComplexEvent
{
	private ODIDataStorageSystem	dss	= ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemLogger	log	= dss.getLogger();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();


	public ExposureArchived()
	{
		this.expression = "Select * from " + ArchiveExposureComplete.class.getSimpleName();
		this.name = this.expression;
	}

	public void update(ArchiveExposureComplete exposureComplete)
	{
		this.publishExposureArchived(exposureComplete);
	}
	
	private String buildPortalMessage(ArchiveExposureComplete exposureComplete)
	{
		ODIDataSubsystemExposure exposure = exposureComplete.getExposure();
		
		String portalMessage = "";
		portalMessage += "<JobStart>";
		portalMessage += "<XSEDEJobID>"+exposure.getId()+"</XSEDEJobID>";
		portalMessage += "<InputExposures>";
		portalMessage += "	<Exposure>";
		portalMessage += "	<LogicalID>"+exposure.getId()+"</LogicalID>";
		portalMessage += " 		<Path>"+exposure.getLocalPath()+"</Path>";
		portalMessage += "</Exposure>";
		portalMessage += "</InputExposures>";
		portalMessage += "<Params>";
		portalMessage += "</Params>";
		portalMessage += "</JobStart>";

		return portalMessage;
	}
	
	private void publishExposureArchived(ArchiveExposureComplete exposureComplete)
	{
		ODIDataStorageSystem.getInstance().publishTopic(exposureComplete.getXmlString(),props.getFileArchiveStatusQueue());
		if(exposureComplete.fullSetArchived())
			dss.notifyPortalExposureArchived(this.buildPortalMessage(exposureComplete));
	}
}
