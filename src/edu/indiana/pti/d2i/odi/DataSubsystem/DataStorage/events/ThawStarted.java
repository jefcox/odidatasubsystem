package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.CacheArchiveDataTask;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class ThawStarted extends ODIDataSubsystemComplexEvent
{
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	
	public ThawStarted()
	{
		this.name = ThawStarted.class.getCanonicalName();
		this.expression = "select * from " + CacheArchiveDataTask.class.getSimpleName();
		this.expression += " where " + CacheArchiveDataTask.class.getSimpleName() + ".state.toString() = '"+ ODIDataSubsystemTaskState.Started.toString() +"'";
	}
	
	public void update(CacheArchiveDataTask task)
	{
		String message = "";
		message += "<Response>";
		message += "<Files>";
		message += "<File>";
		message += "<LogicalID>"+task.getDataProduct().getId()+"</LogicalID>";
		message += "<Status>"+task.getStatus()+"</Status>";
		message += "<Detail> Task: [ "+task.getClass().getSimpleName()+" ] State: [ "+task.getState()+" ] Status: [ "+task.getStatus()+" ]</Detail>";
		message += "</File>";
		message += "</Files>";
		message += "</Response>";
		dss.publishFileThawUpdate(message);
	}
}
