package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class RemoveRequest extends ODIDataSubsystemComplexEvent
{
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	public RemoveRequest()
	{
		this.name = RemoveRequest.class.getCanonicalName();
		this.expression = "select * from " + RabbitMQMessage.class.getSimpleName();
		this.expression += " where RabbitMQMessage.envelope.routingKey = '" + props.getRemoveRequestQueue() + "'";
	}

	public void update(RabbitMQMessage event)
	{
		String message = new String(event.getBody());
		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			String scope =  XMLUtil.getTextValue(request, "scope");
			List<String> logicalIDs = XMLUtil.getTextValues(request, "LogicalID");
			
			if(!logicalIDs.isEmpty())
			{
				ArchiveSetRemoved fileSetRemoved = new ArchiveSetRemoved(logicalIDs);
				RemoveRequestComplete removeRequestComplete = new RemoveRequestComplete(fileSetRemoved.getID());
				RemoveRequestAck requestAck = new RemoveRequestAck(event.getEnvelope().getDeliveryTag(), removeRequestComplete.getID());
				
				EventStream.getInstance().detect(requestAck);
				EventStream.getInstance().detect(removeRequestComplete);
				EventStream.getInstance().detect(fileSetRemoved);
			}
			else
			{
				//TODO: add some sort of error handling here for empty set of logical id's
			}
			
			for (String logicalID : logicalIDs)
			{
				ODIDataSubsystemDataProduct dataProduct = IRODSUtil.getDataProduct(logicalID, dss.getLogger());

//				ODIDataSubsystemDataProduct dataProduct = new ODIDataSubsystemDataProduct(logicalID);
				
				if(scope.equals("all"))
					dss.removeDataProduct(dataProduct, true);
				else
					dss.removeDataProduct(dataProduct, false);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
