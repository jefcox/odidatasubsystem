package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import java.util.List;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;

public class ArchiveSetRemoved extends ODIDataSubsystemComplexEvent
{
	protected String		xmlString	= "<Files>";
	protected List<String>	logicalIDs	= null;

	public ArchiveSetRemoved(List<String> logicalIDs)
	{
		this.name = ArchiveSetRemoved.class.getCanonicalName();
		this.logicalIDs = logicalIDs;
		int idCount = logicalIDs.size();
		this.expression = "select * from " + ArchiveRemoved.class.getSimpleName();
		this.expression += " where " + ArchiveRemoved.class.getSimpleName() + ".logicalID ";

		if (idCount == 1)
		{
			this.expression += "= '" + logicalIDs.get(0) + "'";
		}
		else
		{
			this.expression += "IN (";
			for (String logicalID : logicalIDs)
			{
				this.expression += "'" + logicalID + "'";
				idCount--;
				if (idCount > 0)
					this.expression += ",";
			}
			this.expression += ") ";
		}

		this.name = this.expression;
	}

	public void update(ArchiveRemoved event)
	{
		this.xmlString += event.getXmlString();
		this.logicalIDs.remove(event.getLogicalID());
		if (logicalIDs.isEmpty())
		{
			this.xmlString += "</Files>";
			EventStream.getInstance().insertEvent(this);
			EventStream.getInstance().stopDetect(this);
		}
	}

	public String getXmlString()
	{
		return xmlString;
	}

	public void setXmlString(String xmlString)
	{
		this.xmlString = xmlString;
	}
}
