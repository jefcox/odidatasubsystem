package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage;

import java.io.IOException;
import java.util.HashMap;

import edu.indiana.pti.d2i.odi.DataSubsystem.IRODSUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemDataProduct;
import edu.sdsc.grid.io.irods.IRODSFile;
import edu.sdsc.grid.io.irods.IRODSFileSystem;
import edu.sdsc.grid.io.local.LocalFile;

public class ODIDataSubsystemExposure extends ODIDataSubsystemDataProduct
{
	private String											exposureDir		= null;
	private HashMap<String, ODIDataSubsystemDataProduct>	dataProducts	= new HashMap<String, ODIDataSubsystemDataProduct>();

	public ODIDataSubsystemExposure(String id, String exposureDir)
	{
		super(id);
		this.exposureDir = exposureDir;
	}

	public ODIDataSubsystemExposure(String id, String localPath, String exposureDir, String cacheResource, String archiveResource)
	{
		super(id, localPath, cacheResource, archiveResource);

		this.exposureDir = exposureDir;
	}

	@Override
	public String getArchiveDir()
	{
		return exposureDir;
	}
	
	@Override
	public void setArchiveDir(String archiveDir)
	{
		this.exposureDir = archiveDir;
	}
	
	public String getExposureDir()
	{
		return exposureDir;
	}

	public void setExposureDir(String exposureDir)
	{
		this.exposureDir = exposureDir;
	}

	public void setArchiveResource(String archiveResource)
	{
		super.setArchiveResource(archiveResource);
		for (ODIDataSubsystemDataProduct ota : dataProducts.values())
			ota.setArchiveResource(archiveResource);
	}

	public void setCacheResource(String cacheResource)
	{
		super.setCacheResource(cacheResource);
		for (ODIDataSubsystemDataProduct ota : dataProducts.values())
			ota.setCacheResource(cacheResource);
	}

	public void addFile(ODIDataSubsystemExposureFile exposureFile)
	{
		this.dataProducts.put(exposureFile.getId(), exposureFile);
	}

	public HashMap<String, ODIDataSubsystemDataProduct> getOTAS()
	{
		return dataProducts;
	}

	public String toString()
	{
		String eString = " [ Exposure: { " + super.toString() + " } Dataproducts: [ ";
		for (ODIDataSubsystemDataProduct dataProduct : dataProducts.values())
			eString += "{ " + dataProduct.toString() + " } ";
		eString += " ] ]";
		return eString;
	}

	@Override
	public boolean isCached()
	{
		boolean isCached = super.isCached();
		if (isCached)
			isCached = this.verifyCache();
		
		return isCached;
	}
	
	@Override
	protected boolean verifyCache()
	{
		boolean verified = true;
		try
		{
			IRODSResource cacheResource = this.getCacheResource();
			if (cacheResource != null)
			{
				IRODSFileSystem irodsFS = IRODSUtil.getIRODSFileSystem();

				
				IRODSFile archiveDir = new IRODSFile(irodsFS, this.getArchiveDir());
				String relativePath =  IRODSUtil.stripZone(archiveDir.getPath());

				String resourcePath = cacheResource.getResourceVaultPath();
				LocalFile localDirectory = new LocalFile(resourcePath + "/" + relativePath);
				
				if(!localDirectory.exists())
					localDirectory.mkdir();

				for(ODIDataSubsystemDataProduct dataProduct: this.dataProducts.values())
				{
					 if(dataProduct.isCached() == false)
						 verified = false;
				}
			}
		}
		catch (IOException ioe)
		{
			ioe.printStackTrace();
			//TODO: Log this exception
			return false;
		}

		return verified;
	}
}
