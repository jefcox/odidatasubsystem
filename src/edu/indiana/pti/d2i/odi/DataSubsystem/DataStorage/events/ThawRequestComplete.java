package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class ThawRequestComplete extends ODIDataSubsystemComplexEvent
{
	private ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	public ThawRequestComplete(String requestSetId)
	{
		this.expression = "Select * from " + ArchiveSetThawed.class.getSimpleName();
		this.expression += " where ArchiveSetThawed.ID = '" + requestSetId + "'";
		this.name = this.expression;
	}
	
	public void update(ArchiveSetThawed event)
	{
		dss.publishTopic(event.getXmlString(), props.getExposureThawedTopic());
		EventStream.getInstance().stopDetect(this);
		EventStream.getInstance().insertEvent(this);
	}
}
