package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class ThawRequestAck extends ODIDataSubsystemComplexEvent
{
	long deliveryTag = 0;
	ODIDataStorageSystem dss = ODIDataStorageSystem.getInstance();

	public ThawRequestAck(long deliveryTag, String requestSetId)
	{
		this.deliveryTag = deliveryTag;
		this.name = ThawRequestAck.class.getCanonicalName() + "|" + deliveryTag;
		this.expression = "select * from " + ThawRequestComplete.class.getSimpleName();
		this.expression += " where " + ThawRequestComplete.class.getSimpleName() + ".ID  = '" + requestSetId + "'";
	}
	
	public void update(ThawRequestComplete exposureComplete)
	{
		dss.acknowledgeMessage(deliveryTag, false);
		EventStream.getInstance().stopDetect(this);
	}
}
