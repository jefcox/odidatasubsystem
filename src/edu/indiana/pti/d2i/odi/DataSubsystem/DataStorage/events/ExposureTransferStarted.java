package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.XMLUtil;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;
import edu.indiana.pti.d2i.odi.DataSubsystem.messaging.RabbitMQMessage;

public class ExposureTransferStarted extends ODIDataSubsystemComplexEvent
{
	ODIDataStorageSystem	dss	= ODIDataStorageSystem.getInstance();
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	public ExposureTransferStarted()
	{
		this.name = ExposureTransferStarted.class.getCanonicalName();
		this.expression = "select * from " + RabbitMQMessage.class.getSimpleName();
		this.expression += " where RabbitMQMessage.envelope.routingKey = '" + props.getInstrumentIngested() + "'";
	}

	public void update(RabbitMQMessage event)
	{
		String message = new String(event.getBody());
		try
		{
			Document xmlDoc = XMLUtil.parseXML(message);
			Element request = xmlDoc.getDocumentElement();
			NodeList nodeList = request.getElementsByTagName("TABLE");
			for (int i = 0; nodeList != null && i < nodeList.getLength(); i++)
			{
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					
//					System.out.println(element);

//					String exposureID = XMLUtil.getTextValue(element, "ID");
//					String numOTAs = XMLUtil.getTextValue(element, "OTACount");
					
					
//					NodeList otas = element.getElementsByTagName("OTA");
//					for (int j = 0; otas != null && j < otas.getLength(); j++)
//					{
//						Node ota = otas.item(j);
//						if (ota.getNodeType() == Node.ELEMENT_NODE)
//						{
//							Element otaElement = (Element) ota;
//							String file = XMLUtil.getTextValue(otaElement, "File");
//							String logicalID = XMLUtil.getTextValue(otaElement, "ID");
//							ODIid id = new ODIid(logicalID);
//							ODIdn dn = new ODIdn("test");
//							dss.archiveData(id, dn, exposureID, file);
//						}
//					}

				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
