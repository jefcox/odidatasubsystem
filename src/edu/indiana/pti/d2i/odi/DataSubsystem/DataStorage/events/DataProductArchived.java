package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemProperties;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskState;
import edu.indiana.pti.d2i.odi.DataSubsystem.ODIDataSubsystemTaskStatus;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.EventStream;
import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ArchiveDataProductTask;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageSystem;

public class DataProductArchived extends ODIDataSubsystemComplexEvent
{
	private String logicalID = "";
	private String xmlString = "";
	private ODIDataSubsystemTaskStatus status = null;
	private String path = "";
	private ODIDataSubsystemTaskState state = null;
	private ODIDataSubsystemProperties props = ODIDataSubsystemProperties.getProperties();

	
	public DataProductArchived()
	{
		this.name = DataProductArchived.class.getCanonicalName();
		this.expression = "select * from "+ ArchiveDataProductTask.class.getSimpleName() +" ";
		this.expression += "where " + ArchiveDataProductTask.class.getSimpleName() + ".state.toString() = '"+ ODIDataSubsystemTaskState.Completed.toString() +"'";
	}
	
	public void update(ArchiveDataProductTask task)
	{
		this.logicalID = task.getDataProduct().getId();
		this.status = task.getStatus();
		this.state = task.getState();
		this.path = task.getDataProduct().getArchivePath();
		
		this.xmlString = "<File>";
		this.xmlString += "<LogicalID>"+this.logicalID+"</LogicalID>";
		this.xmlString += "<Status>"+task.getStatus()+"</Status>";
		this.xmlString += "<Path>"+this.path+"</Path>";
		this.xmlString += "<Detail> Task: ["+task.getClass().getSimpleName()+"] State: ["+task.getState()+"] Status: ["+task.getStatus()+"]</Detail>";
		this.xmlString += "</File>";
		
		String message ="";
		message += "<ArchiveResponse>";
		message += "<Files>";
		message += this.xmlString;
		message += "</Files>";
		message += "</ArchiveResponse>";
		
		ODIDataStorageSystem.getInstance().publishTopic(message,props.getFileArchiveStatusQueue());

		EventStream.getInstance().insertEvent(this);
	}
	
	public String getXmlString()
	{
		return this.xmlString;
	}
	
	public void setXmlString(String xml)
	{
		this.xmlString = xml;
	}

	public String getLogicalID()
	{
		return logicalID;
	}

	public void setLogicalID(String logicalID)
	{
		this.logicalID = logicalID;
	}

	public ODIDataSubsystemTaskStatus getStatus()
	{
		return status;
	}

	public void setStatus(ODIDataSubsystemTaskStatus status)
	{
		this.status = status;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public ODIDataSubsystemTaskState getState()
	{
		return state;
	}

	public void setState(ODIDataSubsystemTaskState state)
	{
		this.state = state;
	}
}
