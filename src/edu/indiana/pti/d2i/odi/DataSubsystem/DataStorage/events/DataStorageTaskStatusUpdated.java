package edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.events;

import edu.indiana.pti.d2i.odi.DataSubsystem.CEP.Events.ODIDataSubsystemComplexEvent;
import edu.indiana.pti.d2i.odi.DataSubsystem.DataStorage.ODIDataStorageAbstractTask;

public class DataStorageTaskStatusUpdated extends ODIDataSubsystemComplexEvent
{

	public DataStorageTaskStatusUpdated()
	{
		this.name = DataStorageTaskStatusUpdated.class.getCanonicalName();
		this.expression = "select * from " + ODIDataStorageAbstractTask.class.getSimpleName();
	}

	public void update(ODIDataStorageAbstractTask event)
	{
		
	}
}
