package edu.indiana.pti.d2i.odi.DataSubsystem;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ODIDataSubsystemTaskQueue
{
	protected BlockingQueue<ODIDataSubsystemTask>	queue	= new LinkedBlockingQueue<ODIDataSubsystemTask>();

	public void addTask(ODIDataSubsystemTask task)
	{
		queue.add(task);
	}

	public ODIDataSubsystemTask getTask() throws InterruptedException
	{
		ODIDataSubsystemTask task = new NullTask(this);

		task = queue.take();

		return task;
	}
}
