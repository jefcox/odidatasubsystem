package edu.indiana.pti.d2i.odi.DataSubsystem;

public class ODIDataSubsystemQueueingModule extends ODIDataSubsystemModule
{

	protected int							NUM_THREADS	= 5;
	protected ODIDataSubsystemTaskWorker[]	workers		= null;
	protected ODIDataSubsystemTaskQueue		taskQueue	= new ODIDataSubsystemTaskQueue();


	protected ODIDataSubsystemQueueingModule(String moduleName)
	{
		super(moduleName);
		final int numWorkers = NUM_THREADS;
		workers = new ODIDataSubsystemTaskWorker[numWorkers];
		for (int i = 0; i < workers.length; i++)
		{
			workers[i] = new ODIDataSubsystemTaskWorker(taskQueue);
			workers[i].start();
			workers[i].setName(this.moduleName + " Worker Thread " + i);
		}
		this.NUM_THREADS = props.getNumThreads(moduleName);
	}

	public void shutdown()
	{
		for (ODIDataSubsystemTaskWorker worker : workers)
			taskQueue.addTask(new ShutdownTask(worker));
	}
}
