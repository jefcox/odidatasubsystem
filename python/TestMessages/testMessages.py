#!/opt/local/bin/python2.7

import pika
import uuid
import sys
import shutil

from  DataSubsystemMQServer import *


HOST="localhost"
VHOST="/odi"
USER="datasubsystem"
PASSWORD="sdfa3laj43adjkfa"

DSSMQServer  = DataSubsystemMQServer(HOST,VHOST,USER,PASSWORD)

#Thaw Set Test
message = "<LogicalIDs>"
message += "<LogicalID>o20111101T012313.00</LogicalID>"
message += "</LogicalIDs>"
print " [x] Testing Thaw LogicalID set"
print " [x] Sending (" + message + ")"
#DSSMQServer.send(message,"","datasubsystem.thaw")
DSSMQServer.call(message,"","datasubsystem.thaw")

#Detrended Detail Test
#message = "<Request>"
#message += "<LogicalID>ecb136b7-bc8c-4fa2-a0ca-b757e0b051d3</LogicalID>"
#message += "</Request>"
#print " [x] Testing findDetrendedDetailById"
#print " [x] Calling (" + message + ")"
#print " [x] Response: " + DSSMQServer.call(message, "", "dataengine.detrended_details")

#PSA Test
#message = "<request>"
#message += "<time>2011-11-04:09:40:10</time>"
#message += "<dataset>20111031_7decd3a</dataset>"
#message += "<procid>7dec3a</procid>"
#message += "<nights>"
#message += "<night>2011-10-31</night>"
#message += "<night>2011-11-01</night>"
#message += "<night>2011-11-02</night>"
#message += "</nights>"
#message += "<subsets>"
#message += "<subset>*</subset>"
#message += "</subsets>"
#message += "<replace>false</replace>"
#message += "<archive>true</archive>"
#message += "</request>"
#print " [x] Testing PSA Scheduleing"
#print " [x] Sending (" + message + ")"
#DSSMQServer.send(message,"","dataengine.psa.request")
#DSSMQServer.consume("dataengine.psa.response")
