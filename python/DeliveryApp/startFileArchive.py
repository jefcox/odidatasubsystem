#!/opt/local/bin/python2.7

import pika
import sys
import os
import uuid
import shutil
from  DataSubsystemMQServer import *


HOST="localhost"
VHOST="/odi"
USER="dataengine"
PASSWORD="dataengine123"
QUEUE="datasubsystem.incoming"
EXCHANGE=""
DC_MOUNT="/Users/jefcox/Documents/ODI/ODIDataSubsystem/stage-files/"

if len(sys.argv) < 2:
    sys.exit('Usage: %s <FILE_PATH> (i.e. /path/to/myfile.fits)' % sys.argv[0])

if not os.path.exists(sys.argv[1]):
    sys.exit('ERROR: File: %s was not found!' % sys.argv[1])

filePath =  sys.argv[1]

print " [x] Copying " + filePath + " to " + DC_MOUNT + os.path.basename(filePath)
shutil.copy2(filePath, DC_MOUNT)
print " [x] Copy Complete"

print " [x] Notifying DataSubsystem"
DSSMQServer  = DataSubsystemMQServer(HOST,VHOST,USER,PASSWORD)

message = "<IncomingRawExposures>"
message += "	<IncomingRawExposure>"
message += "		<ID>"+os.path.basename(filePath)+"-dir</ID>"
message += "		<ExposureDir>"+filePath+"-dir</ExposureDir>"
message += "		<OTACount>3</OTACount>"
message += "		<OTAs>"
message += "				<OTA>"
message += "				<ID>"+os.path.basename(filePath)+"-1</ID>"
message += "				<File>"+filePath+"-1</File>"
message += "				</OTA>"
message += "				<OTA>"
message += "				<ID>"+os.path.basename(filePath)+"-2</ID>"
message += "				<File>"+filePath+"-2</File>"
message += "				</OTA>"
message += "				<OTA>"
message += "				<ID>"+os.path.basename(filePath)+"-3</ID>"
message += "				<File>"+filePath+"-3</File>"
message += "				</OTA>"
message += "		</OTAs>"
message += "	</IncomingRawExposure>"
message += "</IncomingRawExposures>"

DSSMQServer.send(message, EXCHANGE, QUEUE)
print " [x] DataSubsystem Notified"
print " [x] Delivery Complete"
