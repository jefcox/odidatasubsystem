#!/opt/local/bin/python2.7

import pika
import uuid
import sys

class DataSubsystemMQServer(object):
	def __init__(self, host, vhost, user, password):
		self.host = host
		self.vhost = vhost
		self.user = user
		self.password = password
		self.consumer_tag = "temp"

		credentials = pika.PlainCredentials(user, password)
		self.connection = pika.BlockingConnection(pika.ConnectionParameters(credentials=credentials, host=host, virtual_host=vhost))

		self.channel = self.connection.channel()

#		result = self.channel.queue_declare(exclusive=True)
#		self.callback_queue = result.method.queue
		self.callback_queue = "datasubsystem.CallBackQueue"
#		self.callback_queue += str(uuid.uuid4())
		self.channel.queue_declare(queue=self.callback_queue, durable=True)

		self.channel.basic_consume(self.on_response, no_ack=True, queue=self.callback_queue)

	def callback(self, ch, method, properties, body):
		print " [x] Received %r" % (body,)

	def on_response(self, ch, method, props, body):
		print " [x] Received %r in on_response" % (body,)
		if self.corr_id == props.correlation_id:
			self.response = body

	def call(self, n, exchange, queue):
		self.channel.queue_declare(queue=queue, durable=True)
		self.response = None
		self.corr_id = str(uuid.uuid4())
		self.channel.basic_publish(exchange=exchange,routing_key=queue,properties=pika.BasicProperties(reply_to = self.callback_queue,correlation_id = self.corr_id,), body=str(n))

		while self.response is None:
			self.connection.process_data_events()
		
		return str(self.response)
	
	def send(self, n, exchange, queue):
		self.channel.queue_declare(queue=queue, durable=True)
		self.response = None
		self.channel.basic_publish(exchange=exchange,routing_key=queue,body=str(n))

	def consume(self, queue):
		self.channel.queue_declare(queue=queue, durable=True)
		method_frame, header_frame, body = self.channel.basic_get(queue=queue)
#    	while (method_frame.NAME != 'Basic.GetEmpty'):
		print " [x] Received %r" % (body,)
#			method_frame, header_frame, body = self.channel.basic_get(queue=queue)

