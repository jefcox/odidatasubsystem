#!/opt/local/bin/python2.7
##!/usr/bin/python

import pika
import sys
import os
import shutil
import hashlib
from xml.etree import ElementTree

#HOST="event-dev2.odi.iu.edu"
HOST="localhost"
VHOST="/odi"
USER="deliveryapp"
PASSWORD="13fpgfgfdk30bvgasdd2"
ARRIVALQUEUE="deliveryapp.arrival"
DATAQUEUE="datasubsystem.incoming"
ERRORQUEUE = "deliveryapp.error"
EXCHANGE=""
#DESTDIR="/N/dcwan/projects/odi/dev/incoming_raw_tmp"
DESTDIR="/Users/jefcox/Documents/odi/ODIDataSubsystem/python/DTS_Delivery_App/incoming_raw_temp"

#open the file, read it, close it, return checksum
def getMD5(file):
	f = open(file)
	m = hashlib.md5()
	m.update(f.read())
	f.close
	return m.hexdigest()


credentials = pika.PlainCredentials(USER,PASSWORD)
connection = pika.BlockingConnection(pika.ConnectionParameters(credentials=credentials, host=HOST, virtual_host=VHOST))
channel = connection.channel()

channel.queue_declare(queue=ARRIVALQUEUE, durable=True)
channel.queue_declare(queue=DATAQUEUE, durable=True)
channel.queue_declare(queue=ERRORQUEUE, durable=True)

def callback(ch, method, properties, body):
	msg = ElementTree.fromstring(body)
	for ota in msg.findall('IncomingRawExposure/OTAs/OTA/File'):
		otafile = ota.text
		cp_otafile = DESTDIR + '/' + str(os.path.basename(otafile))
		try: 
			shutil.copy2(otafile, DESTDIR)
			checksumSource = getMD5(otafile)
			checksumDest = getMD5(cp_otafile)
			if(checksumSource != checksumDest):
				error_message = "Checksum failed for " + str(otafile) + '!'
				channel.basic_publish(exchange=EXCHANGE,routing_key=ERRORQUEUE,body=error_message)
				return
			print ' [x] Copied ' + otafile + ' to ' + cp_otafile
		except:
			error_message = "Copy failed for " + str(otafile) + '!'
			channel.basic_publish(exchange=EXCHANGE,routing_key=ERRORQUEUE,body=error_message)
			return 
		ota.text = cp_otafile
	delivery_msg = ElementTree.tostring(msg)
	channel.basic_publish(exchange=EXCHANGE,routing_key=DATAQUEUE,body=delivery_msg)
	print ' [x] Delivery message sent'
	
channel.basic_consume(callback,queue=ARRIVALQUEUE,no_ack=True)

print ' [*] Waiting for messages. To exit press CTRL+C'
channel.start_consuming()
