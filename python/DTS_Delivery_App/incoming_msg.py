#!/opt/local/bin/python2.7
##!/usr/bin/python

import pika
import sys
import os
from  DataSubsystemMQServer import *

## TODO Move this stuff to config file
#HOST="event-dev2.odi.iu.edu"
HOST="localhost"
VHOST="/odi"
USER="deliveryapp"
PASSWORD="13fpgfgfdk30bvgasdd2"
QUEUE="deliveryapp.arrival"
ERRORQUEUE = "deliveryapp.error"
EXCHANGE=""

## TODO: Consider sending console errors from this script to MessageBus (to an error queue)
if len(sys.argv) < 2:
    sys.exit('Usage: %s <EXPOSURE_PATH> (i.e. /path/to/exposure)' % sys.argv[0])

if not os.path.exists(sys.argv[1]):
    sys.exit('ERROR: Exposure Directory: %s was not found!' % sys.argv[1])

if not os.path.isdir(sys.argv[1]):
	sys.exit('ERROR: %s is not a directory!' % sys.argv[1])
	
SourcePath = os.path.abspath(sys.argv[1])

#Get metadata to fill in the message
exposureID = os.path.basename(SourcePath)
otaCount = str(len(os.listdir(SourcePath)))

print " [x] Notifying PPA"

message = "<IncomingRawExposures>"
message += "<IncomingRawExposure>"
message += "<ID>"+exposureID+"</ID>"
message += "<OTACount>"+otaCount+"</OTACount>"
message += "<OTAs>"

#Loop through all the OTAs in the exposure
#No file checking at this point, assumes all files in exposure directory are OTAs
for ota in os.listdir(SourcePath):
	#add file to the message
	otaID = ota.strip('.fz').strip('.fits') #clear off the file extensions to generate the uniqueID
	message += "<OTA>"
	message += "<ID>"+otaID+"</ID>"
	message += "<File>"+SourcePath+'/'+ota+"</File>"
	message += "</OTA>"
	
#close out the message
message += "</OTAs>"
message += "</IncomingRawExposure>"
message += "</IncomingRawExposures>"


DSSMQServer  = DataSubsystemMQServer(HOST,VHOST,USER,PASSWORD)
DSSMQServer.send(message, EXCHANGE, QUEUE)
print " [x] PPA Notified"

